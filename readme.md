# Curtin University Software Engineering Project 2
## Group ID: C_SEP_005
## Project ID: CP072016

**Group Member Details:**

```
#!command

1. Ali Nauf - 18872886
2. Imad Shan - 18679074
3. B.Balanirajh - 18889093
```

**Weekly Documents:** [SEP Weekly Documents (C_SEP_005)](https://bitbucket.org/SEP005/sep/src/2a307c9cc24e741d4556788c7a21527f0fd39f7e/SEP%20Weekly%20Documents%20(C_SEP_005)/?at=master)

# System Requirements

* Php and apache server or nginx server should be available
* Laravel 5.2 or higher should be installed.
* Helpful installation guide: [Laravel Homestead install](https://www.youtube.com/watch?v=tbPGuikTzKk&list=PLfdtiltiRHWEaImCreOC7UkK4U5Xv9LdI)

**In the composer.json the following section should be there in file section:
** 
 
```
#!php

"files":[
            "app/helpers.php"
        ]
```


## Composer Installations for the required api dependencies(Required to be run on command-prompt)
  

```
#!command

* composer require intervention/image
* composer require stripe/stripe-php
* composer require barryvdh/laravel-ide-helper
```

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).