<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});


/**
 * Faker helps to input dummy data 
 */

$factory->define(App\Task::class, function (Faker\Generator $faker) {
    return [


        'topic' => $faker->jobTitle,

        'user_id' => 2,//factory('App\User')->create()->id,

        'category_id' => $faker->numberBetween(1,2),

        'subcategory_id' => $faker->numberBetween(1,2),

        'details' => $faker->paragraph(4),

        'status' => 1,

        'price' => $faker->numberBetween(1,1000)

    ];
});

$factory->define(App\Showcase::class, function (Faker\Generator $faker) {
    return [

        'user_id' => 3,//factory('App\User')->create()->id,
        'category_id' => $faker->numberBetween(1,2),
        'title' => $faker->jobTitle,
        'description' => $faker->paragraph(1),
        'currency_type' =>"USD",// $faker->currencyCode,
        'budget' => $faker->numberBetween(1,1000)

    ];
});

$factory->define(App\ToDoList::class, function (Faker\Generator $faker) {
    return [

        'user_id' => 1,//factory('App\User')->create()->id,

        'body' => $faker->paragraph(1),

        'completed' => 0

    ];
});




