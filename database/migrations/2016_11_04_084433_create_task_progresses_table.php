<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskProgressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_progresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned()->index();
            $table->integer('requester_id')->unsigned()->index();
            $table->integer('worker_id')->unsigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_progresses');
    }
}
