<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('statusId')->unsigned();
            $table->foreign('statusId')->references('id')
                ->on('statuses')->onDelete('cascade');
            $table->string('image');
            $table->timestamps();
        });

    }
    public function down()
    {
        Schema::drop('images');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
}
