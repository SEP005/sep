<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMilestonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('milestones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bid_id')->unsigned()->index();
            $table->string('milestone');
            $table->string('complete_by'); // The date that the milestone is due by


            $table->integer('user_id')->unsigned()->index(); // Requester
            /**
             * Status :
             *        1 = ongoing
             *        2 = completed
             *        3 = overdue
             */
            $table->integer('status')->default(1)->unsigned()->index();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('milestones');
    }
}
