<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('account_id')->unsigned(); // strip_accounts table_id
            $table->integer('bid_id')->unsigned()->index();

            $table->integer('sender_id')->unsigned()->index(); // payment made to
            $table->integer('receiver_id')->unsigned()->index(); // Payment received by

            $table->double('payment_amount')->default(0)->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_payments');
    }
}
