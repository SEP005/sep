<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowcasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('showcases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',40);
            $table->text('description');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();

            $table->string('currency_type',10);
            $table->double('budget');

            $table->string('video_path');
            $table->string('pdf_path');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('showcases');
    }
}
