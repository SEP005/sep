<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowcasePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('showcase_photos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('showcase_id')->unsigned();

            //If Task is deleted the photos associated with that also gets deleted
            $table->foreign('showcase_id')->references('id')->on('showcases')->onDelete('cascade');

            $table->string('name');

            $table->string('path');

            $table->string('thumbnail_path');

            $table->timestamps();
        });

        }

    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('showcase_photos');
    }
}
