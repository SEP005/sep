<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('task_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->double('bid_price');
            $table->string('deliver_in');
            $table->text('proposal');

            //If Task is deleted the photos associated with that also gets deleted
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');

            /**
             * Status :
             *        1 = default
             *        2 = accepted
             *        3 = rejected
             *        4 = Disapproved by admin
             *        5 = Finished Tasks
             */
            $table->integer('status')->default(1)->unsigned()->index();

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('task_bids');
    }
}
