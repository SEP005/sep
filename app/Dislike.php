<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Dislike extends Authenticatable{

    protected $table = 'dislikeable';

    public function dislikeable()
    {
        return $this->morphTo();

    }
    public function user()
    {
        return $this->belongsTo('App\User'.'user_id');

    }
}