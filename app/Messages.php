<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = ['subject', 'message', 'sender_id', 'receiver_id'];

    public function getUserName($id){
        $name = User::where('id', $id)->firstOrFail();

        return $name->name;
    }
}
