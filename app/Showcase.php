<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Showcase extends Model
{
    //
    protected $fillable =[
        'category_id',
        'title' ,
        'description',
        'currency_type',
        'budget',
        'user_id',
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    /**
     * The Showcase is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * To show the showcase if the path is selected
     * @return string
     */
    public function path()
    {
        return '/showcase/' . $this->id;
    }

    /**
     * To get the showcase which has the referenced id
     * @param $id
     * @return mixed
     */
    public static function showcaseLocatedAt($id)
    {
        return static::where(compact('id'))->firstOrFail();
    }


    /**
     * To add photo to its database which belongs to the showcase
     * @param ShowcasePhoto $photo
     * @return Model
     */
    public function addShowcasePhoto(ShowcasePhoto $photo)
    {
        return $this->photos()->save($photo); //TO add photo belonging to the showcase
    }


    /**
     * A showcase post may contain many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function photos()
    {
        return $this->hasMany(ShowcasePhoto::class);
    }

    /**
     * Modify the video URL to include embed/ so that it can be
     * embedded in the view using iFrame
     * @param $video_path
     * @return mixed
     */
    public function getVideoPathAttribute($video_path)
    {
        return str_replace('watch?v=','embed/', $video_path);
    }

    /**
     * To delete the record from folder and from database
     * @throws \Exception
     */
    public function deleteFile()
    {
        \File::delete([ //To delete from the folder
            $this->pdf_path,
        ]);
        parent::delete();
    }


}
