<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Milestone extends Model
{
    //
    protected $fillable = ['bid_id','milestone','complete_by','user_id','status'];

    public function getTaskBid()
    {
        return $this->belongsTo(TaskBid::class,'bid_id');
    }

    public function getTaskRequester()
    {
        return $this->belongsTo(User::class,'requester_id');
    }

    public function getTaskWorker()
    {
        return $this->belongsTo(User::class,'worker_id');
    }
}
