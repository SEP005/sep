<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeQuestions extends Model
{
    protected $fillable = ['question_id', 'user_id'];
}
