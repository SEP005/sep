<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Undo extends Authenticatable{

    protected $table = 'likeable';

    public function likeable()
    {
        return $this->morphTo();

    }
    public function user()
    {
        return $this->belongsTo('App\User'.'user_id');

    }
}