<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('startpage');
});

Route::get('/home','PagesController@home');


//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('begin', function () {

   // Session::flash('status','HelloThere');



    flash('You are now signed in!','error');


    return redirect('/');
});



/** ROUTES ADDED BY SHAAN
----------------------------------------------------------------------------------- **/

Route::get('/api/users', function () {
    return \App\User::all();
});



/**-----User Category-----**/
Route::get('/categories', 'CategoryController@index');
Route::get('/category/{category}', 'CategoryController@show');


Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin', function () {
        $usersCount = \App\User::all()->count();
        $tasksCount = \App\Task::all()->count();
        $bidsCount = \App\TaskBid::all()->count();
        $questionCount = \App\Questions::all()->count();
        $answerCount = \App\Answer::all()->count();
        $value = \App\TaskBid::select('bid_price')->where('status', 2)->sum('bid_price');

        $users = \App\User::orderBy('created_at', 'DESC')->limit(8)->get();
        $tasks = \App\Task::orderBy('created_at', 'DESC')->limit(4)->get();
        $bids = \App\TaskBid::orderBy('created_at', 'DESC')->limit(4)->get();

        $reportedQuestions = \App\FlagQuestions::orderBy('created_at', 'DESC')->limit(5)->get();
        $reportedAnswers = \App\FlagAnswers::orderBy('created_at', 'DESC')->limit(5)->get();


        return view('admin.dashboard', compact('usersCount', 'tasksCount', 'bidsCount', 'value', 'users', 'tasks', 'bids', 'reportedQuestions', 'reportedAnswers', 'questionCount', 'answerCount'));
    });

    /**-----Categories-----**/
    Route::get('/admin/categories', 'Admin\CategoryController@index');
    Route::get('/admin/categories/{category}/edit', 'Admin\CategoryController@edit');
    Route::get('admin/categories/{category}/delete', 'Admin\CategoryController@delete');
    Route::post('/admin/categories/new', 'Admin\CategoryController@store');
    Route::patch('admin/categories/{category}', 'Admin\CategoryController@update');

    /**-----Roles-----**/
    Route::get('admin/roles', 'Admin\RolesController@index');
    Route::get('admin/roles/{role}/edit', 'Admin\RolesController@edit');
    Route::patch('admin/roles/{role}', 'Admin\RolesController@update');
    Route::get('admin/roles/{role}/delete', 'Admin\RolesController@delete');
    Route::post('/admin/roles/new', 'Admin\RolesController@store');

    /**-----Users-----**/
    Route::get('admin/users', 'Admin\UsersController@index');
    Route::get('admin/user-new', 'Admin\UsersController@userNewIndex');
    Route::post('admin/user-new', 'Admin\UsersController@store');
    Route::get('admin/profile/{user}/edit', 'Admin\UsersController@edit');
    Route::patch('admin/profile/{user}', 'Admin\UsersController@update');
    Route::post('admin/user/resetPassword', 'Auth\PasswordController@sendResetLinkEmail');
    Route::get('admin/user/{user}/delete', 'Admin\UsersController@delete');

    /**-----Tasks-----**/
    Route::get('admin/tasks', 'Admin\TaskController@index');
    Route::get('/admin/tasks/{task}/status', 'Admin\TaskController@status');
    Route::get('admin/tasks/{task}/delete', 'Admin\TaskController@delete');

    /**-----Bids-----**/
    Route::get('admin/bids', 'Admin\BidController@index');
    Route::get('/admin/bids/{bid}/status', 'Admin\BidController@status');
    Route::get('admin/bids/{bid}/delete', 'Admin\BidController@delete');

    /** Community Experts */
    Route::get('admin/community/experts/categories', 'Admin\Community\ExpertController@categoryIndex');
    Route::post('admin/community/experts/category/new', 'Admin\Community\ExpertController@categoryStore');
    Route::get('admin/community/experts/category/{category}/edit', 'Admin\Community\ExpertController@categoryEdit');
    Route::patch('admin/community/experts/category/{category}', 'Admin\Community\ExpertController@categoryUpdate');
    Route::get('admin/community/experts/category/{category}/delete', 'Admin\Community\ExpertController@categoryDelete');

    Route::get('admin/community/experts/questions', 'Admin\Community\ExpertController@questionIndex');
    Route::get('admin/community/experts/answers', 'Admin\Community\ExpertController@answerIndex');
    Route::post('admin/community/experts/question/{question}/status', 'Admin\Community\ExpertController@questionStatus');
    Route::post('admin/community/experts/answer/{answer}/status', 'Admin\Community\ExpertController@answerStatus');
    Route::get('admin/community/experts/question/{question}/edit', 'Admin\Community\ExpertController@questionEdit');
    Route::get('admin/community/experts/answer/{answer}/edit', 'Admin\Community\ExpertController@answerEdit');

});

Route::group(['middleware' => 'auth'], function () {
/** Messages */
Route::post('message/store', 'MessagesController@storeMessage');
Route::resource('messages','MessagesController');

/** Community Experts */
Route::get('community/experts/my', 'Community\ExpertController@myIndex');
Route::get('community/experts/myAnswers', 'Community\ExpertController@myAnswerIndex');
    
Route::post('community/experts/question/toggle/like', 'Community\ExpertController@toggleLike');
Route::post('community/experts/answer/toggle/like', 'Community\ExpertController@toggleAnswerLike');
Route::post('community/experts/answer/report', 'Community\ExpertController@reportAnswer');
Route::post('community/experts/question/report', 'Community\ExpertController@reportQuestion');
Route::get('community/experts/ask', 'Community\ExpertController@create');
Route::get('community/experts/question/{id}', 'Community\ExpertController@show');
Route::post('community/experts/question/answer', 'Community\ExpertController@storeAnswer');

Route::resource('community/experts','Community\ExpertController');
});

/** API */
Route::get('/api/footerstats', function () {
    $usersCount = \App\User::all()->count();
    $tasksCount = \App\Task::all()->count();

    $data = array('users'=>$usersCount, 'tasks'=>$tasksCount);

    return $data;
});

Route::get('/api/answers', function () {
    $data = \App\Answer::where('active', 1)->get();

    return $data;
});

Route::get('/api/question-likes', function () {
    $data = \App\LikeQuestions::all();

    return $data;
});

Route::get('/api/answer-likes', function () {
    $data = \App\LikeAnswers::all();
    return $data;
});

Route::get('/api/answer-reports', function () {
    $data = \App\FlagAnswers::all();
    return $data;
});

Route::get('/api/question-reports', function () {
    $data = \App\FlagQuestions::all();
    return $data;
});

Route::get('/api/answers/question/{id}', function ($id) {
    /*$data = \App\Answer::where('question_id', $id)->get();*/
    $data = \DB::table('answers')
        ->join('users','users.id','=','answers.user_id')
        ->select('answers.id as id','users.name as name', 'answers.created_at as created_at', 'answers.answer')
        ->where('question_id', $id)
        ->where('active', 1)
        ->latest()
        ->get();

    return $data;
});

Route::get('/api/answers/{qid}{uid}', function ($qid, $uid) {
    $data = \App\Answer::where('question_id', $qid)->where('user_id', $uid)->get();

    return $data;
});

Route::get('/api/questions', function () {
    $data = \App\Questions::where('active', 1)->get();

    return $data;
});

Route::get('/api/questions/{id}', function ($id) {
    $data = \App\Questions::where('questionCategory_id', $id)->get();

    return $data;
});

Route::get('/api/questions/user/{id}', function ($id) {
    $data = \App\Questions::where('user_id', $id)->get();

    return $data;
});

Route::get('/api/question-categories', function () {
    $data = \App\QuestionsCategory::all();

    return $data;
});

Route::get('/api/messages', function () {
    return \App\Messages::latest()->get();
});
/** END OF ROUTES ADDED BY SHAAN
------------------------------------------------------------------------------------ **/


Route::auth();
Route::get('/home', 'HomeController@index');






/**
 *  Code Written By Nauf for Routes
 */

Route::resource('tasks','TaskController'); //REST type calling routes

Route::get('workerTasks','TaskController@workerIndex');

Route::get('workerBids','TaskBidController@workerBids');

Route::get('tasks/{id}/delete', 'TaskController@destroy'); /**TODO Test this part thoroughly */

Route::get('taskBids','TaskController@taskBids');

Route::post('tasks/{id}/photos',[
    // This is a named route. use route('store_task_photo')  to call this route
    'uses'=>'TaskController@addPhoto',
    'as' => 'store_task_photo'
]);

Route::post('bidSelection/{id}',[
    // This is a named route. use route('store_like_sub')  to call this route
    'uses'=>'TaskBidController@selection',
    'as' => 'store_select_bid'
]);

Route::resource('tasks.bids','TaskBidController');//REST type calling routes

Route::delete('taskphotos/{id}','TaskPhotoController@destroy');


Route::put('tasks/{taskId}/bids/{bidId}',[
    // This is a named route. use route('bid.edit')  to call this route
    'uses'=>'TaskBidController@update',
    'as' => 'bid.edit'
]);

//Info: Dashboard and taskprogress routes
Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::resource('taskprogress','TaskInProgressController');//REST type calling routes

// Info: TodoList routes
Route::resource('todolist','ToDoListController');//REST type calling routes

Route::get('api/todolist', function () {
    return App\ToDoList::latest()->where('user_id',Auth::user()->id)->get();
});

Route::put('updateToDoItem/{id}','ToDoListController@updateItem');



// Info: Showcase Routes
Route::resource('showcase','ShowcaseController');//REST type calling routes

Route::get('/myshowcases/', 'ShowcaseController@myShowcases');

Route::get('showcase/{id}/delete', 'ShowcaseController@destroy'); /**TODO Test this part thoroughly */

Route::post('showcase/{id}/photos',[
    // This is a named route. use route('store_showcase_photo')  to call this route
    'uses'=>'ShowcaseController@addPhoto',
    'as' => 'store_showcase_photo'
]);

Route::delete('showcasephotos/{id}','ShowcasePhotoController@destroy');

Route::put('deletePdf/{id}','ShowcaseController@deletePdf');

// Info: Milestones Routes
Route::resource('taskprogress.milestone','MilestoneController');

Route::put('taskprogress/{bidId}/updateMilestone/{editId}','MilestoneController@updateMilestone');


///taskprogress/{{$taskProgress->bid_id}}/updateMilestone/@{{editId}}

// Info: Payment Routes
Route::post('payments','PaymentController@store');

Route::resource('ReceiveAccount','ReceiveAccountController');

Route::get('viewpayments','TaskInProgressController@viewpayments');



/**
 * End of Code Written By Nauf for Routes
 */

//  tasks/id/photos //POST



Route::group(['middleware' => ['web']], function () {
    //

//    Route::get('/start', function () {
//        return view('startpage');
//    })->middleware(['auth']);

  //  Route::get('/request', 'TaskController@showAddPage');//shows request add page






   // Route::get('/requestshow', 'TaskController@showAllRequests');//shows request add page

});


/**
 *
 * The Route::auth() is a shortcut to define the routes below

// Authentication Routes...
 *$this->get('login', 'Auth\AuthController@showLoginForm');
 *$this->post('login', 'Auth\AuthController@login');
 *$this->get('logout', 'Auth\AuthController@logout');

// Registration Routes...
 *$this->get('register', 'Auth\AuthController@showRegistrationForm');
 *$this->post('register', 'Auth\AuthController@register');

// Password Reset Routes...
 *$this->get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
 *$this->post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
 *$this->post('password/reset', 'Auth\PasswordController@reset');
 *
 */

/**user related routes
**
such as for search,posting advertisements,user profiles,timeline and etc.
**
*/
//search
Route::get('/search', [
    'uses' =>'\App\Http\Controllers\SearchController@getResults',
    'as'   =>'search.results'
]);

//user profile
Route::get('/user/{name}', [
    'uses' =>'\App\Http\Controllers\ProfileController@getProfile',
    'as'   =>'profile.index'
]);
Route::get('/user/{userId}', [
    'uses' =>'\App\Http\Controllers\ProfileController@getReview',
    'as'   =>'profile.review'
]);

//edit profile
Route::get('/profile/edit}', [
    'uses' =>'\App\Http\Controllers\ProfileController@getEdit',
    'as'   =>'profile.edit',
    'middleware' => ['auth'],
]);

Route::post('/profile/edit}', [
    'uses' =>'\App\Http\Controllers\ProfileController@postEdit',
    'middleware' => ['auth'],

]);
//post status page
Route::get('/postStatus', [
    'uses' =>'\App\Http\Controllers\statusController@getPostStatus',
    'as'=>'postStatus'
]);
//add photos page
Route::get('/addPhotos/{statusId}', [
    'uses' =>'\App\Http\Controllers\statusController@getAddPhotos',
    'as'=>'addPhotos'
]);
//upload photos

Route::post('/{statusId}/photos', [
    'uses' =>'\App\Http\Controllers\statusController@uploadPhotos',
    'as'=>'uploadPhotos'
]);

// friends
Route::get('/friends}', [
    'uses' =>'\App\Http\Controllers\FriendController@getIndex',
    'as'   =>'friend.index',
    'middleware' => ['auth'],
]);

Route::get('/friends/add/{name}', [
    'uses' =>'\App\Http\Controllers\FriendController@getAdd',
    'as'   =>'friend.add',
    'middleware' => ['auth'],
]);

//accept
Route::get('/friends/accept/{name}', [
    'uses' =>'\App\Http\Controllers\FriendController@getAccept',
    'as'   =>'friend.accept',
    'middleware' => ['auth'],
]);

//delete
Route::get('/friends/delete/{name}', [
    'uses' =>'\App\Http\Controllers\FriendController@getDelete',
    'as'   =>'friend.delete',
    'middleware' => ['auth'],
	]);

//statuses
Route::post('/status}', [
    'uses' =>'\App\Http\Controllers\statusController@postStatus',
    'as'=>'status.post',
    'middleware' => ['auth'],

]);

Route::post('/status/{statusId}', [
    'uses' =>'\App\Http\Controllers\statusController@updateStatus',
    'as'=>'status.update',

]);
//reply status
Route::post('/status/{statusId}/reply', [
    'uses' =>'\App\Http\Controllers\statusController@postReply',
    'as'=>'status.reply',
    'middleware' => ['auth'],

]);

//likes
Route::get('/status/{statusId}/like', [
    'uses' =>'\App\Http\Controllers\statusController@getLike',
    'as'   =>'status.like',
    'middleware' => ['auth'],
]);
//dislike route
Route::get('/status/{statusId}/dislike', [
    'uses' =>'\App\Http\Controllers\statusController@getDislike',
    'as'   =>'status.dislike',
    'middleware' => ['auth'],
]);
//delete post route
Route::get('/status/{statusId}/delete', [
    'uses' =>'\App\Http\Controllers\statusController@getDelete',
    'as'   =>'status.delete',
    'middleware' => ['auth'],
]);
//edit route
Route::post('/edit', [
    'uses' => '\App\Http\Controllers\statusController@postEditPost',
    'as' => 'edit'
]);
//save image
Route::post('/upateimage', [
    'uses' => 'ProfileController@postSaveAccount',
    'as' => 'account.save'
]);

Route::get('/userimage/{filename}', [
    'uses' => 'ProfileController@getUserImage',
    'as' => 'account.image'
]);

//undo likes 
Route::get('/status/{statusId}/undo', [
    'uses' =>'\App\Http\Controllers\statusController@getUndo',
    'as'   =>'status.undo',
    'middleware' => ['auth'],
]);

//post image save route
Route::post('/{status}', [
    'uses' => 'StatusController@postSaveImage',
    'as' => 'image.save'
]);

Route::get('/postimage/{filename}', [
    'uses' => 'StatusController@getPostImage',
    'as' => 'post.image'
]);


// Route that handles submission of review - rating/comment
Route::post('user/{id}',[
    'uses' => '\App\Http\Controllers\ReviewController@getReview',
    'as' => 'review.save',
]);