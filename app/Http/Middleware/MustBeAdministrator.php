<?php

namespace App\Http\Middleware;

use Closure;

class MustBeAdministrator
{
    /**
     * 
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     */
    public function handle($request, Closure $next)
    {

        $user = $request->user();

        if($user && $user->role_id == 2) //role_id = 2 is assumed to be administrator
        {
            return $next($request);
        }

        //abort(404,'You should be an administrator to view this page');

        //return $next($request);

        return redirect('/home'); // Addes by Shaan
    }

}

