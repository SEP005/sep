<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ShowcaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the show case request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'budget' => 'required|numeric|max:50000', // Maximum of 50,000 USD is the rate
            'user_id' => 'required',
            'category_id' => 'required|numeric|min:1',
            'pdfFile' => 'mimes:pdf'// Only pdf type files are allowed
        ];
    }

}
