<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TaskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * true if user has authorization and viceversa for false
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            'topic' => 'required',
            'details' => 'required',
            'price' => 'required|numeric|max:500000',
            'user_id' => 'required',
            'category_id' => 'required|numeric|min:1',

            /** Custom category error message implemented in validation.php
             * 'numeric' => 'Please choose a category' */
        ];
    }
}
