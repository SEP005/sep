<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReceiveAccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'user_id' => 'required',
            'access_token' => 'required',
            'refresh_token' => 'required',
            'token_type' => 'required',
            'stripe_publishable_key' => 'required',
            'stripe_user_id' => 'required',
            'scope' => 'required',

        ];
    }
}
