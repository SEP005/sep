<?php

namespace App\Http\Controllers;

use App\ToDoList;
use Illuminate\Http\Request;

use App\Http\Requests;

class ToDoListController extends Controller
{
    /**
     * Display a listing of the resource.
     * /todolist
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todolist = ToDoList::all()->where('user_id',\Auth::user()->id );

        return view('TaskInProgress.index', ['todolist'=>$todolist]);

    }

    /**
     * Show the form for creating a new resource.
     * /
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // To validate the data received from the form
        $this->validate($request, [
            'user_id' => 'required',
            'body' => 'required',
            'completed' => 'required',
        ]);
        
        if( ToDoList::create($request->all()) ) {

            //Flash Message
            flash()->success('Success!', 'The item is successfully posted');
            return redirect()->back();

        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }
   //     $todolist=ToDoList::all();//->sortByDesc('id')->first();


        //Redirect to landing page
        // return redirect()->back();
    //    return view('TaskInProgress.index', ['todolist'=>$todolist]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateItem($id,Request $request)
    {
        // Validating the inputs received from the edit form
        $this->validate($request, [
            'user_id' => 'required',
            'body' => 'required',
            'completed' => 'required',
        ]);

        // TO make the update to the to-do list item
        if( ToDoList::where('id', $id)->update([
            'body' => $request->body,
            'completed' => $request->completed,
        ]))
        {
            //Flash Message
            flash()->success('Success!', 'Item is successfully updated');
            return redirect()->back();

        }else
        {
            flash()->error('Oops!', 'Please check whether the fields are properly entered!');
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $todo = ToDoList::find($id);

        if($todo->completed==0) {
            ToDoList::where('id', $id)->update([
                'completed' => 1,
            ]);
        }
        else{
            ToDoList::where('id', $id)->update([
                'completed' => 0,
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ToDoList::find($id)->delete();
    }
}
