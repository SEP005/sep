<?php

namespace App\Http\Controllers;

use App\Milestone;
use Illuminate\Http\Request;

use App\Http\Requests;

define("ONE", 1);
define("TWO", 2);
define("THREE", 3);
//$ONE = 1;
//$TWO = 2;
//$THREE = 2;
//$FIVE = 5;
//$EIGHT = 8;

class MilestoneController extends Controller
{
    //


    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * | POST | taskprogress/{taskprogress}/milestone  | taskprogress.milestone.store   |
     */
    public function store($bidId,Request $request)
    {

        // Validate items recieved from the form
        $this->validate($request, [
            'bid_id' => 'required',
            'milestone' => 'required',
            'complete_by' => 'required|date',
            'user_id' => 'required',
        ]);

        if( Milestone::create($request->all()) ) {

            //Flash Message
            flash()->success('Success!', 'you have successfully created a milestone');

            return redirect()->back();
        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * 
     * taskprogress/{bidId}/updateMilestone/{editId}
     */
    public function updateMilestone(Request $request,$bidId, $editId)
    {

        $this->validate($request, [
            'bid_id' => 'required',
            'milestone' => 'required',
            'complete_by' => 'required|date',
            'user_id' => 'required',
        ]);

        if( Milestone::where('id', $editId)->update([

            'bid_id' => $request->bid_id,
            'milestone' => $request->milestone,
            'complete_by' => $request->complete_by,
            'user_id' => $request->user_id,
        ]))
        {
            //Flash Message
            flash()->success('Success!', 'Milestone is successfully updated');
            return redirect()->back();

        }
//        else
//        {
//            flash()->error('Oops!', 'Please check whether the milestone fields are properly entered!');
//            return redirect()->back();
//        }
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$bidId, $id)
    {
        /**
         * 1 : default
         * 2: Complete
         * 3: Overdue
         */
        $milestone= Milestone::find($id); // Find the milestone
        $status = $milestone->status;
        
        if($status== ONE || $status== THREE) // Change status to completed if its default or overdue
        {
            $val = TWO;
        }
        elseif ($status== TWO)// Change status to default if its completed
        {
            $val = ONE;
        }

        // Push the update to databse
        if( Milestone::where('id', $id)->update([
            'status' => $val,
        ]))
        {
//            Flash Message to be used if the milestone is updated as a post message
              flash()->success('Success!', 'Milestone Status changed');
            return redirect()->back();

        }
        else
        {
               flash()->error('Oops!', 'Please check whether the fields are properly entered!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bidId,$id)
    {
        $milestone = Milestone::find($id); // Find the milestone through the model

        if($milestone->delete())
        {
            // If bid gets success fully deleted
             flash()->success('Success!', 'Milestone has been successfully deleted!');
            return redirect()->back();

        }
    }

}
