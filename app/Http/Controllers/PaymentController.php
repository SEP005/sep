<?php

namespace App\Http\Controllers;

use App\ReceiveAccount;
use App\StripeAccount;
use App\TaskBid;
use App\UserPayment;
use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;
use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * Receive payment from requester and transfer payment to worker
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Get the bid
        $taskBid = TaskBid::where('id', $request->bid_id)->get()->first();
        $priceCharged = $taskBid->getActualPrice();

        // Get the worker account
        $receiver = ReceiveAccount::where('user_id', $request->worker_id)->get()->first();

        if (!isset($receiver)) // Check if price received
        {
            flash()->overlay('Oops!', 'The Worker you are trying to pay does not have a stripe account');
            return redirect()->back();

        }

        $receiverId = $receiver->stripe_user_id;

        if (!isset($priceCharged)) // Check if price received
        {
            flash()->error('Error!', 'Bid price not available');
            return redirect()->back();

        }

        // Perform the charge
        Stripe::setApiKey(config('services.stripe.secret'));// TO get my api key

        try {
            // Create a customer account in stripe
            $customer = Customer::create([
                'email' => request('stripeEmail'),
                'source' => request('stripeToken') //Get stripeToken returned from the stripe server
            ]);

            // To create the charge from the requester
            Charge::create([
                'customer' => $customer->id,
                'amount' => $priceCharged,
                'currency' => 'usd',
                'description' => ('$'. $taskBid->getActualPrice().' is paid to '.$taskBid->bidOwner->name.
                    ' for completing the task \' '.$taskBid->getTask->topic.
                    ' \' . Payment made by '.$taskBid->getTask->owner->name),
                // Transfer the payment to the worker who will receive the money
            ], array('stripe_account' => $receiverId));

        } catch (Exception $e) {
            //Handle Exception thrown by stripe
            return response()->json(
                ['status' => $e->getMessage()], 422
            );

        }
        //Input to stripe account
        //Get if existing account available
        $stripeAccount = StripeAccount::where('stripe_id', $customer->id)->get()->first();
        if (!isset($stripeAccount->stripe_id))// if no stripe account in database
        {
            $stripeAccount = new StripeAccount;

            $stripeAccount->stripe_id = $customer->id;
            $stripeAccount->user_id = \Auth::user()->id;
            $stripeAccount->save();
        }

        if (isset($stripeAccount->stripe_id)) {
            // Store payment record

            // Get the UserPayment Model
            $userPayment = new UserPayment;

            $userPayment->account_id = $stripeAccount->id;
            $userPayment->bid_id = $taskBid->id;
            $userPayment->sender_id = \Auth::user()->id; // Sender
            $userPayment->receiver_id = $taskBid->bidOwner->id;
            $userPayment->payment_amount = $priceCharged;

            // Create the payment
            if ($userPayment->save()) {

                if( TaskBid::where('id', $request->bid_id)->update([
                    //Change the status of the bid as complete
                    'status' => 5,
                ])){

                    flash()->success('Success!', 'Payment successfully made' );
                    return redirect()->back();

                }

            } else {
                flash()->error('Error!', 'Payment cannot be made');
                return redirect()->back();
            }

        }


//        else {
//            flash()->error('Error!', 'payment could not be made');
//            return redirect()->back();
//
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
