<?php

namespace App\Http\Controllers\Admin;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        $roles = Role::all();

        return view('admin.users', compact('users','roles'));

    }

    public function userNewIndex()
    {
        $roles = Role::all();

        return view('admin.user-new', compact('roles'));
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);

        $user = new User;

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $password = $request->password;

        $user->password = bcrypt($password);
        $user->role_id = $request->role_id;

        $user->save();

        flash()->success('Success', 'User has been successfully created!');

        return back();
    }

    /**
     * Edit function to edit users
     *
     * @param User $user
     * @return View
     */

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('admin.profile', compact('user','roles'));
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|max:255'
        ]);

        $users = User::all()->where('email', $request->email)->first();
        if(($users) && ($users->email != $user->email))
        {
            flash()->overlay('Warning', 'Email: ' . $request->email . ' is already being used by another user!', 'warning');
        }
        else
        {
            $user->name = $request->name;
            $user->lastname = $request->lastname;
            $user->address = $request->address;
            $user->street = $request->street;
            $user->city = $request->city;
            $user->country = $request->country;
            $user->website = $request->website;
            $user->bio = $request->bio;
            $user->email = $request->email;
            $user->role_id = $request->role_id;
            $user->update();

            flash()->success('Success', 'User has been successfully updated!');
        }

        return back();
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();

        flash()->success('Success', 'User has been successfully deleted!');

        return back();
    }

    /* User Function
    --------------------------*/

    public function updateUser(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'city' => 'required',
            'country' => 'required',
            'address' => 'required'
        ]);

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->address = $request->address;
            $user->city = $request->city;
            $user->country = $request->country;
            $user->website = $request->website;
            $user->bio = $request->bio;

            $user->update();

            flash()->success('Success', 'Your information has been successfully updated!');

        return back();
    }

    public function updatePasswordUser(Request $request, User $user)
    {
        $this->validate($request, [
            'password' => 'required',
            'newPassword' => 'required|min:6|confirmed'
        ]);

        $password = bcrypt($request->password);

        $result = User::all()->where('id', 1)->first();

        if($result)
        {
            if(($result->password) == ($password))
            {
                flash()->success('Success', 'Your Password is correct');
            }
            else
            {
                flash()->success('Success', 'Your Password is incorrect');
            }
        }
        else
        {
            flash()->error('Error', 'No user found!');
        }

        return back();
    }
}
