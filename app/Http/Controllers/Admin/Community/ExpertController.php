<?php

namespace App\Http\Controllers\Admin\Community;

use App\Answer;
use App\Questions;
use App\QuestionsCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExpertController extends Controller
{
    public function categoryIndex() {
        $data = QuestionsCategory::all();
        return view('admin.community.experts.categories', compact('data'));
    }

    public function questionIndex() {
        /*$data = \DB::table('questions')
            ->join('flag_questions','flag_questions.question_id','=','questions.id')
            ->select('questions.id as id','questions.question as question', 'questions.user_id as user_id', \DB::raw('COUNT(flag_questions.question_id) as qCount'))
            ->get();*/

        $data = Questions::all();

        return view('admin.community.experts.question', compact('data'));
    }

    public function answerIndex() {
        /*$data = \DB::table('questions')
            ->join('flag_questions','flag_questions.question_id','=','questions.id')
            ->select('questions.id as id','questions.question as question', 'questions.user_id as user_id', \DB::raw('COUNT(flag_questions.question_id) as qCount'))
            ->get();*/

        $data = Answer::all();

        return view('admin.community.experts.answer', compact('data'));
    }

    public function categoryStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:questions_categories,name'
        ]);

        $data = new QuestionsCategory;

        $data->name = $request->name;
        $data->description = $request->description;

        $data->save();

        flash()->success('Success', 'Category been added successfully!');

        return back();
    }

    public function categoryDelete($id)
    {

        $data = QuestionsCategory::find($id);
        $data->delete();

        flash()->success('Success', 'Category has been successfully deleted!');

        return back();
    }

    public function categoryEdit($id)
    {

        $data = QuestionsCategory::where('id', $id)->firstOrFail();

        return view('admin.community.experts.categories-edit', compact('data'));
    }

    public function categoryUpdate(Request $request, QuestionsCategory $data)
    {

        $this->validate($request, [
            'name' => 'required'
        ]);


            $data->update($request->all());
            flash()->success('Success', 'Updated Successfully');


        return back();
    }

    public function questionEdit($id)
    {

        $data = Questions::where('id', $id)->firstOrFail();

        return view('admin.community.experts.question-edit', compact('data'));
    }

    public function answerEdit($id)
    {

        $data = Answer::where('id', $id)->firstOrFail();

        return view('admin.community.experts.answer-edit', compact('data'));
    }

    public function questionStatus($id)
    {
        $data = Questions::find($id);

        $status = $data->active;

        if($status == 1)
        {
            $data->active = 0;
        }
        else
        {
            $data->active = 1;
        }

        $data->update();

        return back();
    }

    public function answerStatus($id)
    {
        $data = Answer::find($id);

        $status = $data->active;

        if($status == 1)
        {
            $data->active = 0;
        }
        else
        {
            $data->active = 1;
        }

        $data->update();

        return back();
    }
}
