<?php

namespace App\Http\Controllers\Admin;

use App\TaskBid;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BidController extends Controller
{
    /**
     * Return all bids to the view
     * @return mixed
     */
    public function index()
    {
        $bids = TaskBid::all();

        return view('admin.bids', compact('bids'));
    }

    /**
     * Approve or Disapprove bid by admin
     *
     * @param $id
     * @return mixed
     */
    public function status($id)
    {
        $bid = TaskBid::find($id);

        $status = $bid->status;

        if($status == 1)
        {
            $bid->status = 4;
        }
        else
        {
            $bid->status = 1;
        }

        $bid->update();

        return back();
    }

    /**
     * Delete worker bid by admin
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {

        $bid = TaskBid::find($id);
        $bid->delete();

        flash()->success('Success', 'Worker Bid has been successfully deleted!');

        return back();
    }
}
