<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Category;

use Illuminate\Http\Request;

use App\Http\Requests;



class CategoryController extends Controller
{
    /**
     * Returns all the categories to the view
     *
     * @return mixed
     */
    public function index()
    {
        /*$categories = Category::all()->where('parent_id', 0);*/
        $category = new Category();
        $categories = $category->getCategories();
        return view('admin.categories', compact('categories'));
    }

    /**
     * Returns the selected category for the view and returns the parent categories for the dropdown
     *
     * @param Category $category
     * @return mixed
     */
    public function edit(Category $category)
    {
        //$allCategories = $category->getCategories();
        $categories = Category::where('id', '!=', 15)->where('parent_id', 0)->get();

       // return view('admin.category-edit', compact('category'));
        return view('admin.category-edit', ['category'=>$category,
            'categories'=>$categories
        ]);
    }

    /**
     *
     * Add a new category
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name'
        ]);

        $category = new Category;

        $category->name = $request->name;
        $category->description = $request->catDescription;
        $category->parent_id = $request->catParent;
        $category->save();

        flash()->success('Success', 'Category has been added successfully!');

        return back();
    }

    /**
     * Delete a category
     *
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {

        $category = Category::find($id);

        if($category->parent_id == 0)
        {
            $subCategory = Category::all()->where('parent_id', $category->id)->first();
            if($subCategory)
            {
                flash()->overlay('Warning', 'This category contains sub categories. Please delete the sub categories first!', 'error');
            } else
            {
                $category->delete();
                flash()->success('Success', 'Category has been successfully deleted!');
            }

        } else
        {
            $category->delete();
            flash()->success('Success', 'Category has been successfully deleted!');
        }

        return back();
    }

    /**
     * Update the category
     *
     * @param Request $request
     * @param Category $category
     * @return mixed
     */
    public function update(Request $request, Category $category)
    {

        $this->validate($request, [
            'name' => 'required'
        ]);

        $name = $category->name;
        $requestedName = $request->name;

        $categories = Category::all()->where('name', $requestedName)->first();
        if (($categories) && ($categories->name != $name)){
            flash()->warning('Warning', 'Category already exists!');
        }
        else {
            $category->update($request->all());
            flash()->success('Success', 'Category has been updated successfully!');
        }
        
        return redirect()->back();
    }

}
