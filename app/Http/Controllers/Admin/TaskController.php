<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Task;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Returns all the tasks to the view admin tasks
     *
     * @return mixed
     */
    public function index()
    {
        $tasks = Task::all();

        return view('admin.tasks', compact('tasks'));
    }

    /**
     * Updates the status depending on the approval of the admin
     *
     * @param $id
     * @return mixed
     */
    public function status($id)
    {
        $task = Task::find($id);

        $status = $task->status;

        if($status == 0)
        {
            $task->status = 4;
        }
        else
        {
            $task->status = 0;
        }

        $task->update();

        return back();
    }

    /**
     * Delete the Task by admin
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {

        $task = Task::find($id);
        $task->delete();

        flash()->success('Success', 'Requester Task has been successfully deleted!');

        return back();
    }
}
