<?php

namespace App\Http\Controllers;


use App\User;
use App\Task;
use App\Http\Requests;
use DB;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * this controller will be connecting user model, status model with search view.
     * thease function can retrieve data from database according to user's input
     */
    
    public function getResults(Request $request)
    {   //this function will retrieve users profile according to user input
        $query = $request->input('query');
        if(!$query){
            //if no results found
            flash()->info('Sorry!', 'No results found!!!');
            return redirect()->back();
        }

        $tasks = Task::where(DB::raw("topic"),'LIKE',"%{$query}%")
            ->get();
        $users = User::where(DB::raw("name"),'LIKE',"%{$query}%")
            ->get();

        $bidTasks=Task::where('user_id','<>', \Auth::user()->id)->get();


        return view('search.results', compact('tasks','users','bidTasks'));
//            ->with('users', $users)
//            ->with('tasks', $tasks);
    }
}
