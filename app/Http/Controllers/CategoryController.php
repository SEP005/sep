<?php

namespace App\Http\Controllers;
use App\Category;
use App\Task;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    /**
     * Returns all the categories to the view
     * @return mixed
     */
    public function index()
    {
        $category = new Category();
        $categories = $category->getCategories();
        return view('categories.index', compact('categories'));
    }

    public function show($id)
    {
        $tasks = Task::where('category_id', $id)->where('user_id','<>', \Auth::user()->id)->get();

        return view('categories.show', compact('tasks'));
    }
}
