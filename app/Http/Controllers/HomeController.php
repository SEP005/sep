<?php

namespace App\Http\Controllers;

use App\Status;
use App\Http\Requests;
use App\Task;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); //Gets the auth middleware
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * this function will retrieve all the posts, posted by all users from the database
         */
        if (Auth::check()) {
            $statuses = Status::notReply()->where(function ($query) {
                return $query;

            })
                ->orderBy('created_at', 'desc')
                ->paginate(10);
            
            //  Code Written by Nauf
            $latestTasks = Task::where('user_id', '!=' ,\Auth::user()->id )
                ->get()->sortByDesc('id')->take(10); // TO get latest 10 tasks
            return view('timeline.index', compact('statuses','latestTasks'));

            //End of Code Written by Nauf

            // Bala Prev code
//            return view('timeline.index')
//                ->with('statuses', $statuses);
        }
        return view('home');
    }
}
