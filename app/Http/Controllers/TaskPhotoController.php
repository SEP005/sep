<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\TaskPhoto;

use App\Http\Requests;

class TaskPhotoController extends Controller
{
    //
    /**
     * Finds the image record and deletes
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        TaskPhoto::findOrFail($id)->delete();

        return back();

    }
    
}



//        $taskPhoto = TaskPhoto::findOrFail($id);
//
//        $taskPhoto->delete();