<?php

namespace App\Http\Controllers;

use App\ShowcasePhoto;
use Illuminate\Http\Request;

use App\Http\Requests;

class ShowcasePhotoController extends Controller
{
    //
    /**
     * Finds the image record and deletes
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        ShowcasePhoto::findOrFail($id)->delete();

        return back();

    }
}
