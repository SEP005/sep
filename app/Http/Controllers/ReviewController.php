<?php

namespace App\Http\Controllers;


use App\User;
use App\Review;
use App\Http\Requests;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ReviewController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     * this controller will be connecting user model, status model with search view.
     * thease function can retrieve data from database according to user's input
     */

    public function getReview($userId , Request $request)
    {
        $this->validate($request, [
            'comment'=>'required|max:50',
        ]);

        $review = new Review();

        $review->productId = $userId;
        $review->userId = Auth::user()->id;
        $review->comment = $request->input('comment');
        $review->rating = $request->input('rating');
        $review->save();


        Flash()->success('Success!', 'successfully posted');
        return redirect('/home');
    }
}
