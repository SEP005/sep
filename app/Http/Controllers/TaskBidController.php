<?php

namespace App\Http\Controllers;

use App\TaskBid;
use App\TaskProgress;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Task;

class TaskBidController extends Controller
{
    /**
     * Needs to be authenticated
     * TaskBidController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth'); //Gets the auth middleware

        parent::__construct(); //TO delegate up and check parent controller
    }


    /**
     * Display a listing of the resource.
     *
     * tasks/{tasks}/bids
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //To show all bids for a task

        // To get all bids belonging to the task
        if(TaskBid::where('task_id', $id)->where('status',1)->get()->isEmpty())
            // Method returns true if collection is empty
        {
            flash()->error('Oops!', 'Theres no bids for this task');
            return back();
        }
        else    
        {
            $taskBids = TaskBid::where('task_id', $id)->where('status', 1)->get(); // user_id == Auth user Id

            $TaskBidCount = Task::bidCount($id);//Calculates no of bids for task and returns


            return view('bids.index', ['taskBids' => $taskBids,
                'TaskBidCount' => $TaskBidCount,
            ]);
        }
        // $task=Task::where('user_id', '!=' , 2)->orWhereNull('user_id');
        //return view('tasks.index', ['tasks'=>$task]);

    }

    /**
     * Display a listing of the resource.
     *
     * tasks/{tasks}/workerbids
     * @return \Illuminate\Http\Response
     */
    public function workerBids()
    {
        //To show all bids for a task

        if(TaskBid::where('user_id', \Auth::user()->id)->get()->isEmpty() == false)
        {
            // user_id == Auth user Id
            $workerBids = TaskBid::where('user_id', \Auth::user()->id)->get();

            return view('bids.workerbids', ['workerBids' => $workerBids]);
        }else
        {
            flash()->error('Oops!', 'You have not made any bids');

            return back();
        }
    }
    
    

    /**
     * Show Create a bid for the task
     * Show the form for creating a new resource.
     *
     * tasks/{tasks}/bids/create
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        $task = Task::find($id);

        $ldate = date('Y-m-d');


        return view('bids.create', compact('task','ldate'));
    }

    /**
     * Store a newly created resource in storage.
     * Store the bid in database
     *
     * tasks/{tasks}/bids
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        // Validation of the values passed from form
        $this->validate($request, [
            'task_id' => 'required',
            'user_id' => 'required',
            'bid_price' => 'required|numeric|max:500000',
            'deliver_in' => 'required|date',
            'proposal' => 'required',

        ]);

        // Create the bid and store in database
        if (TaskBid::create($request->all()))
        {
            $task = Task::find($id);
            $taskBidCount = $task->bid_count;
            $taskBidCount = $taskBidCount + 1;

            // For each bid the bid_count is incremented in the model Task
            if (Task::where('id', $id)->update([
                'bid_count' => $taskBidCount,
            ]))
            {
                //Flash Message
            flash()->success('Success!', 'You have placed a bid');
            }
        } else
        {
            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }

        $latestBid=TaskBid::getLatestBid(); // Get latest Bid

        $taskProgress = new TaskProgress();
        $taskProgress->bid_id = $latestBid->id;
        $taskProgress->worker_id = $latestBid->user_id; // aka Bid Owner
        $taskProgress->requester_id = $latestBid->getTask->user_id; // aka Task Owner
        $taskProgress->save();

        return redirect("workerTasks");
    }

    /**
     * Display the specified resource.
     *
     * tasks/{tasks}/bids/{bids}
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($taskId,$id)
    {

        //
        // $task = Task::where(compact('id'))->first();
        $task = Task::taskLocatedAt($taskId,$id);
        $bid = TaskBid::taskBidLocatedAt($id);


        // return $task;
        return view('bids.show', compact('task','bid'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * tasks/{tasks}/bids/{bids}/edit
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * tasks/{tasks}/bids/{bids}
     * @param  \Illuminate\Http\Request $request
     * @param  int $id which is bid Id
     * @return \Illuminate\Http\Response
     */
    public function update($taskId,$bidId,Request $request)
    {
        $this->validate($request, [
            'bid_price' => 'numeric|required|max:500000',
            'deliver_in' => 'date|required',
            'proposal' => 'required',

        ]);

        if( TaskBid::where('id', $bidId)->update([
            'bid_price' => $request->bid_price,
            'deliver_in' => $request->deliver_in,
            'proposal' => $request->proposal,
            'status' => 1,
        ])){

            //Flash Message
            flash()->success('Success!', 'Bid is successfully updated');
        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     * tasks/{tasks}/bids/{bids}
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($taskId,$id)
    {
        $taskBid = TaskBid::find($id);

        $task = Task::find($taskId);
        $task->bid_count = $task->bid_count - 1;


        // For each bid the bid_count is incremented in the model Task

            // If deleted

        if($taskBid->delete())
        {
            // If bid gets success fully deleted

            if ($task->save())
            {
                // If the task's bid_count is decremented
                flash()->success('Success!', 'Bid has been successfully deleted!');
            }
        }


        return redirect('/taskBids');
    }

    /**
     * Store whether it is accepted or rejected
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function selection($id,Request $request)
    {
        $bidStatus = TaskBid::bidStatusCheck($id); // Check the status of bid

        if($bidStatus== 1 || $bidStatus== 2 || $bidStatus== 3   )//If its already default or accepted or rejected
        {

            if($request['status']==2) //Accept
            {
                // Update the status of bid
                if( TaskBid::where('id', $request->id)->update([
                    'status' => 2,
                ])){
                    flash()->success('Accepted!', 'You have accepted the bid');
                }
                else{
                    flash()->error('Oops!', 'There seems to be an error!');
                    return redirect()->back();
                }
            } //IF status == 2 END
//
            if($request['status']==3) //Reject
            {
                // Update the status of bid
                if( TaskBid::where('id', $request->id)->update([
                    'status' => 3,
                ]))
                {
                    flash()->success('Rejected!', 'You have rejected the bid');
                }
                else
                {
                    flash()->error('Oops!', 'There seems to be an error!');
                    return redirect()->back();
                }
            }
        }
        return redirect('/taskBids');
    }

}
