<?php
namespace App\Http\Controllers;

use App\Status;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class statusController extends Controller{

    /**
     * @param Request $request
     * @return mixed
     * to get advertisement post page
     */
    public function getPostStatus(Request $request)
    {
        return view('timeline.postStatus');
    }

    /**
     * @param $statusId
     * @return mixedto get drag and drop image upload function
     */
    public function getAddPhotos($statusId)
    {
        $status = Status::where('id', $statusId)->first();
        return view('timeline.addPhotos')
            ->with('status',$status);
    }

    public function postStatus(Request $request)
    {
        $this->validate($request, [
            'heading'=>'required|max:50',
            'status'=>'required|max:1000',
			 'role'=>'required',
        ]);

        Auth::user()->statuses()->create([
            'body'=>$request->input('status'),
			'role'=>$request->role,
            'heading'=>$request->input('heading'),
            'body'=>$request->input('status'),
            'role'=>$request->role,
            'tags'=>$request->input('tags'),
        ]);
		Flash()->success('Success!', 'successfully posted');
        return back();
    }
    public function updateStatus($statusId, Request $request)
    {
        $this->validate($request, [
            'edit-body'=>'required|max:1000',
        ]);
        
        Auth::user()->statuses()->update([
            'body'=>$request->input('edit-body'),
        ]);
        Flash()->success('Success!', 'successfully posted');
        return back();
    }

    /**
     * @param Request $request
     * @param $statusId
     * @return mixedto upload function
     */
    public function uploadPhotos($statusId,Request $request){

        $this->validate($request,[
            'photo' => 'required|mimes:jpg,jpeg,png,bmp'
        ]);
        
        $file = $request->file('photo');
//        var_dump($file->getClientOriginalName());
        $name = time().$file->getClientOriginalName();
        $file->move('advertisement/images',$name);

        $status = Status::Find($statusId)->first();
        $status->photos()->create(['image' => "/advertisement/images{$name}"]);
    }

    public function postReply(Request $request, $statusId)
    {
        $this->validate($request,[

            'reply' => 'required',
            
        ]);
//        $this->validate($request, [
//            "reply-{$statusId}" =>'required|max:100',
//            ], [
//            'required'=> 'The reply body is required'
//        ]);

//        Auth::user()->statuses()->create([
//            'body'=>$request->input('status'),
//        ],

        $status= Status::notReply()->find($statusId);
        if(!$status){
            return redirect()->
                route('home');
        }

        if(!Auth::user()->isFriendsWith($status->user) && Auth::user()->id !== $status->user->id)
        {
            return redirect()->
                route('home');
        }

        $reply = Status::create([
            'body' => $request->input("reply"),

        ])->user()->associate(Auth::user());

        $status->replies()->save($reply);

        return redirect()
            ->back();
    }

    /*
     * function responsible for getting like for a post
     */
    public function getLike($statusId)
{
    $status = Status::find($statusId);
    if (!$status) {
        return redirect()->route('home');
    }

    if (Auth::user()->hasDislikedStatus($status)) {
        return redirect()->back();
    }

    if (Auth::user()->hasLikedStatus($status)) {
        return redirect()->back();
    }
    $like = $status->likes()->create([]);
    Auth::user()->likes()->save($like);

    return redirect()->back();
}
    public function getUndo($statusId)
    {
        $status = Status::find($statusId);
        $status->likes()->delete();
        $status->dislikes()->delete();
        Flash()->info('Info!', ' You can like or Dislike now');
        return redirect()->back();
    }

    /**
     * @param $statusId
     * @return mixed
     * function responsible for getting dislike for the post
     */
	public function getDislike($statusId)
    {
        $status = Status::find($statusId);
        if (!$status) {
            return redirect()->route('home');
        }

        if (Auth::user()->hasLikedStatus($status)) {
            return redirect()->back();
        }
        
        if (Auth::user()->hasDislikedStatus($status)) {
            return redirect()->back();
        }
        $dislike = $status->dislikes()->create([]);
        Auth::user()->dislikes()->save($dislike);

        return redirect()->back();
    }

    /**
     * @param $statusId
     * @return mixed Delete post function
     */
    public function getDelete($statusId)
    {
        $status =   Status::where('id', $statusId)->first();
        $status->delete();
        Flash()->success('Success!', 'successfully Deleted');
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed edit post function
     */
    public function postEditPost(Request $request)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);
        $post = Status::find($request['id']);
        
        $post->body = $request['body'];
        $post->update();
        return response()->json(['new_body' => $post->body], 200);
    }
}
?>