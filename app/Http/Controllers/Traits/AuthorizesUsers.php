<?php

namespace App\Http\Controllers\Traits;

use App\Showcase;
use App\Task;
use Illuminate\Http\Request;
trait AuthorizesUsers
{

    /**
     * To check whether user created the task
     * @param Request $request
     * @return mixed
     */
    public function userCreatedTask(Request $request)
    {
        return Task::where(
            [
                'id' => $request->id,
                'user_id' => \Auth::user()->id
            ])->exists();//Not grabbing but checks if the record matches
    }

    /**
     * To check whether user created the Showcase
     * @param Request $request
     * @return mixed
     */
    public function userCreatedShowcase(Request $request)
    {
        return Showcase::where(
            [
                'id' => $request->id,
                'user_id' => \Auth::user()->id
            ])->exists();//Not grabbing but checks if the record matches
    }

    /**
     * A function so that if unauthorized returns 403 status code
     * to indicate that the server can be reached and understood the request,
     * but refuses to take any further action
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|
     * \Illuminate\Http\RedirectResponse|
     * \Illuminate\Routing\Redirector|
     * \Symfony\Component\HttpFoundation\Response
     */
    public function unauthorized(Request $request)
    {
        if ($request->ajax()) {

            return response(['message' => 'No way.'], 403);

        }
        flash()->error('Oops!', 'You are not allowed to view this page');

        return redirect('/home');

    }

}