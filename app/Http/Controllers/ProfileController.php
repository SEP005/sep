<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Review;
use App\Task;
use App\TaskBid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use App\Status;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class ProfileController extends Controller
{
    public function getProfile($name)
    {
        $user = User::where('name', $name)->first();
        $reviews = Review::where('productId',Auth::user()->id)->get();

        if(!$user){
            abort(404);
        }
        $statuses= $user->statuses()->notReply()->get();

        $tasksCount = Task::where('user_id', Auth::user()->id)->count();
        $bidsCount = TaskBid::where('user_id', Auth::user()->id)->count();
        $friendsCount = Friend::where('user_id', Auth::user()->id)->where('accepted', 1)->count();

        return view('profile.index')
            ->with('user',$user)
            ->with('statuses',$statuses)
            ->with('authUserIsFriend', Auth::user()->isFriendsWith($user))
            ->with('tasksCount', $tasksCount)
            ->with('friendsCount', $friendsCount)
            ->with('bidsCount', $bidsCount)
            ->with('reviews',$reviews);

    }
    public function getReview($userId)
    {
        $reviews = Review::where('productId', $userId)->get();
        
        return view('profile.index')
            ->with('reviews',$reviews);

    }
//    rending a updating form to user
    public function getEdit()
    {
        return view('profile.edit');
    }

    /**
     * this function is will be responsible for update the new details to the user's table in database.
     * 
     */
    
    public function postEdit(Request $request)
    {   //validation for update form
        $this->validate($request,[
            'firstname' =>'required|alpha|max:50',
            'lastname' =>'required|alpha|max:50',
            'address' =>'required',
            'street' =>'required',
            'city' =>'required',
            'country' =>'required',
            'website'=>'alpha_num|max:50',
            'bio'=>'alpha|max:100',

        ]);
        //updating user table
        Auth::user()->update([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'address' =>$request->input('address'),
            'street' =>$request->input('street'),
            'city' =>$request->input('city'),
            'country' =>$request->input('country'),
            'website'=>$request->input('website'),
            'bio'=>$request->input('bio'),

            
        ]);
        flash()->success('Success!', 'success fully updated');
        return redirect()->route('profile.edit');
    }
    public function postSaveAccount(Request $request)
    {

        $user = Auth::user();
        $old_name = $user->name;
        $file = $request->file('image');
        $filename = $request['name'] . '-' . $user->id . '.jpg';
        $old_filename = $old_name . '-' . $user->id . '.jpg';
        $update = false;
        if (Storage::disk('local')->has($old_filename)) {
            $old_file = Storage::disk('local')->get($old_filename);
            Storage::disk('local')->put($filename, $old_file);
            $update = true;
        }
        if ($file) {
            Storage::disk('local')->put($filename, File::get($file));
        }
        if ($update && $old_filename !== $filename) {
            Storage::delete($old_filename);
        }
        flash()->success('Success!', 'Profile picture success fully updated');
        return redirect()->back();
    }

    public function getUserImage($filename)
    {
        $file = Storage::disk('local')->get($filename);
        return new Response($file, 200);
    }
}
