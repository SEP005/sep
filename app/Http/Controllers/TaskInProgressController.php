<?php

namespace App\Http\Controllers;

use App\Milestone;
use App\ReceiveAccount;
use App\TaskBid;
use App\TaskDiscussion;
use App\TaskProgress;
use App\UserPayment;
use Illuminate\Http\Request;

use App\Http\Requests;

/**
 * Class TaskInProgressController
 * @package App\Http\Controllers
 *
 * This class would handle the milestones, TaskDiscussions 
 */
class TaskInProgressController extends Controller
{

    /**
     * Needs to be authenticated
     * TaskInProgressController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth'); //Gets the auth middleware

        parent::__construct(); //TO delegate up and check parent controller
    }

    /**
     * Display a listing of the resource.
     * Gets all the Assigned Tasks and show them
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Gets WorkerAccepted Bids
        $acceptedBids = TaskBid::getAcceptedBids();//->where('user_id', \Auth::user()->id);

        $receiver = ReceiveAccount::where('user_id', \Auth::user()->id)->get()->first();

        
        return view('TaskInProgress.index', compact('acceptedBids','receiver'));

    }

    /**
     * Display a listing of the payments.
     * Gets all the Assigned Tasks and show them
     * @return \Illuminate\Http\Response
     */
    public function viewpayments()
    {
        //Gets WorkerAccepted Bids
        $acceptedBids = TaskBid::getAcceptedBids();//->where('user_id', \Auth::user()->id);

        $receiver = ReceiveAccount::where('user_id', \Auth::user()->id)->get()->first();

        $payments = UserPayment::where('receiver_id',\Auth::user()->id)->orWhere('sender_id',\Auth::user()->id)->get();

        // To get payment receiveed
        $paymentsRecieved = UserPayment::where('receiver_id',\Auth::user()->id)->sum('payment_amount');

        // To get payments made
        $paymentsSpent = UserPayment::where('sender_id',\Auth::user()->id)->sum('payment_amount');

        return view('TaskInProgress.viewpayments',
            compact('acceptedBids','receiver','payments','paymentsSpent','paymentsRecieved'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate the inputs
        $this->validate($request, [
            'user_id' => 'required',
            'details' => 'required',
            'bid_id' => 'required'
        ]);

        // Store the data in database
        if( TaskDiscussion::create($request->all()) )
        {

            //Flash Message
            flash()->success('Success!', 'you have successfully posted');
            return redirect()->back();
        }
        else
        {
            flash()->error('Oops!', 'Please check whether the fields are properly entered!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $taskProgress=TaskProgress::where('bid_id',$id)->get()->first();
        $taskDiscussions=TaskDiscussion::where('bid_id',$id)->orderBy('created_at', 'desc')->get();
        $milestones= Milestone::where('bid_id',$id)->orderBy('created_at', 'desc')->get();

        $milestonesCompletedCount = Milestone::where('bid_id',$id)->where('status',2)->count();
        $milestonesCompletedStatus = $milestones->count() == $milestonesCompletedCount;


        return view('TaskInProgress.show', compact('taskProgress','taskDiscussions','milestones','milestonesCompletedStatus'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validation rules applied for the recieved form input fields
        $this->validate($request, [
            'user_id' => 'required',
            'details' => 'required',
            'bid_id' => 'required'
        ]);

        // TaskDiscussion update.
        if( TaskDiscussion::where('id', $id)->update([
            
            'user_id' => $request->user_id,
            'details' => $request->details,
            'bid_id' => $request->bid_id,
            
        ]))
        {
            //Flash Message
            flash()->success('Success!', 'Item is successfully updated');
            return redirect()->back();
        }
        else
        {
            flash()->error('Oops!', 'Please check whether the fields are properly entered!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(TaskDiscussion::find($id)->delete())
        {
        flash()->success('Success!', 'The post has been deleted');
        return redirect()->back();
        }
        else
        {
            flash()->error('Oops!', 'There was a problem deleting the post');
            return redirect()->back();
        }
    }
}
