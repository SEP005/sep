<?php

namespace App\Http\Controllers;

use App\Messages;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{
    protected $receiverID;

    public function __construct()
    {
        $this->middleware('auth'); //Gets the auth middleware

        parent::__construct(); //TO delegate up and check parent controller
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $messages = Messages::groupBy('receiver_id')
            ->where('sender_id', Auth::user()->id)
            ->orWhere('receiver_id', Auth::user()->id)
            ->get();

        return view('messages.index', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        Messages::create($request->all());

        flash()->success('Success', 'Message has been sent successfully!');

        return redirect()->back();
    }

    public function storeMessage(Request $request) {
        Messages::create($request->all());
    }

    public function setReceiverID($id) {
        $this->receiverID = $id;
    }

    public function getReceiverID() {
        return $this->receiverID;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->setReceiverID($id);

        $receiver = User::where('id', $id)->firstOrFail();
        $rname = $receiver->name;
        $rid = $receiver->id;

        $messages = Messages::where('sender_id', Auth::user()->id)
            ->where('receiver_id', $id)
            ->orWhere(function ($query) {
                $query->where('sender_id', $this->getReceiverID())
                    ->where('receiver_id', Auth::user()->id);
            })
            ->get();

        return view('messages.show', compact('rname', 'rid', 'messages'));
    }

    /*public function showMessages($sid, $rid)
    {
        $data = Messages::where('receiver_id', $rid)->where('sender_id', $sid)->get();
        return view('messages.show', compact('data'));
    }*/

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
