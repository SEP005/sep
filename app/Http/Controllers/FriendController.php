<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use DB;
use Illuminate\Http\Request;


class FriendController extends Controller
{
    /**
     * this controller is responisble for to link friend profile views and user model.
    all functions which are related to the connect users and views are listed here....
     */
    
    public function getIndex()
    {

        $friends = Auth::user()->friends();
        $requests = Auth::user()->friendRequests();

        return view('friend.index')
            ->with('friends',$friends)
            ->with('requests',$requests);
    }
    //function to add a user as a friend
    public function getAdd($name)
    {
        $user = User::where('name',$name)->first();
        if(!$user){
            return redirect()
                ->route('home')
               ->with('flash_message','That user could not be found');
        }

        if(Auth::user()->id === $user->id)
        {
            return redirect()
                ->route('home');
        }

        if(Auth::user()->hasFriendRequestsPending($user) || 
            $user->hasFriendRequestsPending(Auth::user())){
            flash()->info('Info!', 'Connection request pending');
            return redirect()->route('profile.index',['name'=>$user->name]);
        }

        if(Auth::user()->isFriendsWith($user))
        {
            flash()->info('Info!', 'You are already connected');
            return redirect()
                ->route('profile.index',['name'=>$user->name]);
        }

        Auth::user()->addFriend($user);
        flash()->success('Success', 'Friend request sent.');
        return redirect()
            ->route('profile.index',['name'=>$user->name]);
    }
    //function which allows one user to accept another user's request
    public function getAccept($name)
    {
        $user = User::where('name',$name)->first();

        if(!$user){
            flash()->warning('Sorry!!', 'That user could not be found.');
            return redirect()
                ->route('home');
        }

        if(!Auth::user()->hasFriendRequestsReceived($user)){
            flash()->info('Info!', 'Connetion request already pending..');
            return redirect()
                ->route('home');
            //->with('flash_message','Friend request already pending.');
        }
        
        Auth::user()->acceptFriendRequest($user);
        //if user's request succesfully accepted success message will pop up
        flash()->success('Success!', 'Friend request accepted.');
        return redirect()
            
            ->route('profile.index',['name'=>$name]);

    }
    //function which allows one user to delete another user's request
    public function getDelete($name)
    {
        $user = User::where('name',$name)->first();

        Auth::user()->deleteFriendRequest($user);
        //if user's request succesfully deleted success message will pop up
        flash()->success('Success!', 'Friend request deleted.');
        return redirect()

            ->route('profile.index',['name'=>$name]);

    }
}
   