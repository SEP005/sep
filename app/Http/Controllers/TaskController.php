<?php

namespace App\Http\Controllers;
use App\Category;
use App\Task;
use App\User;
use Carbon\Carbon;
use App\TaskPhoto;

//use Illuminate\Support\Facades\Auth;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Auth;


use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;
use App\Http\Requests;
use App\Http\Controllers\Traits\AuthorizesUsers;


class TaskController extends Controller
{
    //
    use AuthorizesUsers;

    /**
     * Needs to be authenticated
     * TaskController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth'); //Gets the auth middleware

        parent::__construct(); //TO delegate up and check parent controller
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //To Show all task posted by requester
        //
        $bidTask=Task::where('user_id', '!=' , 2)->orWhereNull('user_id');

        $task = Task::all()->where('user_id',\Auth::user()->id );

        return view('tasks.index', ['tasks'=>$task,'$bidTasks'=>$bidTask]);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //To show the create form

       // flash()->overlay('Welcome Aboard','Thankyou for signing up!');


//        $category = Category::all();
//        $category = $category->getCategories();

        $category = new Category();
        $category = $category->getCategories();
        return view('tasks.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param TaskRequest|\Illuminate\Http\TaskRequest $taskRequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,TaskRequest $taskRequest)
    {
        
        //Validation of the form is done in TaskRequest

        // Input the task to database
      if( Task::create($request->all()) ) {

          //Flash Message
          flash()->success('Success!', 'Task is successfully posted');
      }else{

          flash()->error('Oops!', 'Please check whether the fields are properly entered!');

          return redirect()->back();

      }
        $taskId=Task::all()->sortByDesc('id')->first();

        return redirect('/tasks/'.$taskId->id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
       // $task = Task::where(compact('id'))->first();
        $task = Task::taskLocatedAt($id);

       // return $task;
        return view('tasks.show', compact('task'));

    }

    /**
     * @param $id
     * @param Request $request
     * @return string
     * validates the photo
     * makes the photoInstance by calling the method in Model Photo
     */
    public function addPhoto($id,Request $request)
    {
        $this->validate($request,
            [
             'photo' => 'required|mimes:jpg,jpeg,png,bmp'

            ]);

            if(! $this->userCreatedTask($request)) { //If user didn't create task

                // make use of the trait AuthorizesUsers to
                // indicate server wont take any action
                return $this->unauthorized($request);
            }

        $photo = $this->makePhoto($request->file('photo'));

        Task::taskLocatedAt($id)->addTaskPhoto($photo); //Add photo which belongs to task
        
        return 'Done';
    }



    /**
     * TO make photo instance
     * @param UploadedFile $file
     * @return $this
     */
    protected function makePhoto(UploadedFile $file)
    {
       //To build up the photo instance and to store in proper file directory
       return TaskPhoto::named($file->getClientOriginalName())
           ->move($file);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$category = Category::all();

        $category = new Category();
        $category = $category->getCategories();


        $task = Task::taskLocatedAt($id);
        
        return view('tasks.edit', compact('task','category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,TaskRequest $taskRequest)
    {
        //

        // Input the task to database
        if( Task::where('id', $id)->update([
            'category_id' => $request->category_id,
            'topic' => $request->topic,
            'details' => $request->details,
            'price' => $request->price,
            'user_id' => $request->user_id,
            'skills' => $request->skills,
            'video_path' => $request->video_path
            ])){

            //Flash Message
            flash()->success('Success!', 'Task is successfully updated');
        }else{

            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();

        }

        //Redirect to landing page
        // return redirect()->back();
        return redirect('/tasks');



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Task::findOrFail($id)->delete();

        flash()->success('Deleted', 'The Task post is deleted');
        return back();

    }

    /**
     * Old version Below
     * Delete the code below when ready
     *
     */


    public function showAddPage()
    {
        $category = Category::all();
        // $category->subCategories();
        //  $category->load('subCategories'); //Eager load like the statment below

        return view('request.addrequest', compact('category'));
    }

    /** Bids Section */

    /**
     * TO display all tasks where a worker can bid
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function workerIndex()
    {
        $bidTasks=Task::where('user_id','<>', \Auth::user()->id)->get();

        return view('tasks.workerIndex', ['bidTasks'=>$bidTasks]);
    }

    /**
     * Returns to view with the array tasks which contains all bids
     * more than or equal to one for a task
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function taskBids()
    {
        $task = Task::where('user_id',\Auth::user()->id )->where('bid_count','>=',1)->get();

        return view('tasks.viewbids', ['tasks'=>$task]);
    }


    /** End of Bids Section */









//    public function addtask(Request $request)
//    {
//
//        $message = "There was an error";
//
//
//        $this->validate($request,[
//
//            'category' => 'required',
//            'topic' => 'required',
//            'details' => 'required',
//            'price' => 'required',
//            'user_id' => 'required'
//
//        ]);
//
//        $task = new Task();
//        $task->topic = $request->topic;
//        $task->details = $request->details;
//        $task->user_id = $request->user_id;//Auth::id();
//        $task->category_id = $request->category;
//        $task->subcategory_id = 1;
//        $task->price = $request->price;
//        $task->status = 1;
//
//        if( $task->save()) { //TO write to database
//            $message = "Category Successfully created";
//        }
//
////        flash()->success('HIITITLE','Thisis a mesage');
//
//        //return back()->with(['message'=> $message]);
//        // return redirect()->route('request')->with(['message'=> $message]);//Chains method with allows  to pass a message //Message array passed
//
//        return redirect()->route('requestshow')->with(['message'=> $message]);
//
//    }

// add photo comments down below

    //    $file = $request->file('photo'); //Get uploaded file instance

    //Its done this way so that there will be no same files overriding eachother
    //  $name = Carbon::now().rand(1,100).$file->getClientOriginalName();

    //$name = str_replace(' ','-', $name);

    //$file->move('tasks/photos',$name);

    // $task = Task::where(compact('id'))->first();

    //$task->photos()->create(['path' => "/tasks/photos/{$name}"]);

    // $photo = TaskPhoto::fromForm($request->file('photo'))->store(); //To build up the photo instance //To store in proper file directery




}

