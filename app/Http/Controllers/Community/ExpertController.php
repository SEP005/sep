<?php

namespace App\Http\Controllers\Community;

use App\Answer;
use App\FlagAnswers;
use App\FlagQuestions;
use App\LikeAnswers;
use App\LikeFlags;
use App\LikeQuestions;
use App\Questions;
use App\QuestionsCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExpertController extends Controller
{
    public function index() {
        $cats = QuestionsCategory::all();
        return view('community.experts.index', compact('cats'));
    }

    public function create() {
        $cats = QuestionsCategory::all();
        return view('community.experts.create', compact('cats'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'question' => 'required',
            'questionCategory_id' => 'required'
        ]);

        $data = new Questions;

        $data->user_id = Auth::user()->id;
        $data->question = $request->question;
        $data->information = $request->information;
        $data->questionCategory_id = $request->questionCategory_id;
        $data->likes = 0;

        $data->save();

        flash()->success('Success', 'Your Question has been posted!');

        return view('community.experts.index');
    }

    public function show($id) {
        $question = Questions::where('id', $id)->first();
        
        return view('community.experts.show', compact('question'));
    }

    public function storeAnswer(Request $request) {
        $this->validate($request, [
            'answer' => 'required',
        ]);

        $data = new Answer;

        $data->user_id = Auth::user()->id;
        $data->question_id = $request->question_id;
        $data->answer = $request->answer;

        $data->save();

        flash()->success('Success', 'Your Answer has been posted!');

        return redirect()->back();
    }

    public function toggleLike(Request $request) {
        $data = LikeQuestions::where('question_id', $request->question_id)->where('user_id', $request->user_id)->first();

        if($data){
            $data->delete();
            return 'liked';
        }
        else {
            $d = new LikeQuestions;
            $d->create($request->all());
            return 'not';
        }
    }

    public function toggleAnswerLike(Request $request) {
        $data = LikeAnswers::where('answer_id', $request->answer_id)->where('user_id', $request->user_id)->first();

        if($data){
            $data->delete();
            return 'liked';
        }
        else {
            $d = new LikeAnswers;
            $d->create($request->all());
            return 'not';
        }
    }

    public function reportAnswer(Request $request) {
        $data = FlagAnswers::where('answer_id', $request->answer_id)->where('user_id', $request->user_id)->first();

        if($data){
            $data->delete();
            return 'liked';
        }
        else {
            $d = new FlagAnswers;
            $d->create($request->all());
            return 'not';
        }
    }

    public function reportQuestion(Request $request) {
        $data = FlagQuestions::where('question_id', $request->question_id)->where('user_id', $request->user_id)->first();

        if($data){
            $data->delete();
            return 'liked';
        }
        else {
            $d = new FlagQuestions;
            $d->create($request->all());
            return 'not';
        }
    }

    public function myIndex() {
        $data = Questions::where('user_id', Auth::user()->id)->get();
        return view('community.experts.my', compact('data'));
    }

    public function myAnswerIndex() {
        $data = Answer::where('user_id', Auth::user()->id)->get();
        return view('community.experts.myAnswers', compact('data'));
    }
}
