<?php

namespace App\Http\Controllers;

use App\Showcase;
use App\Category;

use App\ShowcasePhoto;
use Carbon\Carbon;
use Illuminate\Http\Request;


use App\Http\Requests;
use App\Http\Requests\ShowcaseRequest;
use App\Http\Controllers\Traits\AuthorizesUsers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Console\Input;

use Illuminate\Support\Facades\Validator;


class ShowcaseController extends Controller
{
    use AuthorizesUsers;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $showcase = Showcase::all();//->where('user_id',\Auth::user()->id );

        
        
        return view('showcase.index',['showcases'=>$showcase]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category = new Category();
        $category = $category->getCategories();
        return view('showcase.create', compact('category'));
    }

    /**
     * This store method is used to store the showcase
     * @param Request $request
     * @param ShowcaseRequest $showcaseRequest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request,ShowcaseRequest $showcaseRequest)
    {
        // Get the Showcase model's object
        $showcase = new Showcase;

        // Get the form values and assign to object
        $showcase->title = $showcaseRequest->title;
        $showcase->description = $showcaseRequest->description;
        $showcase->budget = $showcaseRequest->budget;
        $showcase->user_id = $showcaseRequest->user_id;
        $showcase->currency_type = $showcaseRequest->currency_type;
        $showcase->category_id = $showcaseRequest->category_id;

        //Check if user has embedded a videp
        if($showcaseRequest->video_path)
        {
            $showcase->video_path = $showcaseRequest->video_path;
        }

        //Info:: Storing the pdf file
        if($showcaseRequest->pdfFile) // If pdf file submitted
        {
            //To receive the file
            $file = $showcaseRequest->pdfFile;

            //Base Directory where pdf file would be saved
            $baseDirectory = 'showcaseFiles/files';
            $filename = $file->getClientOriginalName();// To get the uploaded filename

            // Edit the file name so when stored, It would contain the date it was uploaded
            $document_name = sprintf("%s-%s", Carbon::now() . rand(1, 100), $filename);
            $document_name = str_replace(' ', '-', $document_name);

            // Move the file uploaded to the baseDirectory defined
            $upload_success = $file->move($baseDirectory, $document_name);

            if ($upload_success) {
                $doc_path = sprintf("%s/%s", $baseDirectory, $document_name);// TO get store location in directory
                $showcase->pdf_path = $doc_path; // Add the file path to the pdf_path in database
            }
        }

        //Store the showcase
        if($showcase->save())
        {
            flash()->success('Success!', 'You have successfully added your showcase');

            $showcaseId=Showcase::all()->sortByDesc('id')->first();
            return redirect('/showcase/'.$showcaseId->id);
        }
        else
        {
            flash()->error('Oops!', 'Please check whether the fields are properly entered!');

            return redirect()->back();
        }
    }


    /**
     * Display the specified resource.
     * | GET|HEAD  | showcase/{showcase} | showcase.show
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $showcase = Showcase::showcaseLocatedAt($id);
        return view('showcase.show', compact('showcase'));


    }

    /**
     * Show the form for editing the specified resource.
     * showcase/{showcase}/edit
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = new Category();
        $category = $category->getCategories();


        $showcase = Showcase::showcaseLocatedAt($id);

        return view('showcase.edit', compact('showcase','category'));
    }

    /**
     * Update the specified resource in storage.
     * PUT|PATCH | showcase/{showcase}| showcase.update
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, ShowcaseRequest $showcaseRequest)
    {
        // Showcase request Class checks for validation of the showcase
        // If pdf file is submitted
        if($showcaseRequest->pdfFile)
        {
            $file = $showcaseRequest->pdfFile; // Get the posted pdf file
            //Base Directory where pdf file would be saved
            $baseDirectory = 'showcaseFiles/files';
            $filename = $file->getClientOriginalName();

            // Edit the file name so when stored, It would contain the date it was uploaded
            $document_name = sprintf("%s-%s", Carbon::now() . rand(1, 100), $filename);
            $document_name = str_replace(' ', '-', $document_name);

            // Move the file uploaded to the baseDirectory defined
            $upload_success = $file->move($baseDirectory, $document_name);

            if ($upload_success)
            {
                $doc_path = sprintf("%s/%s", $baseDirectory, $document_name);// TO get store location in directory
                Showcase::where('id', $id)->update(['pdf_path' => $doc_path ]); // Updates the path
            }
            else
            {
                flash()->error('Oops!', 'There was a problem with moving the file to directory.
                 Maybe Check your file permission');
            }
        }
        // Input the Showcase to database
        if( Showcase::where('id', $id)->update([
            'category_id' => $showcaseRequest->category_id,
            'title' => $showcaseRequest->title,
            'description' => $showcaseRequest->description,
            'budget' => $showcaseRequest->budget,
            'user_id' => $showcaseRequest->user_id,
            'currency_type' => $showcaseRequest->currency_type,
            'video_path' => $showcaseRequest->video_path,

        ]))
        {
            flash()->success('Success!', 'Showcase is successfully updated');
        }
        else{
            flash()->error('Oops!', 'Please check whether the fields are properly entered!');
            return redirect()->back();
        }
        return redirect('/showcase');
    }

    /**
     * Remove the specified resource from storage.
     * DELETE | showcase/{showcase} | showcase.destroy
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Showcase::findOrFail($id)->deleteFile();

        flash()->success('Deleted', 'The Showcase is deleted');
        return back();
    }

    

    public function myShowcases()
    {
        //
        $showcase = Showcase::all()->where('user_id',\Auth::user()->id );

        return view('showcase.myshowcases', ['showcases'=>$showcase]);
    }

    public function deletePdf($id)
    {
        $showcase = Showcase::find($id);

        if( Showcase::where('id', $id)->update(['pdf_path' => ''])) {
            \File::delete([ //To delete from the folder
                $showcase->pdf_path,
            ]);

         //   flash()->success('Deleted', 'The File is deleted');
        }
    }

    //Info: Showcase Photo functions
    /**
     * @param $id
     * @param Request $request
     * @return string
     * validates the photo
     * makes the photoInstance by calling the method in Model Photo
     */
    public function addPhoto($id,Request $request)
    {
        $this->validate($request,
            [
                'photo' => 'required|mimes:jpg,jpeg,png,bmp'

            ]);

        if(! $this->userCreatedShowcase($request)) { //If user didn't create showcase

            // make use of the trait AuthorizesUsers to
            // indicate server wont take any action
            return $this->unauthorized($request);
        }

        $photo = $this->makePhoto($request->file('photo'));

        Showcase::showcaseLocatedAt($id)->addShowcasePhoto($photo); //Add photo which belongs to showcase

        return 'Done';
    }
    /**
     * TO make photo instance
     * @param UploadedFile $file
     * @return $this
     */
    protected function makePhoto(UploadedFile $file)
    {
        //To build up the photo instance and to store in proper file directory
        return ShowcasePhoto::named($file->getClientOriginalName())
            ->move($file);
    }




}
