<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionsCategory extends Model
{
    protected $fillable = ['name', 'description'];

    public function pathEdit()
    {
        return '/admin/community/experts/category/' . $this->id . '/edit';
    }
}
