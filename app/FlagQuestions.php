<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlagQuestions extends Model
{
    protected $fillable = ['question_id', 'user_id', 'reason'];

    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function question()
    {
        return $this->belongsTo('App\Questions','question_id');
    }

    public function pathEdit()
    {
        return '/admin/community/experts/question/' . $this->id . '/edit';
    }
}
