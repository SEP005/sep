<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlagAnswers extends Model
{
    protected $fillable = ['answer_id', 'user_id', 'reason'];

    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function answer()
    {
        return $this->belongsTo('App\Answer','answer_id');
    }
}
