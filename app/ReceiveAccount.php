<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiveAccount extends Model
{
    //
    protected $fillable =[
        'user_id',
        'access_token' ,
        'livemode',
        'refresh_token',
        'token_type',
        'stripe_publishable_key',
        'stripe_user_id',
        'scope',
    ];
    
    
    /**
     * The person who made the payment
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    
    

}
