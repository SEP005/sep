<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TaskBid extends Model
{
    /**
     * @var array
     * Fillable fields for the bid
     */
    protected $fillable =[
        'task_id',
        'user_id' ,
        'bid_price',
        'deliver_in',
        'proposal'
    ];
    //

    public function getTask()
    {
        return $this->belongsTo(Task::class,'task_id');
    }

    public function bidOwner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * To check if the given user created the bid
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }

    /**
     * To get the task which has the referenced id
     * @param $id
     * @return mixed
     */
    public static function taskBidLocatedAt($id)
    {
        return static::where(compact('id'))->firstOrFail();
    }


    public function getBidPriceAttribute($bid_price)
    {
        return '$ '.number_format($bid_price,2);
    }

//    public function getStatusAttribute($status)
//    {
//        if($status == 1) {
//            return 'Bid In review';
//        }
//        if($status == 2){
//            return 'Bid Accepted!';
//        }
//        if($status == 3){
//            return 'Bid Rejected!';
//        }
//    }

    /**
     * If mutator is not needed
     * @return mixed|string
     */
    public function getActualPrice()
    {
        $price = $this->bid_price;
        $price = str_replace('$ ','', $price);
        $price = substr($price, 0, strpos($price, '.'));

        $price = str_replace(',','', $price);
        $price = str_replace('.','', $price);

        return $price;
    }


    /**
     * TO Check bid status
     * @param $bidId
     * @return int
     */
    public static function bidStatusCheck($bidId)
    {

        if($bid = static::where('id',$bidId)->get()->first()) {
            return $bid->status;
        }
        else{
            return 10;
        }

    }

    /**
     * To get All the accepted bids which are ongoing
     * @return mixed
     */
    public static function getAcceptedBids()
    {
       $acceptedBids = static::where('status',2)->orWhere('status',5)->get();
        return $acceptedBids;
    }


    /**
     * To get the latest Bid
     * @return mixed
     */
    public static function getLatestBid()
    {
        $latestBid = static::orderBy('created_at', 'desc')->first();
        return $latestBid;
    }

    /**
     * To get task progress path
     * @return string
     */
    public function taskProgressPath()
    {
        //$this->id is bid's id
        return '/taskprogress/' .$this->id;
    }

}
