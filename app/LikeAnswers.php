<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikeAnswers extends Model
{
    protected $fillable = ['answer_id', 'user_id'];
}
