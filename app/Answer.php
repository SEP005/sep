<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['answer', 'question_id', 'user_id'];

    public function question()
    {
        return $this->belongsTo('App\Questions','question_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
