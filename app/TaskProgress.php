<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskProgress extends Model
{
    //
    protected $fillable = ['bid_id','requester_id','worker_id'];

    /**
     * To get the task which the bid belongs to
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTaskBid()
    {
        return $this->belongsTo(TaskBid::class,'bid_id');
    }

    /**
     * To get the requester information 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTaskRequester()
    {
        return $this->belongsTo(User::class,'requester_id');
    }

    /**
     * To get information related to the worker
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTaskWorker()
    {
        return $this->belongsTo(User::class,'worker_id');
    }
    
}
