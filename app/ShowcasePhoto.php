<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Image;

class ShowcasePhoto extends Model
{
    //
    protected $table = 'showcase_photos'; //Get the table name

    protected $fillable = ['path','name','thumbnail_path'];

    protected $baseDir = 'showcaseFiles/photos';

    public function showcase()
    {
        $this->belongsTo(Showcase::class);
    }

    /**
     * name the photo
     * move it to a directory
     *
     * @param $name
     * @return static
     */
    public static function named($name)
    {
        $photo = new static; //Static instance
        return $photo->saveAs($name); //Gets the file name
    }
    /**
     * To modify the save as name
     * @param $name
     * @return $this
     */
    public function saveAs($name)
    {
        $this->name= sprintf("%s-%s",Carbon::now().rand(1,100),$name);
        $this->name = str_replace(' ','-', $this->name);
        $this->path = sprintf("%s/%s",$this->baseDir,$this->name);
        $this->thumbnail_path = sprintf("%s/tn-%s",$this->baseDir,$this->name);

        return $this;
    }
    /**
     * To move to directory
     * @param UploadedFile $file
     * @return $this
     */
    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir,$this->name); //

        $this->makeThumbnail();

        return $this;
    }
    /**
     * To create thumbnail of the image
     * and store it in the thumbnail path
     */
    public function makeThumbnail()
    {
        Image::make($this->path)
            ->fit(200)
            ->save($this->thumbnail_path);
    }

    /**
     * To delete the record from folder and from database
     * @throws \Exception
     */
    public function delete()
    {
        \File::delete([ //To delete from the folder
            $this->path,
            $this->thumbnail_path
        ]);
        parent::delete();
    }
    
}
