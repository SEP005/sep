<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //Added by Shaan
    // Allow the fields to be filled
    protected $fillable = ['name', 'description', 'parent_id'];
    //End of Added by Shaan

    //
    public function subCategories() //To fetch subcategories
    {
        return $this->hasMany(SubCategory::class);
    }

    public function tasks() 
    {
        return $this->hasMany(Task::class);
    }


    /* ADDED BY SHAAN
    ----------------------------------------------------------- */
    // Generate Category edit path
    public function pathEdit()
    {
        return '/admin/categories/' . $this->id . '/edit';
    }

    // Get all the categories with its subcategories
    public function getCategories()
    {
        $categories = Category::all()->where('parent_id', 0);
        $categories = $this->addRelation($categories);

        return $categories;
    }

    //
    public function selectChild($id)
    {
        $categories = Category::all()->where('parent_id', $id);

        $categories = $this->addRelation($categories);
        return $categories;
    }

    // Adds category relation between categories and subcategories
    public function addRelation($categories)
    {
        $categories->map(function($item, $key)
        {
            $sub = $this->selectChild($item->id);
            return $item = array_add($item, 'subCategory', $sub);
        });

        return $categories;
    }

    // Get the parent of the category
    public function getParent()
    {
        $parent = $this->parent_id;
        $parentName = Category::all()->where('id', $parent)->first();

        if(!$parentName)
        {
            $cat = 'None';
        }
        else
        {
            $cat = $parentName->name;
        }

        return $cat;
    }

    // Checks if child exists
    public function checkChildExist()
    {
        $check = '';
        if($this->parent_id == 0)
        {
            $subCategory = Category::all()->where('parent_id', $this->id)->first();
            if($subCategory)
            {
                $check = 'disabled';
                //$message = 'You';
            }
        }

        return $check;
    }
    /* END OF ADDED BY SHAAN
    ----------------------------------------------------------- */

}
