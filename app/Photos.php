<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Photos extends Authenticatable{

    protected $table = 'post_images';


    protected $fillable = [
        'image',
    ];

    public function user()
    {
        return $this->belongsTo('App\Status');

    }


}