<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    //

    /**
     * The user who made payment
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getReceiver()
    {
        return $this->belongsTo('App\User','receiver_id');
    }

    /**
     * The user receives payment
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSender()
    {
        return $this->belongsTo('App\User','sender_id');
    }

    /**
     * To get the stripe account details
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getStripeAccount()
    {
        return $this->belongsTo(StripeAccount::class,'account_id');
    }

    /**
     * To get the task which the bid belongs to
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTaskBid()
    {
        return $this->belongsTo(TaskBid::class,'bid_id');
    }


//    public function getPayment_AmountAttribute($payment_amount)
//    {
//        return '$'.number_format($payment_amount,2);
//    }
    
}
