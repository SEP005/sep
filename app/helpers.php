<?php
/**
 * This Class was made to make all the the functions written in this class to be globally available
 * */


function flash($title = null,$message = null)
{

        $flash = app('App\Http\Flash'); //Fetches Flash Class

        /**
         * If number of function arguments is 0
         */
        if (func_num_args() == 0)
        {
                return $flash; //Return flash instance // If flash()->success('title','body') ,info() or error()
        }


        return $flash->info($title,$message); //If flash('title','body')

}

//linkTo('Delete?',$model,'DELETE')
function linkTo($body,$path,$type)
{
        $csrf = csrf_field();


        if (is_object($path)){
                //
                $action = $path->getTable();
                if(in_array($type,['PUT','PATCH','DELETE'] )){

                        $action .='/'.$path->getKey();

                }
                else{
                        $action = $path;
                }


        }

        return <<<EOT
        
                <form method="POST" action="{$action}">
                        <input type='hidden' name='_method' value='{$type}'>
                        $csrf
                        <button type="submit">{$body}</button>
                </form>
EOT;


}

