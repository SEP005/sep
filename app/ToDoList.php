<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDoList extends Model
{
    //
    /**
     * @var array
     * Fillable fields for the bid
     */
    protected $fillable =[
        'user_id' ,
        'body',
        'completed',
    ];

    /**
     * The ToDoList is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
