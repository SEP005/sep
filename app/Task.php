<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * @var array
     * Fillable fields for the task
     */
    protected $fillable =[
        'category_id',
        'topic' ,
        'details',
        'price',
        'skills',
        'user_id',
        'video_path',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCategory()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
    /**
     * The Task is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }

    /**
     * To show the task if the path is selected
     * @return string
     */
    public function path()
    {
        return '/tasks/' . $this->id;
    }

    /**
     * To get the task which has the referenced id
     * @param $id
     * @return mixed
     */
    public static function taskLocatedAt($id)
    {
        return static::where(compact('id'))->firstOrFail();
    }


    /**
     * To add photo to its database which belongs to the task
     * @param TaskPhoto $photo
     * @return Model
     */
    public function addTaskPhoto(TaskPhoto $photo)
    {
        return $this->photos()->save($photo); //TO add photo belonging to the task
    }


    /**
     * A task post may contain many photos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function photos()
    {
        return $this->hasMany(TaskPhoto::class);
    }



    public static function getPriceWithoutRs($price)
    {

       return $price = str_replace('','$ ', $price);

    }


    /**
     * To check if the given user created the task
     * @param User $user
     * @return bool
     */
    public function ownedBy(User $user)
    {
        return $this->user_id == $user->id;
    }

    /**
     * A Task has many bids
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bids()
    {
        return $this->hasMany(TaskBid::class);
    }

    /**
     * Count all bids for the task
     * @param $id
     * @return mixed
     */
    public static function bidCount($id){
        return static::where('id',$id)->firstOrFail()->bids->count();
    }
    
    
    /**
     * Modify the video URL to include embed/ so that it can be
     * embedded in the view using iFrame
     * @param $video_path
     * @return mixed
     */
    public function getVideoPathAttribute($video_path)
    {
        return str_replace('watch?v=','embed/', $video_path);
    }

    /**
     * Allows to format the price while retreiving them
     * getPriceAttribute is part of eloquent mutator model
     * @param $price
     * @return string
     */
    public function getPriceAttribute($price)
    {
        return '$ '.number_format($price,2);
    }

    /**
     * If mutator is not needed
     * @return mixed|string
     */
    public function getActualPrice()
    {
        $price = $this->price;
        $price = str_replace('$ ','', $price);
        $price = substr($price, 0, strpos($price, '.'));

        $price = str_replace(',','', $price);
        $price = str_replace('.','', $price);

        return $price;
    }
    
    


}
