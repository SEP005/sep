<?php

namespace App;
use App\Http\Controllers\ReviewController;
use App\Status;
use App\Review;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastname',
        'address',
        'street',
        'city',
        'country',
        'email',
        'password',
        'website',
        'bio'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //to get the name of the logged in user
    public function getName()
    {
        if ($this->name){
            return $this->name;
        }
        return null;
    }

    public function getAvatarUrl()
    {
        return "https://www.gravatar.com/avatar/{{ md5($this->email)}}?d=mm&s=40";
    }

//    models which have a relationship
//    with user model are listed here

    public function statuses()
    {
        return $this->hasMany('App\Status','user_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Like','user_id');

    }
    public function undo()
    {
        return $this->hasMany('App\Undo','user_id');

    }
	public function dislikes()
    {
        return $this->hasMany('App\Dislike','user_id');

    }

    public function hasLikedStatus(Status $status)
    {
        return (bool) $status
            ->likes
            ->where('user_id',$this->id)->count();


    }
	public function hasDislikedStatus(Status $status)
    {
        return (bool) $status
            ->dislikes
            ->where('user_id',$this->id)->count();


    }
//methods to accepting friend
    public function friendsOfMine()
    {
        return $this->belongsToMany('App\User','friends','user_id','friend_id');
    }

    public function friendOf()
    {
        return $this->belongsToMany('App\User','friends','friend_id','user_id');
    }
    //to get current user's friend
    public function friends()
    {
        return $this->friendsOfMine()->wherePivot('accepted',true)->get()
            ->merge($this->friendOf()->wherePivot('accepted',true)->get());
    }

//    public function notification()
//    {
//        return $this->friendsOfMine()->wherePivot('accepted',false)->get();
//    }
//accessing friend requests
    public function friendRequests()
    {
        return $this->friendsOfMine()->wherePivot('accepted',false)->get();
    }

    public function friendRequestsPending()
    {
        return $this->friendOf()->wherePivot('accepted',false)->get();
    }

    public function hasFriendRequestsPending(User $user)
    {
        return (bool) $this->friendRequestsPending()
            ->where('id',$user->id)->count();
    }

    public function hasFriendRequestsReceived(User $user)
    {
        return (bool) $this->friendRequests()->where('id',$user->id)->count();
    }

    public function addFriend(User $user)
    {
        $this->friendOf()->attach($user->id);
    }

    public function acceptFriendRequest(User $user)
    {
        $this->friendRequests()->where('id',$user->id)->first()->pivot->
            update([
            'accepted'=> true,
        ]);
    }

    public function deleteFriendRequest($user){
        $this->friendRequests()->where('id',$user->id)->first()->pivot->
        delete();
    }

    public function isFriendsWith(User $user)
    {
        return (bool) $this->friends()->where('id',$user->id)->count();
    }

    /**
     * reveiw relationship with the user
     */
    public function review()
    {
        return $this->hasMany(Review::class);
    }

    public function recalculateRating($rating)
    {
        $reviews = $this->reviews()->notSpam()->approved();
        $avgRating = $reviews->avg('rating');
        $this->rating_cache = round($avgRating,1);
        $this->rating_count = $reviews->count();
        $this->save();
    }


    /** Nauf Methods  */

    /**
     * To check if user owns the Relation .
     *
     * This returns true if user owns the relation
     *
     * @param $relation
     * @return bool
     */
    public function owns($relation)
    {
        return $relation->user_id == $this->id;
    }

    public function userBids()
    {
        return $this->hasMany(TaskBid::class);
    }

    public function userTasks()
    {
        return $this->hasMany(Task::class);
    }



    /**
     * Nauf Methods End
     */

    /** Shan Code Start
     * *************************************************************************** */

    /**
     * Creates relationship between user and user role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * Creates a link to edit the user
     *
     * @return string
     */

    public function pathEdit()
    {
        return '/admin/profile/' . $this->id . '/edit';
    }
    
    
    /** Shan Code End
     * *************************************************************************** */

}
