<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Status extends Authenticatable{

    protected $table = 'statuses';


    protected $fillable = [
        'heading',
        'body',
        'role',
        'tags',

    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');

    }

    public function photos()
    {
        return $this->hasMany('App\Photos');

    }

    

    public function scopeNotReply($query)
    {
        return $query->whereNull('parent_id');

    }

    public function replies()
    {
        return $this->hasMany('App\Status','parent_id');
    }

    public function likes()
    {
        return $this->morphMany('App\Like','likeable');

    }

    public function dislikes()
    {
        return $this->morphMany('App\Dislike','dislikeable');

    }
    public function undo()
    {
        return $this->morphMany('App\Undo','likeable');

    }

 
}