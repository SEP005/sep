<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $fillable = ['question', 'information', 'questionCategory_id', 'likes', 'user_id'];

    public function owner()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
