<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskDiscussion extends Model
{
    //
    protected $fillable = ['details','user_id','video_path','bid_id'];


    public function getTaskBid()
    {
        return $this->belongsTo(TaskBid::class,'bid_id');
    }

    public function getUser()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
