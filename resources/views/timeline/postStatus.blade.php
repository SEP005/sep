@extends('layouts.app')

@section('css.header')
    {{--<link rel="stylesheet" href="/css/lity.min.css">--}}
    {{--<link rel="stylesheet" href="../css/CssFileScripts/showcase-menu.css">--}}
    <link rel="stylesheet" href="/css/frontEnd-UI.css">
    <link rel="stylesheet" href="/css/.css">

@stop

@section('content')
    <div class="container">
        <br>
        <br>
        <div class="row"  style="margin: auto;
                        width: 75%;
                        padding: 10px;">
            <div class="col-md-8">

                <form method="post" action="{{ route('status.post') }}" role="form" enctype="multipart/form-data" class="clo-md-6">
                {{--Heading--}}
                    <div class="form-group {{ $errors->has('heading')?' has-error':""}}">
                        <label for="post-heading" class="col-lg-3 control-label">
                            <i class="fa fa-header" aria-hidden="true"></i>eading</label>
                        <div>
                            <input type="text" class="form-control" name="heading" id="heading" placeholder="What's your Advertisement?">
                            @if ($errors->has('heading'))
                                <span class="help-block">
                                    {{ $errors->first('heading')}}
                                </span>
                            @endif
                        </div>
                    </div>
                    {{--Description--}}
                    <div class="form-group {{ $errors->has('status')?' has-error':""}}">
                        <label for=status" class="col-lg-3 control-label"> <i class="fa fa-info-circle" aria-hidden="true"></i>     About</label>
                        <textarea placeholder="Tell us more!!! {{Auth::user()->getName() }}?"
                                  name="status" id="post-body" class="form-control" rows="5"></textarea>
                        @if($errors->has('status'))
                            <span class="help-block">
                                {{ $errors->first('status')}}
                            </span>
                        @endif
                    </div>
                    {{--Role--}}
                    <div class="form-group">
                        <div class="dropdown">
                            <label>Select Role  </label>
                            <select class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" id="role" name="role">
                                <option value="Worker">As a Worker</option>
                                <option value="Requester">As a Requester</option>
                            </select>
                        </div>
                    </div>
                    {{--Tags--}}
                    <div class="form-group">
                        <label for="post-tags" class="col-lg-3 control-label"><i class="fa fa-tags" aria-hidden="true"></i>     Tags</label>
                        <div class="form-group">
                        <input class="form-control col-lg-7" id="tags" name="tags" type="text"
                               placeholder="Tags"
                               value="{{old('skills')}}" data-role="tagsinput">
                            </div>
                    </div>

                    <div class="form-group">
                    <button type="submit" id="post" class="btn btn-default pull-right btn btn-primary ">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        Advertise
                    </button>
                        <a href="{{'/home'}}" role="button" class="btn btn-default btn-default pull-left btn btn-danger">
                            <i class="fa fa-times" aria-hidden="true"></i>Cancel
                        </a>

                        </div>


                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </form>

            </div>



        </div>
    </div>
@stop

@section('scripts.footer')
    <script src="/js/bootstrap-tagsinput.min.js"></script>

@stop