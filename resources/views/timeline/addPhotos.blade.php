@extends('layouts.app')

@section('css.header')


    {{--<link rel="stylesheet" href="../css/CssFileScripts/showcase-menu.css">--}}
    <link rel="stylesheet" href="/css/frontEnd-UI.css">
    {{--<link rel="stylesheet" href="/css/.css">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">

@stop

@section('content')
    <div class="container">
        <div class="col-md-12">
        <h1 style="color:darkslategray "><b>{{$status->heading}}</b></h1>
        <h3>{{$status->body}}</h3>
            {{--<div class="col-md-9">--}}
                {{--@foreach($status->photos as $photo)--}}
                    {{--<img src="{{$photo->path}}" alt="" width="100" height="100">--}}

                {{--@endforeach--}}
            {{--</div>--}}

            <form id="addPhotosForm"
                  action="/{{$status->id}}/photos"
                  method="post"
                  class="dropzone">

                <input type="hidden" name="_token" value="{{Session::token()}}">
            </form>

        </div>


    </div>




@stop
{{--<script>--}}
    {{--var photo_counter = 0;--}}
    {{--Dropzone.options.realDropzone = {--}}

        {{--uploadMultiple: false,--}}
        {{--parallelUploads: 100,--}}
        {{--maxFilesize: 8,--}}
        {{--previewsContainer: '#dropzonePreview',--}}
        {{--previewTemplate: document.querySelector('#preview-template').innerHTML,--}}
        {{--addRemoveLinks: true,--}}
        {{--dictRemoveFile: 'Remove',--}}
        {{--dictFileTooBig: 'Image is bigger than 8MB',--}}

        {{--// The setting up of the dropzone--}}
        {{--init:function() {--}}

            {{--this.on("removedfile", function(file) {--}}

                {{--$.ajax({--}}
                    {{--type: 'POST',--}}
                    {{--url: 'upload/delete',--}}
                    {{--data: {id: file.name, _token: $('#csrf-token').val()},--}}
                    {{--dataType: 'html',--}}
                    {{--success: function(data){--}}
                        {{--var rep = JSON.parse(data);--}}
                        {{--if(rep.code == 200)--}}
                        {{--{--}}
                            {{--photo_counter--;--}}
                            {{--$("#photoCounter").text( "(" + photo_counter + ")");--}}
                        {{--}--}}

                    {{--}--}}
                {{--});--}}

            {{--} );--}}
        {{--},--}}
        {{--error: function(file, response) {--}}
            {{--if($.type(response) === "string")--}}
                {{--var message = response; //dropzone sends it's own error messages in string--}}
            {{--else--}}
                {{--var message = response.message;--}}
            {{--file.previewElement.classList.add("dz-error");--}}
            {{--_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");--}}
            {{--_results = [];--}}
            {{--for (_i = 0, _len = _ref.length; _i < _len; _i++) {--}}
                {{--node = _ref[_i];--}}
                {{--_results.push(node.textContent = message);--}}
            {{--}--}}
            {{--return _results;--}}
        {{--},--}}
        {{--success: function(file,done) {--}}
            {{--photo_counter++;--}}
            {{--$("#photoCounter").text( "(" + photo_counter + ")");--}}
        {{--}--}}
    {{--}--}}
{{--</script>--}}

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>

    <script>
        Dropzone.options.addPhotosForm={
            paramName: 'photo',
            maxFilesize: 3,
            acceptedFiles: '.jpg,.jpeg,.png,.bmp'
        };
    </script>

@stop