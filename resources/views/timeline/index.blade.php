@extends('layouts.app')

@section('css.header')
    {{--<link rel="stylesheet" href="/css/lity.min.css">--}}
    {{--<link rel="stylesheet" href="../css/CssFileScripts/showcase-menu.css">--}}
    <link rel="stylesheet" href="/css/frontEnd-UI.css">
    <link rel="stylesheet" href="/css/.css">

@stop

@section('content')
    <div class="container timelineVue">
        <div class="row">
            <div class="col-md-8 ">
                <div class="panel panel-primary">
                    <div class="panel-heading">News feed</div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <a href="{{route('postStatus')}}" class="advertise" id="advertisement-anchor" role="button">
                                    <B class="blink">
                                        <font id="blink-font">
                                            <i class="fa fa-hand-o-right" aria-hidden="true"></i><i
                                                    class="fa fa-hand-o-right" aria-hidden="true"></i>
                                            !!!Advertise Here!!!
                                            <i class="fa fa-hand-o-left" aria-hidden="true"></i><i
                                                    class="fa fa-hand-o-left" aria-hidden="true"></i>
                                        </font>
                                    </B>
                                </a>
                                <hr>

                            </div>

                        </div>


                        <div class="row">
                            <div class="col-lg-8">
                                <!-- Timeline statuses and replies -->
                                @if(!$statuses->count())
                                    <p>There's nothing in your timeline yet.</p>
                                @else
                                    @foreach($statuses as $status)
                                        <div class="media" data-id="{{ $status->id }}">
                                            <a class="pull-left"
                                               href="{{ route('profile.index',['name'=> $status->user->name]) }}">
                                                @if (Storage::disk('local')->has($status->user->first_name . '-' . $status->user->id . '.jpg'))
                                                    <img class="img-circle"
                                                         src="{{ route('account.image', ['filename' => $status->user->first_name . '-' . $status->user->id . '.jpg']) }}"
                                                         alt="{{$status->user->getName()}}" width="50" height="50">
                                                @else
                                                    <img class="img-circle" alt="{{$status->user->getName()}}"
                                                         src="{{$status->user->getAvatarUrl()}}">
                                                @endif

                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a
                                                            href="{{ route('profile.index',['name'=> $status->user->name]) }}">{{$status->user->getName()}}</a>
                                                </h4>
                                                <h2 style="color:darkslategray "><b>{{$status->heading}}</b></h2>
                                                <p>{{$status->body}}</p>
                                                <p>{{$status->tag}}</p>
                                                <div class="blocktxt">
                                                    <ul class="list-inline">
                                                        <li><a href="/tag/{{$status->tag}}"><span class="label label-success">{{$status->tag}}</span></a></li>
                                                    </ul>
                                                </div>
                                                {{--<div class="col-md-9">--}}
                                                    {{--@foreach($status->photos as $photo)--}}
                                                        {{--<img src="{{$photo->path}}" alt="" width="100" height="100">--}}

                                                    {{--@endforeach--}}
                                                {{--</div>--}}

                                                <p class="text-warning">Posted As a {{ $status->role }}</p>
                                                @if($status->user->id == Auth::user()->id)
                                                    <a href="{{ route('addPhotos',['statusId'=> $status->id])}}">click here to upload images..</a> </p>
                                                @endif
                                                <ul class="list-inline">
                                                    <li>
                                                        <i class="fa fa-clock-o"></i>{{ $status->created_at->diffForHumans() }}
                                                    </li>

                                                    @if($status->user->id !== Auth::user()->id)
                                                        @if((Auth::user()->hasLikedStatus($status)))
                                                            <li>
                                                                <a href="{{route('status.undo', ['statusId'=> $status->id])}}">
                                                                    <p class="text-info">You liked this</p>
                                                                </a>
                                                            </li>
                                                        @elseif((Auth::user()->hasDislikedStatus($status)))
                                                            <li>
                                                                <a href="{{route('status.undo', ['statusId'=> $status->id])}}">
                                                                    <p class="text-info">You disliked this</p>
                                                                </a>
                                                            </li>
                                                        @else
                                                            <li>
                                                                <a href="{{route('status.like', ['statusId'=> $status->id])}}">
                                                                    <i class="fa fa-thumbs-up"></i>Like </a> <b>|</b>
                                                            </li>
                                                            <li>
                                                                <a href="{{route('status.dislike', ['statusId'=> $status->id])}}">
                                                                    <i class="fa fa-thumbs-down"></i>Dislike </a></li>
                                                        @endif
                                                    @else

                                                        <li><a href="#" class="edit"
                                                            @click="statusIdM={{ $status->id}}
                                                            ,statusMessageM={{$status->body}}
                                                            "
                                                            data-toggle="modal" data-target="#editStatusModal"

                                                            >Edit </a><b> |</b>
                                                        <li>
                                                            <a href="{{route('status.delete', ['statusId'=> $status->id])}}">Delete</a>

                                                    @endif
                                                    <li>{{ $status->likes->count() }} <i class="fa fa-thumbs-up"></i>
                                                    </li>
                                                    <li>{{ $status->dislikes->count() }} <i
                                                                class="fa fa-thumbs-down"></i></li>

                                                    {{--<script>--}}
                                                        {{--var postId = 0;--}}
                                                        {{--var postBodyElement = null;--}}

                                                        {{--$('.edit').on('click', function (event) {--}}
                                                            {{--event.preventDefault();--}}

                                                            {{--postBodyElement = event.target.parentNode.parentNode.parentNode.childNodes[5];--}}
                                                            {{--var postBody = postBodyElement.textContent;--}}
                                                            {{--postId = event.target.parentNode.parentNode.parentNode.dataset['postId'];--}}
                                                            {{--$('#edit-body').val(postBody);--}}
                                                            {{--$('#edit-modal').modal();--}}
                                                        {{--});--}}

                                                        {{--$('#modal-save').on('click', function () {--}}
                                                            {{--$.ajax({--}}
                                                                {{--method: 'POST',--}}
                                                                {{--url: urlEdit,--}}
                                                                {{--data: {--}}
                                                                    {{--body: $('#edit-body').val(),--}}
                                                                    {{--postId: postId,--}}
                                                                    {{--_token: token--}}
                                                                {{--}--}}
                                                            {{--})--}}
                                                                    {{--.done(function (msg) {--}}
                                                                        {{--$(postBodyElement).text(msg['new_body']);--}}
                                                                        {{--$('#edit-modal').modal('hide');--}}
                                                                    {{--});--}}
                                                        {{--});--}}

                                                    {{--</script>--}}


                                                </ul>
                                                {{--display comments--}}
                                                @foreach($status->replies as $reply)
                                                    <div class="media">
                                                        <a class="pull-left"
                                                           href="{{ route('profile.index',['name'=> $status->user->name]) }}">
                                                            @if (Storage::disk('local')->has($status->user->first_name . '-' . $status->user->id . '.jpg'))
                                                                <img class="img-circle"
                                                                     src="{{ route('account.image', ['filename' => $status->user->first_name . '-' . $status->user->id . '.jpg']) }}"
                                                                     alt="{{$status->user->getName()}}" width="25" height="25">
                                                            @else
                                                                <img class="img-circle" alt="{{$status->user->getName()}}"
                                                                     src="{{$status->user->getAvatarUrl()}}">
                                                            @endif

                                                        </a>
                                                        <div class="media-body">
                                                            <h5 class="media-heading"><a href="{{ route('profile.index',['name'=> $status->user->name]) }}">
                                                                    {{$reply->user->getName()}}</a></h5>
                                                            <p>{{$reply->body}}</p>
                                                            <ul class="list-inline">
                                                                <li>{{ $reply->created_at->diffForHumans() }}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                {{--Comment form--}}
                                                <form role="form"
                                                      action="{{ route('status.reply',['statusId'=>$status->id]) }}"
                                                      method="post">
                                                    <div class="form-group">
                                                    <input type="text" name="reply"
                                                              class="form-control text-left" rows="1"
                                                              placeholder="comment here">
                                                    </input>
                                                        @if($errors->has("reply-{$status->id}"))
                                                            <span class="help-block">
                                                                {{ $errors->first("reply-{$status->id}")}}
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <input type="submit" value="Comment" class="btn btn-default btn-sm">
                                                    <input type="hidden" name="_token" value="{{Session::token()}}">
                                                </form>
                                            </div>
                                        </div>
                                    @endforeach
                                    {!! $statuses->render() !!}
                                @endif

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- side barr starts here
                trending tags starts here-->
            <div class="col-md-4 " id="right">

                <div class="sidebarblock">
                    <h3>Trending Tags</h3>
                    <div class="divline"></div>

                    <div class="blocktxt">
                        <ul class="list-inline">
                            <li><a href="/tag/general"><span class="label label-success">General</span></a></li>
                            <li><a href="/tag/students"><span class="label label-success">Electorincs</span></a></li>
                            <li><a href="/tag/staff"><span class="label label-success">Home applicance</span></a></li>
                            <li><a href="/tag/lectures"><span class="label label-success">IT professional</span></a>
                            </li>
                            <li><a href="/tag/security"><span class="label label-success">Security</span></a></li>
                            <li><a href="/tag/buildings"><span class="label label-success">Buildings</span></a></li>
                            <li><a href="/tag/car_park"><span class="label label-success">Car</span></a></li>
                            <li><a href="/tag/sports"><span class="label label-success">Sports</span></a></li>
                            <li><a href="/tag/cafeteria"><span class="label label-success">Cafeteria</span></a></li>
                            <li><a href="/tag/exams_and_assignments"><span class="label label-success">Books</span></a>
                            </li>
                        </ul>
                    </div>
                </div>

                {{-- Code Written By Nauf --}}

                {{--Info: Get the latest task posts--}}

                {{-- Check if latest tasks exit--}}
                @if(count($latestTasks)!=0)

                    <div class="panel-group">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#">Recently posted tasks</a>
                                <span style="float: right">
                                <a href="# " id="prevBtn">&laquo;</a>
                                <a href="# " id="nextBtn">&raquo;</a>
                                </span>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <div class="panel-body">

                                    <div id="lTasks">

                                        @foreach($latestTasks as $task)
                                            <div id="lTask" style="background-color: #FFFFFF">

                                                <h4><a href="{{$task->path()}}"> {{$task->topic}}</a></h4>
                                                <p class="recentPostDetails"> {{$task->details}}</p>

                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif
                {{--Info: End of latest task posts--}}

                {{--Info : Specific task post--}}

                <div class="panel-group">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#">Post a specific task</a>
                                <span style="float: right">

                                </span>
                            </h4>
                        </div>

                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">


                                <div class="list-group">
                                    <a href="/tasks/create?topic=Website" class="list-group-item ">
                                        <h4 class="list-group-item-heading">Web Application
                                            <span style="float: right"><i class="fa fa-laptop fa-2x"
                                                                          aria-hidden="true"></i> </span>
                                        </h4>
                                        <p class="list-group-item-text">Do you want to create a website
                                        </p>
                                    </a>

                                    <a href="/tasks/create?topic=Photography" class="list-group-item ">
                                        <h4 class="list-group-item-heading ">Photography
                                            <span style="float: right"><i class="fa fa-camera fa-2x"
                                                                          aria-hidden="true"></i> </span>
                                        </h4>
                                        <p class="list-group-item-text">Do you want to create a website
                                        </p>
                                    </a>

                                    <a href="/tasks/create?topic=Maintenance" class="list-group-item  bg-success">
                                        <h4 class="list-group-item-heading">Maintenance
                                            <span style="float: right"><i class="fa fa-wrench fa-2x"
                                                                          aria-hidden="true"></i> </span>
                                        </h4>
                                        <p class="list-group-item-text">Do you want to create a website
                                        </p>
                                    </a>

                                    <a href="/tasks/create?topic=Moving" class="list-group-item  bg-success">
                                        <h4 class="list-group-item-heading">Help me move
                                            <span style="float: right"><i class="fa fa-truck fa-2x"
                                                                          aria-hidden="true"></i> </span>
                                        </h4>
                                        <p class="list-group-item-text">Do you want to create a website
                                        </p>
                                    </a>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
                {{--Info: End of Specific task--}}

                {{-- End of Code Written By Nauf --}}

            </div>
            <!-- End of SideBar-->

            <!-- tremding tags ends here-->
        </div>
        <!-- Edit post modal-->
        <div class="modal fade" tabindex="-1" role="dialog" id="editStatusModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Post</h4>
                    </div>
                    <form method="post" action="/status/@{{statusIdM}}" role="form" >
                        {{--<form method="post" action="/taskprogress" >--}}
                            {{csrf_field()}}

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="edit-body">Edit the Post </label>
                                <textarea class="form-control" name="edit-body" id="edit-body" rows="5"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" id="modal-save">Save changes</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- edit post modal ends here-->
    </div>



@stop

@section('scripts.footer')
    {{--<script src="/js/lity.js"></script>--}}
    {{--<script src="../js/JsFileScripts/showcase-menu.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cycle/3.0.3/jquery.cycle.all.min.js"></script>
    <script>
        $('#lTasks').cycle(
                {
                    fx: 'fade',
                    pause: 1,
                    next: "#nextBtn",
                    prev: "#prevBtn",
                    speed: 500,
                    timeout: 10000
                });
    </script>

    <script>


        //Vue model
        new Vue({

            el: '.timelineVue',

            data: {
                statusIdM: 0,
                statusMessageM:'',
            },

        });


    </script>
@stop