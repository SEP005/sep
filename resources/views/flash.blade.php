@if(session()->has('flash_message'))

    <script type="application/javascript">

            swal({
                title: "{{session()->get('flash_message.title')}}",
                text: "{{session()->get('flash_message.message')}}",
                type: "{{session()->get('flash_message.level')}}",
                timer: 1700,   showConfirmButton: false});

    </script>

@endif

@if(session()->has('flash_message_overlay'))

    <script type="application/javascript">

        swal({
            title: "{{session()->get('flash_message_overlay.title')}}",
            text: "{{session()->get('flash_message_overlay.message')}}",
            type: "{{session()->get('flash_message_overlay.level')}}",
            confirmButtonText: 'Okay',
           });

    </script>

@endif


{{--<script>--}}

    {{--$('button#delete').on('click', function () {--}}
        {{--swal({--}}
                    {{--title: "Are you sure?",--}}
                    {{--text: "You will not be able to recover this lorem ipsum!", type: "warning",--}}
                    {{--showCancelButton: true,--}}
                    {{--confirmButtonColor: "#DD6B55",--}}
                    {{--confirmButtonText: "Yes, delete it!",--}}
                    {{--closeOnConfirm: false--}}
                {{--},--}}
                {{--function () {--}}
                    {{--$("#deleteform").submit();--}}
                {{--});--}}
    {{--})--}}

{{--</script>--}}