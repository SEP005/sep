@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-20 col-md-offset-1">
            <div class="row">
                <div class="col-lg-20">
                    <div class="col-lg-6">
                    <h3>Your Connections</h3>
                    <!-- List of friends -->
                        @if(!$friends->count())
                            <p><b><font color="#dc143c">You have no connections.</font></b></p>
                        @else
                            @foreach($friends as $user)
                                @include('user/partials/userblock')
                            @endforeach
                        @endif
                </div>
                    <div class="col-lg-6">
                    <h4>Connection requests</h4>
                    <!--List of friend requests -->
                    @if(!$requests->count())
                        <p><b><font color="#dc143c">You have no connection requests.</font></b></p>
                    @else
                        @foreach($requests as $user)
                            @include('user.partials.userblock')
                        @endforeach
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
