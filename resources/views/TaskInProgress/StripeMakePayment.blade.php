
@if($milestonesCompletedStatus)
    <div class="callout callout-success">

        @if($milestones->count() >= 1)
            <h4><i class="icon fa fa-check"></i>This task is completed !</h4>
        @else
            <h4> Task in Progress! </h4>
        @endif

        @if($taskProgress->getTaskRequester->id == Auth::user()->id  )

            @if($taskProgress->getTaskBid->status != 5)

                <form action="/payments" method="POST">

                    <p>You can make payment to the worker</p>

                    {{csrf_field()}}

                    <input type="hidden" class="form-control" id="bid_id" name="bid_id"
                           placeholder="" value="{{$taskProgress->bid_id}}">{{--To pass in the Bid's Id--}}

                    <input type="hidden" class="form-control" id="worker_id" name="worker_id"
                           placeholder=""
                           value="{{$taskProgress->getTaskWorker->id}}">{{--To pass in the Worker's Id--}}
                    {{--Info: Use stripe's checkout integration
                            When the form is submitted stripe hijacks the request, submit an ajax request with the credit card
                            data encrypt it filters it and returns a token back

                            My server would then reciece a stripeToken from which a customer can be created and charged
                    --}}
                    <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            {{--Get the stripe secret key--}}
                            data-key="{{config('services.stripe.key')}}"
                            data-amount="{{$taskProgress->getTaskBid->getActualPrice()}}"
                            data-name="Make the payment to {{$taskProgress->getTaskWorker->name}}"
                            data-description="Worker payment"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-locale="auto"
                            data-zip-code="true"
                            data-email="{{$taskProgress->getTaskRequester->email}}"{{--To get requester's email--}}
                    >
                    </script>
                </form>
            @endif{{--Bid status--}}

        @endif


    </div>
@endif
{{--Milestone Complete status End--}}