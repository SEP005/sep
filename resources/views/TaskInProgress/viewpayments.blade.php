@extends('dashboard')

@section('dash.css')
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../css/animate.css">

    <link rel="stylesheet" href="../css/AdminLTE.css">

    <link rel="stylesheet" href="../css/stripe-connect.css">

    <link rel="stylesheet" type="text/css" href="../css/CssFileScripts/showcase-index.css">

    <link rel="stylesheet" href="../css/dataTables.bootstrap.css">


    <style>

        .completed {
            text-decoration: line-through;
            color: #00a65a;
        }

        /* Oval: */
        .checkOval {
            width: 10px;
            height: 15px;
            background: #FFFFFF;
            border: 1px solid #44A6B2;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
            float: left;
            cursor: pointer;
        }

        /* Oval Completed: */
        .checkOvalCompleted {
            width: 10px;
            height: 15px;
            background: #5da86f;
            border: 1px solid #44A6B2;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
            float: left;
            cursor: pointer;
        }


    </style>

@stop

@section('dash.content')
    @include('includes.message_block')



    <div class="container">
        <div class="row">

            <ul class="nav nav-tabs">
                <li role="presentation"><a href="/taskprogress">Ongoing tasks</a></li>
                <li role="presentation" class="active"><a href="/viewpayments">Payments</a></li>
            </ul>
            {{--End of small nav bar--}}
            {{--<br>--}}

            <div class="col-md-8 ">

                <div class="page-header">
                    <h2 class=""> Payments </h2>
                </div>
                <!-- =========================================================== -->

                {{--WIdgets--}}
                @if(isset($payments))

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">


                            <div class="info-box">
                                <span class="info-box-icon bg-navy"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Spent</span>
                                    <span class="info-box-number">{{$paymentsSpent}}</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-usd"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Received</span>
                                    <span class="info-box-number">{{$paymentsRecieved}}</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="fa fa-line-chart"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Profit</span>
                                    <span class="info-box-number">{{$paymentsRecieved-$paymentsSpent}}</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>

                    </div>

                    <!-- /.row -->
                @endif

                {{--WIdgets--}}

            <!-- =========================================================== -->

                {{--Stripe Account Check--}}
                @if(!isset($receiver))
                    {{--Info: Stripe User function--}}
                    <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_9WhKAxtu6s78sgTfWbtfb65dpay2eqqO&scope=read_write"
                       class=" stripe-connect light-blue dark">
                        <span>Connect with Stripe</span> </br>
                    </a>
                    {{--Info: Stripe User function--}}

                @endif

                <?php
                // See full code example here: https://gist.github.com/3507366

                if (isset($_GET['code'])) { // Redirect w/ code
                    $code = "s";
                    $code = $_GET['code'];
//
                    $token_request_body = array(
                            'grant_type' => 'authorization_code',
                            'client_id' => 'ca_9WhKAxtu6s78sgTfWbtfb65dpay2eqqO',
                            'code' => $code,
                            'client_secret' => 'sk_test_5u44L34Lqh3wRR1F8v5B1nVV'
                    );

                    $req = curl_init('https://connect.stripe.com/oauth/token');
                    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($req, CURLOPT_POST, true);
                    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
//
//                    // TODO: Additional error handling
                    $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
                    $resp = json_decode(curl_exec($req), true);
                    curl_close($req);

                }
                ?>

                @if(isset($_GET['code']))

                    <form action="/ReceiveAccount" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" class="form-control"
                               id="user_id" name="user_id" value="{{Auth::user()->id}}">

                        <input type="hidden" class="form-control"
                               id="access_token" name="access_token" value="{{$resp['access_token']}}">

                        <input type="hidden" class="form-control"
                               id="livemode" name="livemode" value="{{$resp['livemode']}}">

                        <input type="hidden" class="form-control"
                               id="refresh_token" name="refresh_token" value="{{$resp['refresh_token']}}">

                        <input type="hidden" class="form-control"
                               id="token_type" name="token_type" value="{{$resp['token_type']}}">

                        <input type="hidden" class="form-control"
                               id="stripe_publishable_key" name="stripe_publishable_key"
                               value="{{$resp['stripe_publishable_key']}}">

                        <input type="hidden" class="form-control"
                               id="stripe_user_id" name="stripe_user_id" value="{{$resp['stripe_user_id']}}">

                        <input type="hidden" class="form-control"
                               id="scope" name="scope" value="{{$resp['scope']}}">

                        <h4 class="">Submit generated stripe account details to view the task progresses </h4>
                        <button class="btn btn-info" type="submit">Submit</button>

                    </form>
                @endif
                {{--INFO: TESTING--}}
                {{--End of Stripe Account Check--}}


                {{--<div class="page-header">--}}
                {{--<h2 class=""> Payments </h2>--}}
                {{--</div>--}}

                {{--Info: Payment Table--}}
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Transaction History</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Task Completed</th>
                                <th class="">Payment Type</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($payments))

                                @foreach($payments as $payment)

                                    <tr>
                                        <td>{{$payment->getTaskBid->getTask->topic}}</td>
                                        <td>
                                            @if($payment->sender_id == Auth::user()->id)
                                                Payment made
                                                <p>
                                                    <span class=" label bg-navy">{{$payment->created_at->diffForHumans()}}</span>
                                                </p>
                                            @elseif($payment->receiver_id == Auth::user()->id)
                                                Received Payment
                                                <p><span class=" label"
                                                         style="background-color:#00a65a ">
                                                    {{$payment->created_at->diffForHumans()}}</span></p>
                                            @endif
                                        </td>
                                        <td>
                                            {{number_format($payment->payment_amount,2)}}
                                            {{--{{$payment->payment_amount}}--}}
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Task Completed</th>
                                <th class="">Payment Type</th>
                                <th>Amount</th>

                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>

                {{--Info: Payment Table End--}}
            </div>{{--End of Column--}}


            <div class="col-md-4">


                <div class="vueToDo ">
                    {{csrf_field()}}

                    <form method="POST" action="/todolist">
                        {{--{{method_field('POST')}}--}}
                        <h3>My To-do list items

                            <button type="submit" style="margin-left: 35px"
                                    class="btn btn-info animated"
                            @click="addItem()"
                            v-show="message"
                            transition="fade"
                            >
                            Add To-do Item
                            </button>

                        </h3>


                        {{csrf_field()}}

                        <input type="hidden" class="form-control" id="user_id" name="user_id"
                               value="{{Auth::user()->id}}">
                        <input type="hidden" class="form-control" id="completed" name="completed" value="0"
                               placeholder="">
                        {{--Info:addButton--}}
                        {{--<div class="input-group">--}}

                    <!-- /btn-group -->

                        {{--<div class="input-group-btn">--}}
                        <input type="text"  class="form-control" id="body" name="body" placeholder="Add a todo Item"
                               v-model="message"
                        >


                        {{--</div>--}}
                        {{--</div>--}}
                    <!-- /input-group -->
                        {{--Info:addButton end--}}
                    </form>
                    <todolist></todolist>
                </div>

            </div>

        </div>
    </div>



    {{--Info: This is the template for the todolist --}}

    <template id="todolist-template">

        {{--<pre>--}}
        {{--@{{ $data | json }}--}}
        {{--</pre>--}}
<br>

        <ul class="list-group todo-list">


            <li
                    class="list-group-item animated "
                    v-for="todoitem in list"
                    transition="fade"

            >

                {{--CheckBox--}}
                <form method="POST" action="/todolist/@{{ todoitem.id }}" v-ajax>
                    {{method_field('PUT')}}
                    <input type="submit" v-if="todoitem.completed" @click="updateToDoItem(todoitem)"
                    value=""
                    name="completed"
                    class="checkOvalCompleted">

                    <input type="submit" v-else @click="updateToDoItem(todoitem)"
                           value=""
                           name="completed"
                           class="checkOval">
                </form>

                {{--ToDo List's Item text--}}
                <p v-if="todoitem.completed" class="completed text">
                    @{{ todoitem.body }}
                </p>

                <p v-else class="text">
                    @{{ todoitem.body }}
                </p>

                <div class="tools">
                    <button class="fa fa-edit" type="button"
                            data-toggle="modal" data-target="#myModal"
                    @click="editId = todoitem.id "> </button>
                </div>

                <div class="tools">
                    {{--<button class="fa fa-edit"></button>--}}
                    {{--<button class="fa fa-trash-o"></button>--}}

                    {{--Info: the directive v-ajax would make the form asynchronous--}}
                    <form method="POST" action="/todolist/@{{ todoitem.id }}" v-ajax complete="Item Deleted">
                        {{method_field('DELETE')}}
                        <button class="fa fa-trash-o" type="submit" @click="deleteToDoItem(todoitem)" >
                        </button>
                    </form>

                    {{--<button class="fa fa-edit"  type="submit" data-toggle="modal" data-target="#editItemModal">--}}
                    {{--</button>--}}

                </div> {{--End of Tools--}}


            </li>
        </ul>

        {{--Info :Edit Todolist Modal--}}

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">


                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit the to do item</h4>
                    </div>

                    <form  method="POST" action="/updateToDoItem/@{{editId}}">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="modal-body">

                            <input type="hidden" class="form-control" id="user_id" name="user_id"
                                   value="{{Auth::user()->id}}">
                            <input type="hidden" class="form-control" id="completed" name="completed" value="0"
                                   placeholder="">
                            {{--Info:addButton--}}

                            <input type="text" class="form-control" id="body" name="body" placeholder="Edit the Item"
                                   v-model="editMessage" value=""
                            >


                            {{--Info:addButton end--}}
                        </div>


                        <div class="modal-footer">
                            <button type="submit" style="margin-left: 35px"
                                    class="btn btn-info animated"
                                    v-show="editMessage"
                                    transition="fade"
                            >
                                Edit Item
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        {{--Info Edit Modal End--}}

    </template>


@stop()

@section('dash.scripts')

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.js"></script>--}}
    {{--<script src="/js/vueajx.js"></script>--}}
    {{--<script src="../js/vue-resource.js"></script>--}}

    <script src="../js/VueScripts/taskprogress-vue.js"></script>
    <script src="../js/AdminApp.js"></script>




    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables.bootstrap.min.js"></script>
    {{--<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>--}}



    <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@stop