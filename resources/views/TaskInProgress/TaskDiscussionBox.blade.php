{{--.Task Discussion  box start --}}
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Task Discussion</h3>

        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div><!-- /.box-header -->
    <div class="box-body vue-taskDiscussion">

        <div class="box-body pad">
            {{--Info: Task Discussion section--}}

            <form method="post" action="/taskprogress" enctype="multipart/form-data">
                {{csrf_field()}} {{--This token is used to verify that the
                            authenticated user is the one actually making the requests to the application.--}}

                <textarea class="textareaDetails" name="details" placeholder="Enter your post"
                          style="width: 100%; height: 100px; font-size: 14px;
                                      line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                            </textarea>

                <input type="hidden" class="form-control" id="user_id" name="user_id"
                       placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                <input type="hidden" class="form-control" id="bid_id" name="bid_id"
                       placeholder="" value="{{$taskProgress->bid_id}}">{{--To pass in the Bid's Id--}}

                <button type="submit" class="btn btn-default pull-right"><i class="fa fa-plus"></i>
                    Post
                </button>


            </form>
        </div>

        <ul class="timeline">

            <!-- timeline time label -->
            <li class="time-label">
                {{--<span class="bg-red">--}}
                {{--Requester--}}
                {{--</span>--}}
            </li>
            <!-- /.timeline-label -->

        @foreach($taskDiscussions as $taskDiscussion)


            <!-- timeline item -->
                <li>
                    <!-- timeline icon -->
                    @if($taskProgress->getTaskRequester->id == $taskDiscussion->getUser->id  )
                        {{--If Requester, then show red color --}}
                        <i class="fa fa-user bg-red"></i>

                    @elseif($taskProgress->getTaskWorker->id == $taskDiscussion->getUser->id )
                        <i class="fa fa-user bg-blue"></i>

                    @endif
                    <div class="timeline-item">
                                    <span class="time"><i
                                                class="fa fa-clock-o"></i> {{$taskDiscussion->created_at->diffForHumans()}}</span>

                        <h3 class="timeline-header"><a href="#">{{$taskDiscussion->getUser->name}}</a>
                        </h3>

                        <div class="timeline-body">
                            {!! $taskDiscussion->details !!}
                        </div>

                        @if($user->owns($taskDiscussion))
                            <div class="timeline-footer">
                                <a class="btn btn-warning btn-xs" @click="
                                            tProgressId = {{$taskDiscussion->id}}
                                ,user_id = {{Auth::user()->id}} ,bid_id ={{$taskProgress->bid_id}}
                                "
                                type="button"
                                data-toggle="modal" data-target="#editTaskProgress"
                                >edit</a>


                                <a class="btn btn-danger btn-xs"
                                   data-toggle="modal" data-target="#deleteTaskProgress"
                                @click="tDeleteId = {{$taskDiscussion->id}}"
                                >
                                Delete
                                </a>
                            </div>
                        @endif

                    </div>
                </li>
                <!-- END timeline item -->


            @endforeach

        </ul>

        {{--Info :Edit TaskDiscussion Modal--}}

        <div class="modal fade" id="editTaskProgress" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">


                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit the post</h4>
                    </div>


                    <form method="POST"
                          action="/taskprogress/@{{tProgressId}}">
                        {{csrf_field()}}
                        {{method_field('PUT')}}


                        <div class="modal-body">


                                         <textarea class="textareaDetails" name="details"
                                                   placeholder="Place some text here"
                                                   style="width: 100%; height: 100px; font-size: 14px;
                                      line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                                        </textarea>

                            <input type="hidden" class="form-control" id="user_id" name="user_id"
                                   placeholder="" value="@{{user_id}}">{{--To pass in the user's Id--}}

                            <input type="hidden" class="form-control" id="bid_id" name="bid_id"
                                   placeholder="" value="@{{bid_id}}">{{--To pass in the Bid's Id--}}

                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" class="btn btn-primary"><i
                                        class="fa fa-edit"></i>Edit
                                Post
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

        {{--Info Edit TaskDiscussion End--}}

        {{--Info: TaskDiscussion Delete modal start--}}

        <div class="example-modal">
            <div class="modal modal-danger " id="deleteTaskProgress">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Are you sure ?</h4>
                        </div>
                        <div class="modal-body">
                            <p>This post cannot be recovered</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                                Close
                            </button>


                            {{--Delete--}}
                            <form method="POST" action="/taskprogress/@{{ tDeleteId }}">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button class="btn btn-outline" type="submit">
                                    <i class="fa fa-trash-o"
                                       aria-hidden="true"></i>
                                    Delete Post
                                </button>
                            </form>
                            {{--Delete End--}}


                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
        <!-- /.example-modal -->

        {{--Info:Task DiscussionDelete End--}}

    </div><!-- /.box-body -->

</div><!-- /.Task Discussion box end -->