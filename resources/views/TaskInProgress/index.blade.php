@extends('dashboard')

@section('dash.css')
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../css/animate.css">

    <link rel="stylesheet" href="../css/AdminLTE.css">

    <link rel="stylesheet" href="../css/stripe-connect.css">

    <link rel="stylesheet" type="text/css" href="../css/CssFileScripts/showcase-index.css">


    <style>

        .completed {
            text-decoration: line-through;
            color: #00a65a;
        }

        /* Oval: */
        .checkOval {
            width: 10px;
            height: 15px;
            background: #FFFFFF;
            border: 1px solid #44A6B2;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
            float: left;
            cursor: pointer;
        }

        /* Oval Completed: */
        .checkOvalCompleted {
            width: 10px;
            height: 15px;
            background: #5da86f;
            border: 1px solid #44A6B2;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            border-radius: 50px;
            float: left;
            cursor: pointer;
        }


    </style>

@stop

@section('dash.content')
    @include('includes.message_block')



    <div class="container">
        <div class="row">

            <ul class="nav nav-tabs">
                <li role="presentation" class="active"><a href="/taskprogress">Ongoing tasks</a></li>
                <li role="presentation"><a href="/viewpayments">Payments</a></li>


                {{--<li role="presentation"><a href="#">Messages</a></li>--}}
            </ul>
            <div class="col-md-8 ">
                <br>

                @if(!isset($receiver))
                    {{--Info: Stripe User function--}}
                    <h4 class="">Create Stripe account to receive payments</h4>

                    <a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_9WhKAxtu6s78sgTfWbtfb65dpay2eqqO&scope=read_write"
                       class=" stripe-connect light-blue dark">
                        <span>Connect with Stripe</span> </br>
                    </a>
                    {{--Info: Stripe User function--}}

                @endif

                <?php
                    // When the above connect button is clicked it will connect to stripe server and get a response for the code
                if (isset($_GET['code']))
                { // Redirect w/ code
                    $code = "s";
                    $code = $_GET['code'];

                    $token_request_body = array(
                            'grant_type' => 'authorization_code',
                            'client_id' => 'ca_9WhKAxtu6s78sgTfWbtfb65dpay2eqqO', // Gogetter Client Id
                            'code' => $code,
                            'client_secret' => 'sk_test_5u44L34Lqh3wRR1F8v5B1nVV'// Gogetter Secret key for stripe
                    );

                    // Making a  request to stripe OAuth token endpoint to fetch the user’s
                    // authorization credentials,to store them on the site.
                    $req = curl_init('https://connect.stripe.com/oauth/token');
                    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($req, CURLOPT_POST, true);
                    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));

                    // Get response from the stripe server
                    $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
                    $resp = json_decode(curl_exec($req), true);
                    curl_close($req);
                }
                ?>

                @if(isset($_GET['code']))

                    <form action="/ReceiveAccount" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" class="form-control"
                               id="user_id" name="user_id" value="{{Auth::user()->id}}">

                        <input type="hidden" class="form-control"
                               id="access_token" name="access_token" value="{{$resp['access_token']}}">

                        <input type="hidden" class="form-control"
                               id="livemode" name="livemode" value="{{$resp['livemode']}}">

                        <input type="hidden" class="form-control"
                               id="refresh_token" name="refresh_token" value="{{$resp['refresh_token']}}">

                        <input type="hidden" class="form-control"
                               id="token_type" name="token_type" value="{{$resp['token_type']}}">

                        <input type="hidden" class="form-control"
                               id="stripe_publishable_key" name="stripe_publishable_key"
                               value="{{$resp['stripe_publishable_key']}}">

                        <input type="hidden" class="form-control"
                               id="stripe_user_id" name="stripe_user_id" value="{{$resp['stripe_user_id']}}">

                        <input type="hidden" class="form-control"
                               id="scope" name="scope" value="{{$resp['scope']}}">

                        <h4 class="">Submit generated stripe account details to view the task progresses </h4>
                        <button class="btn btn-info" type="submit">Submit</button>

                    </form>
                @endif
                {{--INFO: TESTING--}}

                <div class="page-header">
                    <h2 class="">Ongoing Tasks </h2>
                </div>

                <table id="myTable" class="table">
                    <thead>
                    <tr>
                        <th>Task Name</th>

                        <th class="tableTaskDetails">Task Details</th>
                        <th></th>
                        {{--<th class="tableTaskSkills">Skills Required</th>--}}

                        {{-- <th>Category</th> --}}


                    </tr>
                    </thead>


                    <tbody>


                    @foreach($acceptedBids as $bid)

                        {{-- Info: Get Requesters and Workers For the Task --}}
                        @if( ($bid->user_id == Auth::user()->id) || ($bid->getTask->user_id == Auth::user()->id) )

                            <tr>
                                <td><b>{{$bid->getTask->topic}}</b>
                                    @if(($bid->status == 5))
                                        <span class=" label label-success">This Task is Completed</span>
                                    @endif
                                    @if(($bid->user_id == Auth::user()->id))
                                        <p><span class=" label bg-maroon">Task you are working on</span></p>
                                    @endif
                                    @if($bid->getTask->user_id == Auth::user()->id)
                                        <p><span class=" label bg-purple">Task you requested</span></p>

                                    @endif
                                </td>

                                <td>{{$bid->getTask->details}} </td>
                                <td>
                                    <a href="{{$bid->taskProgressPath()}}" class="btn btn-primary">View</a>
                                </td>

                            </tr>

                        @endif


                    @endforeach

                    </tbody>
                </table>

            </div>

            <div class="col-md-4">


                <div class="vueToDo ">
                    {{csrf_field()}}

                    <form method="POST" action="/todolist">
                        {{--{{method_field('POST')}}--}}
                        <h3>My To-do list items

                            <button type="submit" style="margin-left: 35px"
                                    class="btn btn-info animated"
                            @click="addItem()"
                            v-show="message"
                            transition="fade"
                            >
                            Add To-do Item
                            </button>
                        </h3>
                        {{csrf_field()}}

                        <input type="hidden" class="form-control" id="user_id" name="user_id"
                               value="{{Auth::user()->id}}">
                        <input type="hidden" class="form-control" id="completed" name="completed" value="0"
                               placeholder="">
                        {{--Info:addButton--}}

                        <input type="text" class="form-control" id="body" name="body" placeholder="Add a todo Item"
                               v-model="message"
                        >
                        {{--Info:addButton end--}}
                    </form>
                    <todolist></todolist> {{--To-do list vue component--}}
                </div>

            </div>

        </div>
    </div>



    {{--Info: This is the template for the todolist --}}

    <template id="todolist-template">

        <br>

        <ul class="list-group todo-list">


            <li
                    class="list-group-item animated "
                    v-for="todoitem in list"
                    transition="fade"

            >

                {{--CheckBox--}}
                <form method="POST" action="/todolist/@{{ todoitem.id }}" v-ajax>
                    {{method_field('PUT')}}
                    <input type="submit" v-if="todoitem.completed" @click="updateToDoItem(todoitem)"
                    value=""
                    name="completed"
                    class="checkOvalCompleted">

                    <input type="submit" v-else @click="updateToDoItem(todoitem)"
                           value=""
                           name="completed"
                           class="checkOval">
                </form>

                {{--ToDo List's Item text--}}

                <p v-if="todoitem.completed" class="completed text">
                    <span >@{{ todoitem.body }}</span>
                </p>

                <p v-else class="text">
                    <span >@{{ todoitem.body }}</span>
                </p>


                <div class="tools">
                    <button class="fa fa-edit" type="button"
                            data-toggle="modal" data-target="#myModal"
                    @click="editId = todoitem.id "> </button>
                </div>

                <div class="tools">
                    {{--Info: the directive v-ajax would make the form asynchronous--}}
                    <form method="POST" action="/todolist/@{{ todoitem.id }}" v-ajax complete="Item Deleted">
                        {{method_field('DELETE')}}
                        <button class="fa fa-trash-o" type="submit" @click="deleteToDoItem(todoitem)" >
                        </button>
                    </form>
                </div> {{--End of Tools--}}


            </li>
        </ul>


        {{--Info :Edit Todolist Modal--}}

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">


                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit the to do item</h4>
                    </div>

                    <form  method="POST" action="/updateToDoItem/@{{editId}}">
                        {{csrf_field()}}
                        {{method_field('PUT')}}

                        <div class="modal-body">

                            <input type="hidden" class="form-control" id="user_id" name="user_id"
                                   value="{{Auth::user()->id}}">
                            <input type="hidden" class="form-control" id="completed" name="completed" value="0"
                                   placeholder="">
                            {{--Info:addButton--}}

                            <input type="text" class="form-control" id="body" name="body" placeholder="Edit the Item"
                                   v-model="editMessage" value=""
                            >


                            {{--Info:addButton end--}}
                        </div>


                    <div class="modal-footer">
                        <button type="submit" style="margin-left: 35px"
                                class="btn btn-info animated"
                                v-show="editMessage"
                                transition="fade"
                        >
                            Edit Item
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </form>

                </div>
            </div>
        </div>

        {{--Info Edit Modal End--}}

    </template>

@stop()

@section('dash.scripts')

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.js"></script>--}}
    {{--<script src="/js/vueajx.js"></script>--}}
    {{--<script src="../js/vue-resource.js"></script>--}}

    <script src="../js/VueScripts/taskprogress-vue.js"></script>
    <script src="../js/AdminApp.js"></script>




    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false}


                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false}

                ],


            });

        });

    </script>
@stop