@extends('layouts.app')

@section('css.header')

    {{--<link rel="stylesheet" href="/css/dropzone.css">--}}
    {{--<link rel="stylesheet" href="/css/lity.min.css">--}}

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="../css/animate.css">

    {{--<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>--}}
    <link rel="stylesheet" href="../css/AdminLTE.css">

    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../css/bootstrap3-wysihtml5.css">

    <style>
        .completed {
            text-decoration: line-through;
            color: #00a65a;
        }

    </style>
@stop



@section('content')
    @include('includes.message_block')

    {{--Edit Item Modal Start--}}

    <script type="text/javascript"
            src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script>
        webshim.setOptions("forms-ext", {
            "date": {
                "startView": 2,
                "calculateWidth": false,
                "classes": "show-uparrow"
            }
        });

        webshims.polyfill('forms forms-ext');
        $.webshims.formcfg = {
            en: {
                dFormat: '-',
                dateSigns: '-',
                patterns: {
                    d: "dd-mm-yy"
                }
            }
        };
    </script>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Enter the
                        Milestone</h4>
                </div>
                <form method="POST"
                      action="/taskprogress/{{$taskProgress->bid_id}}/milestone">
                    {{csrf_field()}}

                    <div class="modal-body">

                        <label for="milestone">Enter the Milestone</label>
                        <input type="text" class="form-control" id="milestone"
                               name="milestone"
                               placeholder="" value="{{old('milestone')}}" maxlength="14">
                        <input type="hidden" class="form-control" id="user_id"
                               name="user_id"
                               placeholder=""
                               value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                        <input type="hidden" class="form-control" id="bid_id"
                               name="bid_id"
                               placeholder=""
                               value="{{$taskProgress->bid_id}}">{{--To pass in the user's Id--}}

                        <?php $ldate = date('Y-m-d');?>
                        <label for="milestone">Enter the date the milestone should
                            be complete
                            by</label>
                        <input type="date" min="{{$ldate}}" class="form-control"
                               id="complete_by" value="{{old('complete_by')}}"
                               name="complete_by" placeholder="DD-MM-YY" required>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary"><i
                                    class="fa fa-edit"></i>Enter
                            Milestone
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    {{--Edit Item Modal End--}}



    <div class="container">

        <section class="content-header">
            <h1>
                Task Progress
                <small>{{$taskProgress->getTaskBid->getTask->topic}}</small>
                <div>
                    <p>
                        <small>Requester: {{$taskProgress->getTaskRequester->name}}</small>
                        <small style="margin-left: 900px">Worker: {{$taskProgress->getTaskWorker->name}}</small>
                    </p>
                </div>

            </h1>

            <ol class="breadcrumb">
                <li><a href="http://gogetter:8000/taskprogress"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="#">TaskProgress</li>
            </ol>
            {{--<p></p>--}}
        </section>

        {{--Info: Stripe Payment function--}}
        @include('TaskInProgress.StripeMakePayment')
        {{--Info: Stripe Payment function End--}}




        {{--Milestone Row--}}
        <div class="row vueMilestone">

            {{--Info: Milestones section--}}
            {{--@if($taskProgress->getTaskRequester->id == Auth::user()->id  )   --}}{{--If Requester, then show milestones option --}}
            <milestone list="{{json_encode($milestones)}}"></milestone>
            {{--@endif--}}

        </div><!-- End of Milestone Row -->


        <div class="row">
            {{--Info: Task Discussion Component--}}
            @include('TaskInProgress.TaskDiscussionBox')
        </div><!-- End of Row -->


    </div><!-- End of container-->

    {{--Info: Milestone Component--}}
    @include('TaskInProgress.MilestoneTemplate')



@stop



@section('scripts.footer')

    <script src="../js/VueScripts/taskprogress-show.js"></script>

    {{--<script src="/js/dropzone.js"></script>--}}
    {{--<script src="/js/lity.js"></script>--}}
    {{--<script src="/js/jquery.min.js" crossorigin="anonymous"></script>--}}

    <script src="../js/AdminApp.js"></script>

    <!--&lt;!&ndash; Bootstrap WYSIHTML5 &ndash;&gt;-->
    <script src="../js/bootstrap3-wysihtml5.all.js"></script>

    <script>

        if ($('[type="date"]').prop('type') != 'date') {
            $('[type="date"]').datepicker();
        }

        //    if ( $('#deliver_in')[0].type != 'date' ) $('#deliver_in').datepicker();
    </script>

    <script>
        $('.textareaDetails').wysihtml5();
    </script>


    <script>


        //Vue model
        new Vue({

            el: '.vue-taskDiscussion',

            data: {
                tProgressId: 0,
                bid_id: 0,
                user_id: 0,
                tDeleteId: 0,
            },

        });


    </script>

@stop