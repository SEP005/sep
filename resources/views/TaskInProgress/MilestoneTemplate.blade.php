<template id="milestone-template">
<?php
    $ONE = 1;
    $TWO = 2;
    $FIVE = 5;
    $EIGHT = 8;
    ?>

    {{--.Milestone box start --}}
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Milestones</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->


        <div class="box-body">

            {{--TODO--}}
            <style>
                /* Form Progress */
                .progress {
                    width: 1000px;
                    height: 100px;

                    margin: 20px auto;
                    text-align: center;
                }

                .progress .circle,
                .progress .bar {
                    display: inline-block;
                    background: #fff;
                    width: 40px;
                    height: 40px;
                    border-radius: 40px;
                    border: 1px solid #d5d5da;
                }

                .progress .bar {
                    position: relative;
                    width: 80px;
                    height: 6px;
                    top: -33px;
                    margin-left: -5px;
                    margin-right: -5px;
                    border-left: none;
                    border-right: none;
                    border-radius: 0;
                }

                .progress .circle .label {
                    display: inline-block;
                    width: 32px;
                    height: 32px;
                    line-height: 32px;
                    border-radius: 32px;
                    margin-top: 3px;
                    color: #b5b5ba;
                    font-size: 17px;
                }

                .progress .circle .title {
                    color: #b5b5ba;
                    font-size: 13px;
                    line-height: 30px;
                    margin-left: -5px;
                }

                /* Done / Active */
                .progress .bar.done,
                .progress .circle.done {
                    background: #eee;
                }

                .progress .bar.active {
                    background: linear-gradient(to right, #EEE 40%, #FFF 60%);
                }

                .progress .circle.done .label {
                    color: #FFF;
                    background: #8bc435;
                    box-shadow: inset 0 0 2px rgba(0, 0, 0, .2);
                }

                .progress .circle.done .title {
                    color: #444;
                }

                .progress .circle.active .label {
                    color: #FFF;
                    background: #0c95be;
                    box-shadow: inset 0 0 2px rgba(0, 0, 0, .2);
                }

                .progress .circle.active .title {
                    color: #0c95be;
                }
            </style>


            <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
            {{--Info: Progress Bar Start--}}
            <div class="progress">
                <?php $count = $ONE?>
                <?php $finished = $ONE?>
                @foreach($milestones as $milestone)

                    @if($milestone->status == $TWO)
                        {{--If milestone status = 2 mark milestone is complete--}}
                        <div class="circle done">

                            <?php $finished++?>

                            @elseif($milestone->status == $ONE)
                                {{--If milestone status == 1 mark milestone is uncomplete--}}
                                <div class="circle active"

                                     @if($milestone->complete_by == date('Y-m-d') )
                                     style="background: yellow"
                                     @endif
                                >
                                    @endif
                                    <span class="label">{{$count}}</span>
                                    <span class="title" style="white-space: nowrap;">{{$milestone->milestone }}</span>
                                </div>
                                {{--</div>--}}

                                @if($milestone->status == $TWO)
                                    <span class="bar done"></span>
                                @elseif($milestone->status == $ONE)
                                    <span class="bar half"></span>
                                @endif
                                <?php $count++?> {{-- TO count the milestone--}}



                                @endforeach
                                @if($finished == $count)
                                    {{--If milestone finsished  == count all milstones are complete--}}
                                    <div class="circle done">
                                        @elseif($finished != $count)
                                            <div class="circle">

                                                @endif

                                                {{--Check Mark Shown if finished--}}
                                                <span class="label ">&#10003</span>
                                                <span class="title">Finish</span>
                                            </div>
                                    </div>
                        </div>


                        {{--TODO--}}
                        @if($taskProgress->getTaskRequester->id == Auth::user()->id  )
                            {{--Info:If Requester, then show milestones option --}}

                            <ul class="list-group todo-list">

                                {{--Milstone Item--}}
                                <li class="list-group-item animated"
                                    v-for="milestone in list"
                                    transition="fade"
                                >
                                    <!-- drag handle -->

                                    <!-- todo text -->
                        <span v-if="milestone.status == 2" class="completed text">
                            @{{milestone.milestone}}
                        </span>

                        <span v-else class="text">
                            @{{milestone.milestone}}
                        </span>

                                    <!-- Emphasis label -->

                                @if($taskProgress->getTaskBid->status != $FIVE)

                                    <!-- General tools such as edit or delete-->
                                        <div class="tools">
                                            <form method="POST"
                                                  action="/taskprogress/{{$taskProgress->bid_id}}/milestone/@{{milestone.id}}"
                                                  {{--v-ajax--}}
                                                  {{--complete="Milestone Updated"--}}
                                            >
                                                {{csrf_field()}}
                                                {{method_field('PUT')}}

                                                <button type="submit" class="btn btn-xs bg-teal"
                                                        style="float: left"
                                                @click="updateMilestone(milestone)"
                                                name="status"
                                                >Change Milestone status</button>
                                            </form>
                                        </div>

                                        {{--Edit Button--}}
                                        <div class="tools">
                                            <button class="fa fa-edit" type="button"
                                                    data-toggle="modal" data-target="#editModel"
                                            @click="editId = milestone.id ">
                                            </button>
                                        </div>
                                        {{--Edit Button--}}


                                        <div class="tools">
                                            {{--<button class="fa fa-edit"></button>--}}

                                            <button type="button" class="fa fa-trash-o" data-toggle="modal"
                                                    data-target="#deleteMilestoneModal"
                                            @click=" deleteMilestoneId = milestone.id "
                                            >
                                            </button>

                                            {{--<form method="POST"--}}
                                                  {{--action="/taskprogress/{{$taskProgress->bid_id}}/milestone/@{{milestone.id}} "--}}
                                                  {{--v-ajax complete="Milestone Deleted">--}}
                                                {{--{{method_field('DELETE')}}--}}
                                                {{--<button class="fa fa-trash-o" type="submit"--}}
                                                {{--@click="deleteMilestone(milestone)" >--}}
                                                {{--</button>--}}
                                            {{--</form>--}}

                                        </div>

                                        {{--Info: Mileston Delete modal start--}}

                                        <div class="example-modal">
                                            <div class="modal modal-danger " id="deleteMilestoneModal">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title">Are you sure ?</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>This action cannot be recovered</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                                                                Close
                                                            </button>


                                                            {{--Delete--}}
                                                            <form method="POST"
                                                            action="/taskprogress/{{$taskProgress->bid_id}}/milestone/@{{deleteMilestoneId}} "
                                                            {{--v-ajax--}}
                                                                  {{--complete="Milestone Deleted"--}}
                                                            >
                                                                {{csrf_field()}}
                                                            {{method_field('DELETE')}}
                                                            <button id="deleteMilesontbtn" class="btn btn-outline" type="submit"
                                                            @click="deleteMilestone(milestone)"
                                                                {{--data-dismiss="modal"--}}
                                                                >

                                                                <i class="fa fa-trash-o"
                                                                   aria-hidden="true"></i>
                                                                Delete Post

                                                            </button>
                                                            </form>
                                                            {{--Delete End--}}


                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->
                                        </div>
                                        <!-- /.example-modal -->



                                    {{--Info:Milestone Delete Modal End--}}


                                @endif
                                <!-- End of bid status check -->
                                </li>


                            </ul>

                            <div class="box-footer clearfix no-border">
                                @if(count($milestones)< $EIGHT)

                                    <button type="button" class="btn btn-default pull-right" data-toggle="modal"
                                            data-target="#myModal">
                                        <i class="fa fa-plus"></i> Add Milestone
                                    </button>
                                @endif
                            </div>
                        @endif
                        {{--Info:End of If Requester, then show milestones option--}}
            </div><!-- /.box-body -->

        </div><!-- /.Milestone box end -->
    </div><!-- /.Milestone box end -->


    {{--Info :Edit Milestone Modal--}}

    <div class="modal fade" id="editModel" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit the Milestone item</h4>
                </div>


                <form method="POST"
                      action="/taskprogress/{{$taskProgress->bid_id}}/updateMilestone/@{{editId}}">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="modal-body">

                        <label for="milestone">Update the Milestone</label>
                        <input type="text" class="form-control" id="milestone"
                               name="milestone"
                               placeholder="" value="{{old('milestone')}}" maxlength="15">
                        <input type="hidden" class="form-control" id="user_id"
                               name="user_id"
                               placeholder=""
                               value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                        <input type="hidden" class="form-control" id="bid_id"
                               name="bid_id"
                               placeholder=""
                               value="{{$taskProgress->bid_id}}">{{--To pass in the user's Id--}}

                        <?php $ldate = date('Y-m-d');?>
                        <label for="milestone">Enter the new date the milestone should
                            be completed
                            by</label>

                        <input type="date" min="{{$ldate}}" class="form-control"
                               id="complete_by" value="{{old('complete_by')}}"
                               name="complete_by" placeholder="DD-MM-YY" required>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary"><i
                                    class="fa fa-edit"></i>Enter
                            Milestone
                        </button>
                    </div>
                </form>

            </div>
        </div>
    </div>

    {{--Info Edit Milestone End--}}


</template>

