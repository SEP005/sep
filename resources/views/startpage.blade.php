<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <title>

        GoGetter &middot;

    </title>

    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic|Work+Sans:300,400,500,600' rel='stylesheet'
          type='text/css'>
    <link href="assets/css/toolkit-startup.css" rel="stylesheet">
    <link href="assets/css/application-startup.css" rel="stylesheet">


    <style>
        body{margin: 0; padding: 0;}

    </style>
    {{--Test --}}

</head>


<body>


<div class="stage-shelf stage-shelf-right hidden" id="sidebar">
    <ul class="nav nav-bordered nav-stacked">
        <li class="nav-header">Menu</li>
        @if (Auth::guest())
            <li>
                <a href="{{ url('/register') }}">SignUp</a>
            </li>
            <li>
                <a href="{{ url('/login') }}">Login</a>
            </li>
        @else
            <li class="dropdown active">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-expanded="false">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                    </li>
                </ul>
            </li>
        @endif

    </ul>
</div>

<div class="stage" id="stage">

    <div class="block block-inverse block-fill-height app-header"
         style="background-image: url(assets/img/startup-1.jpg);">

        <nav class="navbar navbar-transparent navbar-fixed-top navbar-padded app-navbar p-t-md">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed p-x-0"
                            data-target="#stage" data-toggle="stage" data-distance="-250">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                    @if (Auth::guest())
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <strong style="background: #fff; padding: 12px; border-radius: 4px; color: #28669F;">
                                Go-Getter
                            </strong>
                        </a>
                    @else
                        <a class="navbar-brand" href="{{ url('/home') }}">
                            <strong style="background: #fff; padding: 12px; border-radius: 4px; color: #28669F;">
                                Go-Getter
                            </strong>
                        </a>
                    @endif
                <!-- Branding End -->
                </div>
                <div class="navbar-collapse collapse text-uppercase">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li>
                                <a href="{{ url('/register') }}">SignUp</a>
                            </li>
                            <li>
                                <a href="{{ url('/login') }}">Login</a>
                            </li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>

                </div><!--/.nav-collapse -->
            </div>
        </nav>


        <img class="app-graph" src="assets/img/startup-0.svg">{{-- Background Image--}}


        <div class="block-xs-middle p-b-lg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-md-6">
                        <h1 class="block-title m-b-sm">Be Hired or Post a request for a task to be done!</h1>
                        <p class="lead m-b-md text-muted">No matter how big or small the task is. This Open Community of
                            workers are willing to get the job done</p>
                        <a href="{{url('/workerTasks')}}" class="btn btn-info btn-lg">Get Hired</a>
                        <a href="{{url('/tasks/create')}}" class="btn btn-success btn-lg">Post a request</a>



                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- --------FOOTER-------- --}}

    <div class="block block-inverse app-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 m-b-md">
                    <ul class="list-unstyled list-spaced">
                        <li class="m-b"><h6 class="text-uppercase">About</h6></li>
                        <li class="text-muted">
                            We at GoGetter believe that a request no matter what kind of task it is , would require some
                            help. This Website is an open community where workers and requesters could engage in doing
                            or project, BIG or SMALL ! <a href="mailto: themes@getbootstrap.com">info@gogetter.xyz</a>.
                        </li>
                    </ul>
                </div>
                <div class="col-sm-2 col-sm-offset-1 m-b-md">
                    <ul class="list-unstyled list-spaced">
                        <li class="m-b"><h6 class="text-uppercase">Features</h6></li>
                        <li class="text-muted">Features</li>
                        <li class="text-muted">Examples</li>
                        <li class="text-muted">Tour</li>
                        <li class="text-muted">Real Time Chat</li>
                    </ul>
                </div>
                <div class="col-sm-2 m-b-md">
                    <ul class="list-unstyled list-spaced">
                        <li class="m-b"><h6 class="text-uppercase">Learn More</h6></li>
                        <li class="text-muted">Rich data</li>
                        <li class="text-muted">Simple data</li>
                        <li class="text-muted">Real time</li>
                        <li class="text-muted">Social</li>
                    </ul>
                </div>
                <div class="col-sm-2 m-b-md">
                    <ul class="list-unstyled list-spaced">
                        <li class="m-b"><h6 class="text-uppercase">Support</h6></li>
                        <li class="text-muted">Help</li>
                        <li class="text-muted">Legal</li>
                        <li class="text-muted">Privacy</li>
                        <li class="text-muted">License</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/toolkit.js"></script>
<script src="/assets/js/application.js"></script>
</body>
</html>

