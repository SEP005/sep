@inject('countries', 'App\Http\Utilities\Country')

@extends('layouts.admin')

@section('content')

    <h1>Profile</h1>
    <br/>
<div class="col-md-6">
    <div class="box">
        <form method="post" action="/admin/profile/{{ $user->id }}">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <h3>Name</h3>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label class="control-label">First Name*</label>
                            <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label class="control-label">Last Name*</label>
                            <input type="text" name="lastname" class="form-control" value="{{ $user->lastname }}">
                            @if ($errors->has('lastname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <hr/>
            <h3>Address</h3>
            <div class="form-group">
                <label>Address</label>
                <input type="text" name="address" class="form-control" value="{{ $user->address }}">
            </div>
            <div class="form-group">
                <label>Street</label>
                <input type="text" name="street" class="form-control" value="{{ $user->street }}">
            </div>
            <div class="form-group">
                <label>City</label>
                <input type="text" name="city" class="form-control" value="{{ $user->city }}">
            </div>
            <div class="form-group">
                <label>Country</label>
                <select class="form-control" name="country">
                    <option value="{{ $user->country }}">{{ $user->country }}</option>
                    @foreach($countries::all() as $country)
                        <option value="{{ $country }}">{{ $country }}</option>
                    @endforeach
                </select>
            </div>
            <hr/>
            <h3>Contact Information</h3>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label">Email address</label>
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Website</label>
                        <input type="text" name="website" class="form-control" value="{{ $user->website }}">
                    </div>
                </div>
            </div>

            <hr/>
            <h3>About Yourself</h3>
            <div class="form-group">
                <label>Biographical Info</label>
                <textarea class="form-control" rows="6" name="bio">{{ $user->bio }}</textarea>
                <span class="help-block">Share a little biographical information to fill out your profile. This may be shown publicly.</span>
            </div>
            <hr/>
            <h3>Settings</h3>
            <div class="form-group">
                <label>User Role</label>
                <select class="form-control" name="role_id">
                    <option value="{{ $user->role->id }}">{{ $user->role->name }}</option>
                    @foreach($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
            <br/>
            <button type="submit" class="btn btn-primary">Update Profile</button>
        </form>
    </div>
</div>
    <div class="col-md-6">
        <div class="box">
            <form class="form-horizontal" role="form" method="post" action="/admin/user/resetPassword">
                {{ csrf_field() }}
                <h3>Account Management</h3>
                <br/>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form-group">
                        <label class="col-md-4 control-label">Email address</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ $user->email }}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">New Password</label>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-warning btn-sm">Reset Password</button>
                        <span class="help-block">Password reset link will be sent to the specified email address.</span>
                    </div>

                </div>
            </form>
        </div>
    </div>
@stop
