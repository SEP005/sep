@extends('layouts.admin')

@section('content')

<h1>User Roles</h1>
<div class="col-md-5">
    <div class="box">
        <h3>Edit Role</h3>
        <br/>

        <form method="post" action="/admin/roles/{{ $role->id }}">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="control-label">Name</label>
                <input type="text" name="name" class="form-control" value="{{ $role->name }}">
                @if ($errors->has('name'))
                    <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                @endif
            </div>
            <label>Description</label>
            <textarea class="form-control" name="description" rows="6">{{ $role->description }}</textarea>
            <br/>
            <button type="submit" class="btn btn-primary">Update Role</button>

        </form>
    </div>
</div>

@stop
