@extends('layouts.admin')

@section('content')

    <h1>Bids</h1>

    <br/>

    <div class="box">
        <h3>Bids</h3>
        <table id="dataTable" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Requester</th>
                <th>Task</th>
                <th>Bid Price</th>
                <th>User</th>
                <th>Delivery Date</th>
                <th>Proposal</th>
                <th>Status</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($bids as $bid)
                <tr>
                    <td>{{ $bid->getTask->owner->name }} {{ $bid->getTask->owner->lastname }}</td>
                    <td>{{ $bid->getTask->topic }}</td>
                    <td>{{ $bid->bid_price }}</td>
                    <td>{{ $bid->bidOwner->name }} {{ $bid->bidOwner->lastname }}</td>
                    <td>{{ $bid->deliver_in }}</td>
                    <td>{{ $bid->proposal }}</td>
                    <td>
                        @if($bid->status == 1)
                            Pending
                        @elseif($bid->status == 2)
                            Accepted by Requester
                        @elseif($bid->status == 3)
                            Rejected by Requester
                        @else
                            Disapproved by admin
                        @endif
                    </td>
                    <td>
                        @if($bid->status == 1)
                            <a class="btn btn-warning btn-sm" role="button" href="/admin/bids/{{ $bid->id }}/status">Disapprove</a>
                        @elseif($bid->status == 4)
                            <a class="btn btn-success btn-sm" role="button" href="/admin/bids/{{ $bid->id }}/status">Approve</a>
                        @endif
                    </td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $bid->id }})"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete this worker bid?",
                        text: "All the bids associated with this task will be deleted!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "bids/" + id + "/delete";
                    });
        }
    </script>
@stop
