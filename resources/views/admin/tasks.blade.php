@extends('layouts.admin')

@section('content')

    <h1>Tasks</h1>

    <br/>

    <div class="box">
        <h3>Tasks</h3>
        <table id="dataTable" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Topic</th>
                <th>Details</th>
                <th>User</th>
                <th>Category</th>
                <th>Price</th>
                <th>Skills</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>{{ $task->topic }}</td>
                    <td>{{ $task->details }}</td>
                    <td>{{ $task->owner->name }} {{ $task->owner->lastname }}</td>
                    <td>{{ $task->getCategory->name }}</td>
                    <td>{{ $task->price }}</td>
                    <td>{{ $task->skills }}</td>
                    <td>
                        @if($task->status == 0)
                            <a class="btn btn-warning btn-sm" role="button" href="/admin/tasks/{{ $task->id }}/status">Disapprove</a>
                        @elseif($task->status == 4)
                            <a class="btn btn-success btn-sm" role="button" href="/admin/tasks/{{ $task->id }}/status">Approve</a>
                        @endif
                    </td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $task->id }})"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete this requester task?",
                        text: "Once deleted you wont be able to recover it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "tasks/" + id + "/delete";
                    });
        }
    </script>
@stop
