@extends('layouts.admin')
@section('content')
    <h1>Community Experts: Questions</h1>
    <div class="col-md-12">
        <div class="box">
            <a href="/admin/community/experts/questions" class="btn btn-default btn-sm"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>Back to all Questions</a>
            <h3>
                {{ $data->question }}
                @if($data->active == 1)
                    <form action="/admin/community/experts/question/{{ $data->id }}/status" method="post">
                        {{ csrf_field() }}
                        <button class="btn btn-danger btn-sm pull-right" type="submit">Disapprove</button>
                    </form>
                @else
                    <form action="/admin/community/experts/question/{{ $data->id }}/status" method="post">
                        {{ csrf_field() }}
                        <button class="btn btn-success btn-sm pull-right" type="submit">Approve</button>
                    </form>
                @endif
            </h3>
            <p>{{ $data->information }}</p>
        </div>
    </div>
@stop