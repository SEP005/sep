@extends('layouts.admin')
@section('content')
    <h1>Community Experts: Categories</h1>
    <div class="col-md-5">
        <div class="box">
            <h3>Edit Category</h3>
            <br/>

            <form method="post" action="/admin/community/experts/category/{{ $data->id }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $data->name }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <label>Description</label>
                <textarea class="form-control" name="description" rows="6">{{ $data->description }}</textarea>
                <br/>
                <button type="submit" class="btn btn-primary">Update Category</button>

            </form>
        </div>
    </div>
@stop