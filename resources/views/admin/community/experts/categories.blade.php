@extends('layouts.admin')
@section('content')
    <h1>Community Experts: Categories</h1>

    <div class="col-md-5">
        <div class="box">
            <h3>Add New Category</h3>
            <br/>

            <form method="post" action="/admin/community/experts/category/new">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="control-label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <label>Description</label>
                <textarea class="form-control" name="description" rows="6">{{ old('description') }}</textarea>
                <br/>
                <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i>Add New Category</button>

            </form>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box">
            <h3>Categories</h3>
            <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Category ID</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                @foreach($data as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td><a href="{{ $d->pathEdit() }}">{{ $d->name }}</a></td>
                        <td>{{ $d->description }}</td>
                        <td><a class="btn btn-warning btn-sm" role="button" href="{{ $d->pathEdit() }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></td>
                        <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $d->id }})"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete the category?",
                        text: "Once deleted you wont be able to recover it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "/admin/community/experts/category/" + id + "/delete";
                    });
        }
    </script>
@stop