@extends('layouts.admin')
@section('content')
    <h1>Community Experts: Questions</h1>

    <div class="col-md-12">
        <div class="box">
            <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Question</th>
                    <th>Asked By</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                @foreach($data as $d)
                    <tr style="@if($d->active==0) background-color: #ffaaaa; @endif">
                        <td>{{ $d->question }}</td>
                        <td>{{ $d->owner->name }}</td>
                        <td>
                            @if($d->active == 1)
                                Active
                                @else
                            Disabled
                                @endif
                        </td>
                        <td><a href="/admin/community/experts/question/{{ $d->id }}/edit" class="btn btn-primary btn-sm">View</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    {{--<div class="col-md-12">
        <div class="box">
            <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Question</th>
                    <th>Asked By</th>
                    <th>Reports</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                @foreach($data as $d)
                    <tr>
                        <td>{{ $d->question }}</td>
                        <td>{{ $d->user_id }}</td>
                        <td>{{ $d->qCount }}</td>
                        <td><a class="btn btn-warning btn-sm" role="button" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></td>
                        <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete({{ $d->id }})"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>--}}
    {{--<script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete the category?",
                        text: "Once deleted you wont be able to recover it!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "/admin/community/experts/category/" + id + "/delete";
                    });
        }
    </script>--}}
@stop