@extends('layouts.admin')
@section('content')
    <h1>Community Experts: Answers</h1>

    <div class="col-md-12">
        <div class="box">
            <table id="dataTable" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Answer</th>
                    <th>By</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                @foreach($data as $d)
                    <tr style="@if($d->active==0) background-color: #ffaaaa; @endif">
                        <td>{{ $d->answer }}</td>
                        <td>{{ $d->owner->name }}</td>
                        <td>
                            @if($d->active == 1)
                                Active
                            @else
                                Disabled
                            @endif
                        </td>
                        <td><a href="/admin/community/experts/answer/{{ $d->id }}/edit" class="btn btn-primary btn-sm">View</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop