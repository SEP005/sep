@extends('layouts.admin')
@section('content')
    {{--<div class="col-md-12">
        <h2 class="text-center">Welcome to GoGetter Admin Dashboard!</h2>
    </div>--}}

    <div class="col-md-12" style="height: 40px;"></div>

    <div class="col-md-12">
        <div class="col-md-3">
            <div class="col-md-3" style="color: #FFF;background-color: #00AA88; padding-top: 10px; padding-bottom: 10px;">
                <i class="fa fa-users fa-2x" aria-hidden="true"></i>
            </div>
            <div class="col-md-9" style="background-color: #FFF; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                <h4><strong>{{ $usersCount }}</strong> Members</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-md-3" style="color: #FFF;background-color: #5d548f; padding-top: 10px; padding-bottom: 10px;">
                <i class="fa fa-tasks fa-2x" aria-hidden="true"></i>
            </div>
            <div class="col-md-9" style="background-color: #FFF; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                <h4><strong>{{ $tasksCount }}</strong> Tasks</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-md-3" style="color: #FFF;background-color: #c7254e; padding-top: 10px; padding-bottom: 10px;">
                <i class="fa fa-money fa-2x" aria-hidden="true"></i>
            </div>
            <div class="col-md-9" style="background-color: #FFF; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                <h4><strong>{{ $bidsCount }}</strong> Bids</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-md-3" style="color: #FFF;background-color: #286090; padding-top: 10px; padding-bottom: 10px;">
                <i class="fa fa-usd fa-2x" aria-hidden="true"></i>
            </div>
            <div class="col-md-9" style="background-color: #FFF; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                <h4><strong>{{ $value }}</strong> Value</h4>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-3">
            <div class="col-md-3" style="color: #FFF;background-color: #286090; padding-top: 10px; padding-bottom: 10px;">
                <i class="fa fa-question fa-2x" aria-hidden="true"></i>
            </div>
            <div class="col-md-9" style="background-color: #FFF; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                <h4><strong>{{ $questionCount }}</strong> Questions</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="col-md-3" style="color: #FFF;background-color: #00a65a; padding-top: 10px; padding-bottom: 10px;">
                <i class="fa fa-commenting-o fa-2x" aria-hidden="true"></i>
            </div>
            <div class="col-md-9" style="background-color: #FFF; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                <h4><strong>{{ $answerCount }}</strong> Answers</h4>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="height: 40px;"></div>

    <div class="col-md-12">
        <div class="col-md-4">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse1">Latest Members</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            @foreach($users as $user)
                                <div class="col-md-3 text-center">
                                    @if (Storage::disk('local')->has($user->first_name . '-' . $user->id . '.jpg'))
                                        <img src="{{ route('account.image', ['filename' => $user->first_name . '-' . $user->id . '.jpg']) }}"
                                             alt="" class="img-circle" width="50" height="50">
                                    @else
                                        <img class="img-circle" src="{{$user->getAvatarUrl()}}" width="50" height="50">
                                    @endif
                                    <p><a href="user/{{ $user->name }}"><strong>{{ $user->name }}</strong></a></p>
                                    <p class="text-muted" style="font-size: 10px;">{{ $user->created_at->diffForHumans() }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse2">Recently Added Tasks</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <ul class="list-group">
                            @foreach($tasks as $task)

                                    <li class="list-group-item">
                                        <span class="label label-success pull-right">{{ $task->price }}</span>
                                        <p><a href="tasks/{{ $task->id }}"><strong>{{ $task->topic }}</strong></a></p>
                                        {{--<p class="text-muted" style="font-size: 12px;">{{ str_limit($task->details, 20) }}</p>--}}
                                        <span class="text-muted" style="font-size: 10px;">Posted by: {{ $task->owner->name }}, </span>
                                        <span class="text-muted" style="font-size: 10px;">{{ $task->created_at->diffForHumans() }}</span>
                                    </li>

                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" href="#collapse3">Recent Bids</a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <ul class="list-group">
                                @foreach($bids as $bid)

                                    <li class="list-group-item">
                                        <span class="label label-info pull-right">{{ $bid->bid_price }}</span>
                                        <p><strong>{{ $bid->getTask->topic }}</strong></p>
                                        <span class="text-muted" style="font-size: 10px;">Bid by: {{ $bid->bidOwner->name }}, </span>
                                        <span class="text-muted" style="font-size: 10px;">{{ $bid->created_at->diffForHumans() }}</span>
                                    </li>

                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md12">
        <div class="col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-question" aria-hidden="true"></i><strong>Latest Reported Questions</strong></h3>
                </div>
                <div class="panel-body">
                    <table class="table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Question</th>
                            <th>Reported By</th>
                            <th>Reason</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($reportedQuestions as $d)
                        <tr>
                            <td><a href="/admin/community/experts/question/{{ $d->question_id }}/edit">{{ $d->question->question }}</a></td>
                            <td>{{ $d->owner->name }}</td>
                            <td>{{ $d->reason }}</td>
                        </tr>
                    @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-commenting-o" aria-hidden="true"></i><strong>Latest Reported Answers</strong></h3>
                </div>
                <div class="panel-body">
                    <table class="table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Answer</th>
                            <th>Reported By</th>
                            <th>Reason</th>
                        </tr>
                        </thead>
                        @foreach($reportedAnswers as $d)
                            <tr>
                                <td><a href="/admin/community/experts/answer/{{ $d->answer_id }}/edit">{{ str_limit($d->answer->answer, 40) }}</a></td>
                                <td>{{ $d->owner->name }}</td>
                                <td>{{ $d->reason }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="col-md-12">
        <h3>Quick Links</h3>
        <hr>
    <div class="col-md-3">
        <div class="box">
            <h3><i class="fa fa-users" aria-hidden="true"></i><a href="admin/users">Users</a></h3>
            <p>Manage all users</p>
            <ul>
                <li><a href="/admin/users">All Users</a></li>
                <li><a href="/admin/user-new">Add New User</a></li>
                <li><a href="/admin/roles">User Roles</a></li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
            <h3><i class="fa fa-sitemap" aria-hidden="true"></i><a href="admin/categories">Categories</a></h3>
            <p>Manage all categories</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
            <h3><i class="fa fa-tasks" aria-hidden="true"></i><a href="admin/tasks">Requester Tasks</a></h3>
            <p>Manage all requester tasks posted by the users</p>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box">
            <h3><i class="fa fa-money" aria-hidden="true"></i><a href="admin/comments">Worker Bids</a></h3>
            <p>Manage all comments</p>
        </div>
    </div>
    </div>--}}
@stop