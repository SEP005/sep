@extends('layouts.admin')

@section('content')

    <h1>Users</h1>

    <br/>

    <div class="box">
        <h3>Users</h3>
        <table id="dataTable" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Address</th>
                <th>Street</th>
                <th>City</th>
                <th>Country</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role->name }}</td>
                    <td>{{ $user->address }}</td>
                    <td>{{ $user->street }}</td>
                    <td>{{ $user->city }}</td>
                    <td>{{ $user->country }}</td>
                    <td><a class="btn btn-warning btn-sm" role="button" href="{{ $user->pathEdit() }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a></td>
                    <td><a class="btn btn-danger btn-sm" role="button" onclick="confirmDelete('{{ $user->id }}', '{{ $user->first_name }}', '{{ $user->last_name }}')"><i class="fa fa-trash" aria-hidden="true"></i>Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        function confirmDelete(id) {
            swal({
                title: "Are you sure you want to delete this user?",
                text: "Once deleted you wont be able to recover it!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location.href = "user/" + id + "/delete";
            });
        }
    </script>
@stop
