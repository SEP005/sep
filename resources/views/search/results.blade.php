@extends('layouts.app')

@section('content')
    <div class ='row'>
        <div class="col-md-10 col-md-offset-1">
            <h3> Your Search result for "{{ Request::input('query') }}"</h3>

            <div class="col-md-10">
                <div class="row jumbotron" style="background-color: white">
                    <ul class="tab">
                        <li><a href="javascript:void(0)" class="tablinks active" id="defaultOpen" onclick="openCity(event, 'Timeline')">User</a></li>
                        <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Connections')">Tasks</a></li>
                    </ul>

                    <div id="Timeline" class="tabcontent ">
                        <h3 class="col-md-offset-1" style="visibility: hidden">Users</h3>
                            @if(!$users->count())
                                <p>No result found, Sorry.</p>
                            @else
                                @foreach($users as $user)
                                    @include('user/partials/userblock')
                                @endforeach
                            @endif
                    </div>

                    <div id="Connections" class="tabcontent col-md-offset-1">
                        <h3 class="col-md-offset-1" style="visibility: hidden">Tasks</h3>
                        <div class="form-group ">
                                <div class="container">
                                    <div class="row">
                                        <table id="myTable" class="table">
                                            <thead>
                                            <tr>
                                                <td>Task Id</td>
                                                <td class="tableTaskTopic">Topic</td>
                                                <td>Details</td>
                                                <td class="tableTaskSkills">Skills Required</td>
                                                {{-- <th>Category</th> --}}
                                                <td class="tableTaskPrice">Price</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($bidTasks as $task)
                                                @if($task->status != 4)
                                                    <tr>
                                                        <td><a href="{{$task->path()}}">{{$task->id}} </a></td>
                                                        <td class="tableTaskTopic"><a href="{{$task->path()}}">{{$task->topic}} </a></td>
                                                        <td><p class="tableTaskDetails"> <b>{{$task->details}}</b><p></td>
                                                        <td class="tableTaskSkills"> {{$task->skills}}</td>
                                                        {{--<td>{{$task->category_id}}</td>--}}
                                                        <td class="tableTaskPrice">{{$task->price}}</td>
                                                        <td>
                                                            <a href="/tasks/{{$task->id}}/bids/create" class="btn btn-sm btn-success">Bid
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <script>
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
        function openCity(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
@endsection
