{{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}



        {{--
        Paid TO you
        Deliver in
        Proposal

        Project Details
        --}}
        <hr>

        <div class="form-group">
            <i class="fa fa-money " aria-hidden="true"></i>

            <label for="bid_price" >Bid Price</label>

            <input type="text" class="form-control" id="bid_price"
                   value="{{old('bid_price')}}"  name="bid_price" placeholder="The Amount to be paid for you .." required>

            <input type="hidden" class="form-control" id="user_id"
                   name="user_id" placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

            <input type="hidden" class="form-control" id="task_id"
                   name="task_id" placeholder="" value="{{$task->id}}">{{--To pass in the task's Id--}}

        </div>


        <hr>
        <div class="form-group">
            <i class="fa fa-calendar " aria-hidden="true"></i>
            <label for="deliver_in">Deliver In</label>

            <input type="date" min="{{$ldate}}"  class="form-control" id="deliver_in" value="{{old('deliver_in')}}"
                   name="deliver_in" placeholder="YY-MM-DD" required>
        </div>
{{--{{$ldate}}--}}




        <div class="form-group">
            <i class="fa fa-comment-o" aria-hidden="true"></i>
            <label for="proposal">Describe your proposal for this task</label>
            <textarea class="form-control"  rows="10" name="proposal" value="{{old('details')}}" required></textarea>
        </div>


        {{--<div class="form-group">--}}
        {{--<i class="fa fa-picture-o" aria-hidden="true"></i>--}}

        {{--<label for="photo">Add pictures to support the task request</label>--}}
        {{--<input type="file" class="form-control" id="photo"  name="photo" >--}}
        {{--</div>--}}


        <hr>


        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-primary">
                <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>
                Place Bid
            </button>
        </div>


    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <! Will be used to display an alert to the user>
        </div>
    </div>

<script>

    if ( $('[type="date"]').prop('type') != 'date' ) {
        $('[type="date"]').datepicker();
    }

//    if ( $('#deliver_in')[0].type != 'date' ) $('#deliver_in').datepicker();
</script>


