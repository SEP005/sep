@extends('layouts.app')

@section('css.header')



@stop



@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-9 ">


                <h1>Bid Amount: {{$bid->bid_price}}</h1>
                <h3><span class="help-block">Proposed Deliver Date: {{$bid->deliver_in}}</span></h3>
                <h4><span class="help-block">Bid by {{$bid->bidOwner->name}}</span></h4>


                <hr>

                <div class="description">
                    {!!  nl2br($bid->proposal) !!}  {{-- nl2br:To preserve the spaces --}}
                </div>

            </div> {{--End Of Col--}}

            <div class="col-md-3">
            @if($user && $user->id != ($bid->bidOwner->id))
                <br>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Accept/Reject
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="acceptBid"  class="btn btn-success">Accept Bid<span class="badge"></span></a></li>
                            <li><a href="#" id="rejectBid" class="btn btn-danger">Reject Bid<span class="badge"></span></a></li>
                        </ul>
                    </div>
            @endif
            </div>

            <form id="acceptBidForm"
                  action="{{route('store_select_bid',[$bid->id])}}"
                  method="post"
            >
                {{csrf_field()}}
                <input type="hidden" class="form-control" id="status" name="status"
                       placeholder="" value="2">{{--To pass in the user's Id--}}
                <input type="hidden" class="form-control" id="user_id" name="user_id"
                       placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                <input type="hidden" class="form-control" id="id" name="id"
                       placeholder="" value="{{$bid->id}}">{{--To pass in the submission's Id--}}
            </form>

            <form id="rejectBidForm"
                  action="{{route('store_select_bid',[$bid->id])}}"
                  method="post"
            >
                {{csrf_field()}}
                <input type="hidden" class="form-control" id="status" name="status"
                       placeholder="" value="3">{{--To pass in the user's Id--}}
                <input type="hidden" class="form-control" id="user_id" name="user_id"
                       placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                <input type="hidden" class="form-control" id="id" name="id"
                       placeholder="" value="{{$bid->id}}">{{--To pass in the submission's Id--}}
            </form>

            {{--Badge End--}}



        </div>{{--End Of Row--}}


        <br>

    </div>


@stop



@section('scripts.footer')

    <script>
        $('#acceptBid').on('click', function () {
            $("#acceptBidForm").submit();
        });
        $('#rejectBid').on('click', function () {
            $("#rejectBidForm").submit()});
    </script>

@stop