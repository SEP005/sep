@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')
    @include('includes.delete_confirm')

    <div class="container">
        <div class="row">

            <div class="page-header">
                <h2 class="">Bids made by you</h2>
            </div>

            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th>Bid Id</th>

                    <th>Proposal</th>
                    <th class="tableTaskSkills">Deliver Date</th>

                    {{-- <th>Category</th> --}}
                    <th class="tableTaskPrice">Bid Price</th>

                    <th>Bid Status</th>
                    <th></th>
                    <th></th>

                </tr>
                </thead>


                <tbody>

                {{-- View Bids , Show Bid --}}
                @foreach($workerBids as $bid)

                    <tr>
                        {{-- tasks/{tasks}/bids/{bids} --}}
                        <td><a href="tasks/{{$bid->getTask->id}}/bids/{{$bid->id}}">{{$bid->id}} </a></td>

                        <td>

                            <p class="tableTaskDetails"> {{$bid->proposal}}<p>

                        </td>
                        <td class="tableTaskSkills"> {{$bid->deliver_in}}</td>

                        {{--<td>{{$task->category_id}}</td>--}}
                        <td class="tableTaskPrice">{{$bid->bid_price}}</td>

                        <td class="tableTaskPrice">

                            {{--Info: Check the bid status--}}
                        @if($bid->status == 1)
                                Bid In Review </td>
                        @elseif($bid->status == 2)
                                Bid Accepted!</td>
                        @elseif($bid->status == 3)
                            Bid Rejected!</td>
                        @elseif($bid->status == 4)
                            Disapproved by Administrator!</td>
                        @elseif($bid->status == 5)
                            <span class=" label label-success">This Task is Completed</span>
                            </td>
                        @endif


                        <td>
                            @if($bid->status != 5)

                            {{-- TO edit a bid--}}
                            <a href="#" id="bidEdit" class="btn btn-sm btn-warning"
                                @click="workerBidId = {{$bid->id}},
                                 workerBidPrice = {{$bid->id}},
                                 workerBidDeliver = {{$bid->deliver_in}},
                                 workerBidProposal = {{$bid->proposal}},
                                 "
                                >Edit
                            </a>
                                @endif
                        </td>

                        <td>
                            {{-- TO delete a bid
                            tasks/{tasks}/bids/{bids}
                            --}}
                            @if($bid->status != 5)

                            <a href="/tasks/{{$bid->getTask->id}}/bids/{{$bid->id}}" class="btn btn-danger"
                               data-method="delete" data-token="{{csrf_token()}}"
                               data-confirm="Are you sure you want to delete the bid?">Delete</a>
                            </a>
                            @endif

                        </td>



                    </tr>

                    <!--Modal start -->
                    <div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">


                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Edit Bid</h4>
                                    <h6 class="modal-body">
                                        <span class="help-block">Note: If your bid status is accepted , editing the bid will
                                        result in the bid status being changed to Bid in Review</span></h6>

                                </div>
                                <div class="modal-body">
                                    <form id="bidEditForm" action="{{route('bid.edit',[$bid->getTask->id,$bid->id])}}"
                                          method="post">
                                        {{csrf_field()}} {{--This token is used to verify that the
                                                                        authenticated user is the one actually making
                                                                         the requests to the application.--}}
                                        <input type="hidden" name="_method" value="PUT">

                                        <div class="form-group">
                                            <label for="bid_price" >Bid Price</label>

                                            <input type="text" class="form-control" id="bid_price"
                                                   value="{{$bid->getActualPrice()}}"  name="bid_price"
                                                   placeholder="The Amount to be paid for you .." required>

                                            <input type="hidden" class="form-control" id="user_id"
                                                   name="user_id" placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                                            <input type="hidden" class="form-control" id="task_id"
                                                   name="task_id" placeholder="" value="{{$bid->getTask->id}}">{{--To pass in the task's Id--}}

                                        </div>

                                        <hr>
                                        <div class="form-group">
                                            <i class="fa fa-calendar " aria-hidden="true"></i>
                                            <label for="deliver_in">Deliver In</label>
                                            <input type="date"  class="form-control" id="deliver_in" value="{{$bid->deliver_in}}"
                                                   name="deliver_in" placeholder="Task Completion date.." required>
                                        </div>

                                        <div class="form-group">
                                            <i class="fa fa-comment-o" aria-hidden="true"></i>
                                            <label for="proposal">Describe your proposal for this task</label>
                                            <textarea class="form-control"  rows="10" name="proposal" value="" required>{{$bid->proposal}}</textarea>
                                        </div>
                                        <div class="form-group">

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary" id="modal-save">Save changes</button>
                                        </div>
                                             </div>

                                    </form>

                                </div>



                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    {{-- Modal End--}}
                @endforeach

                </tbody>


            </table>


        </div>
    </div>
    </div>
@endsection

@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>

        // Delete Script

        (function() {

            var laravel = {
                initialize: function() {
                    this.methodLinks = $('a[data-method]');
                    this.token = $('a[data-token]');
                    this.registerEvents();
                },

                registerEvents: function() {
                    this.methodLinks.on('click', this.handleMethod);
                },

                handleMethod: function(e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;

                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }

                    // Allow user to optionally provide data-confirm="Are you sure?"
                    if ( link.data('confirm') ) {
                        if ( ! laravel.verifyConfirm(link) ) {
                            return false;
                        }
                    }

                    form = laravel.createForm(link);
                    form.submit();

                    e.preventDefault();
                },

                verifyConfirm: function(link) {
                    return confirm(link.data('confirm'));
                },


                createForm: function(link) {
                    var form =
                            $('<form>', {
                                'method': 'POST',
                                'action': link.attr('href')
                            });

                    var token =
                            $('<input>', {
                                'type': 'hidden',
                                'name': '_token',
                                'value': link.data('token')
                            });

                    var hiddenInput =
                            $('<input>', {
                                'name': '_method',
                                'type': 'hidden',
                                'value': link.data('method')
                            });

                    return form.append(token, hiddenInput)
                            .appendTo('body');
                }
            };

            laravel.initialize();

        })();
    </script>

    <script>
        //Vue model
        new Vue({

            el: '.workerBids',

            data: {
                workerBidId: 0,
                workerBidPrice: 0,
                workerBidDeliver: 0,
                workerBidProposal: 0,
            },

        });
    </script>

        <script>
        $('#bidEdit').on('click', function () {

            $('#edit-modal').modal();
        });
    </script>


@stop