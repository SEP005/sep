@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">

    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script>
        webshim.setOptions("forms-ext", {
            "date": {
                "startView": 2,
                "calculateWidth": false,
                "classes": "show-uparrow"
            }
        });

        webshims.polyfill('forms forms-ext');
        $.webshims.formcfg = {
            en: {
                dFormat: '-',
                dateSigns: '-',
                patterns: {
                    d: "dd-mm-yy"
                }
            }
        };
    </script>



@stop

@section('content')

    @include('includes.message_block')


    <div class="container">

        <h1>Make a bid for the task</h1>
        <div class="row">
            <div class="col-md-8">


        <form method="post" action="/tasks/{{$task->id}}/bids " enctype="multipart/form-data" >

            @include('bids.form')
        </form>
        </div>{{-- End of Coulmn--}}

        </div>{{--End of Row--}}

        <h1>Description of the task</h1>
<hr>
        <div class="row">

            <div class="col-md-6 ">


                <h2>{{$task->topic}}</h2>
                <h2>{{ $task->price }}</h2>

                <hr>

                <div class="description">
                    {!!  nl2br($task->details) !!}  {{-- nl2br:To preserve the spaces --}}
                </div>

            </div> {{--End Of Col--}}


            <div class="col-md-6 gallery">

                @foreach($task->photos->chunk(3) as $set)
                    <div class="row">
                        @foreach($set as $photo)

                            <div class="col-md-4 gallery_image">

                                @if($user && $user->owns($task))

                                    <form method="post" action="/taskphotos/{{$photo->id}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class=" btn btn-sm btn-danger"><i class="fa fa-trash-o"
                                                                                                aria-hidden="true"></i>
                                        </button>

                                    </form>
                                @endif


                                <a href="/{{$photo->path}}" data-lity>

                                    <img src="/{{$photo->thumbnail_path}}" alt="">

                                </a>
                            </div>

                        @endforeach

                    </div>
                @endforeach
                    @if($task->video_path)
                        <iframe width="420" height="315" src="{{$task->video_path}}"
                                frameborder="0" allowfullscreen></iframe>
                    @endif




            </div> {{--End Of Col Gallery--}}


        </div>{{--End Of Row--}}
<hr>
    </div>



@endsection


@section('scripts.footer')

@stop