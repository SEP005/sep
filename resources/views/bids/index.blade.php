@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">



@stop

@section('content')
    @include('includes.delete_confirm')

    <div class="container">
        <div class="row">

            <div class="page-header">
                <h3 class=""><span class="help-block">Bids for the Task: <strong>{{$taskBids[0]->getTask->topic}} </strong>   </span> </h3>
            </div>
            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th>Bid Id</th>

                    <th>Proposal</th>
                    <th class="tableTaskSkills">Deliver Date</th>

                    {{-- <th>Category</th> --}}
                    <th class="tableTaskPrice">Bid Price</th>

                    <th></th>


                </tr>
                </thead>


                <tbody>

{{-- View Bids , Show Bid --}}
                @foreach($taskBids as $bid)

                    <tr>
                        @if($bid->status != 4)

                        {{-- tasks/{tasks}/bids/{bids} --}}
                        <td><a href="/tasks/{{$bid->getTask->id}}/bids/{{$bid->id}}">{{$bid->id}} </a></td>

                        <td><span class="help-block">Bid by: {{$bid->bidOwner->name}}</span>
                            <p class="tableTaskDetails"> {{$bid->proposal}}
                            <p></td>
                        <td class="tableTaskSkills"> {{$bid->deliver_in}}</td>

                        {{--<td>{{$task->category_id}}</td>--}}
                        <td class="tableTaskPrice">{{$bid->bid_price}}</td>


                        <td>
                            {{-- TO show all bids for a task--}}
                            <a href="/tasks/{{$bid->getTask->id}}/bids/{{$bid->id}}" class="btn btn-sm btn-success">View Bid
                            </a>
                        </td>


                        @endif
                    </tr>

                @endforeach

                </tbody>


            </table>


        </div>
    </div>
    </div>
@endsection

@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>

        $('button#delete').on('click', function () {
            swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this task!", type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        $("#deleteform").submit();
                    });
        })

    </script>


@stop