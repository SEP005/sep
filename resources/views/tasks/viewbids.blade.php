@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')
    @include('includes.delete_confirm')

    <div class="container">
        <div class="row">

            <div class="page-header">

                <h2> Bids on the tasks</h2>

            </div>
            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th>Task Id</th>

                    <th class="tableTaskTopic">Topic</th>
                    <th>Details</th>
                    <th class="">Total Bids</th>

                    {{-- <th>Category</th> --}}
                    <th class="tableTaskPrice">Price</th>

                    <th></th>


                </tr>
                </thead>


                <tbody>


                @foreach($tasks as $task)

                    <tr>
                        <td><a href="{{$task->path()}}">{{$task->id}} </a></td>

                        <td class="tableTaskTopic"><a href="{{$task->path()}}">{{$task->topic}} </a></td>
                        <td><p class="tableTaskDetails"> {{$task->details}}<p></td>
                        <td class=""> {{$task->bid_count}}</td>

                        {{--<td>{{$task->category_id}}</td>--}}
                        <td class="tableTaskPrice">{{$task->price}}</td>


                        <td>
                            <a href="/tasks/{{$task->id}}/bids" class="btn btn-sm btn-warning">View Bids
                            </a>
                        </td>




                    </tr>

                @endforeach

                </tbody>


            </table>


        </div>
    </div>
    </div>
@endsection

@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": true},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": true},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>

        (function() {

            var laravel = {
                initialize: function() {
                    this.methodLinks = $('a[data-method]');
                    this.token = $('a[data-token]');
                    this.registerEvents();
                },

                registerEvents: function() {
                    this.methodLinks.on('click', this.handleMethod);
                },

                handleMethod: function(e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;

                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }

                    // Allow user to optionally provide data-confirm="Are you sure?"
                    if ( link.data('confirm') ) {
                        if ( ! laravel.verifyConfirm(link) ) {
                            return false;
                        }
                    }

                    form = laravel.createForm(link);
                    form.submit();

                    e.preventDefault();
                },

                verifyConfirm: function(link) {
                    return confirm(link.data('confirm'));
                },


                createForm: function(link) {
                    var form =
                            $('<form>', {
                                'method': 'POST',
                                'action': link.attr('href')
                            });

                    var token =
                            $('<input>', {
                                'type': 'hidden',
                                'name': '_token',
                                'value': link.data('token')
                            });

                    var hiddenInput =
                            $('<input>', {
                                'name': '_method',
                                'type': 'hidden',
                                'value': link.data('method')
                            });

                    return form.append(token, hiddenInput)
                            .appendTo('body');
                }
            };

            laravel.initialize();

        })();
    </script>


@stop


