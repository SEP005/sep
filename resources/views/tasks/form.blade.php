{{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}
<?php $TOPIC = ''?>
@if(isset($_GET['topic']))
    <?php $TOPIC = $_GET['topic'] .' Task'?>
@endif
<div class="row">

    <div class="col-md-8">

        <div class="form-group">
            <label for="category_id" class=""> Category : </label>

            <select name="category_id" required class="form-control">

                <option>Category</option>


                @foreach($category as $cat)
                    <option value="{{$cat->id}}"><strong>{{$cat->name}}</strong></option>
                        @foreach($cat->subCategory as $firstNestedSub)
                        <option value="{{$firstNestedSub->id}}"> - {{$firstNestedSub->name}}</option>
                    @endforeach
                    {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                @endforeach


            </select>

        </div>

        <hr>

        <div class="form-group">
            <label for="topic" >Topic</label>
            <input type="text" class="form-control" id="topic" name="topic"
                   placeholder="Enter the topic for the request" value="{{$TOPIC}}" required>


            <input type="hidden" class="form-control" id="user_id" name="user_id"
                   placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

        </div>


        <hr>
        <div class="form-group">
            <i class="fa fa-money " aria-hidden="true"></i>
            <label for="price">What's the budget in mind ($)</label>
            <input type="text" class="form-control" id="price" value="{{old('price')}}"  name="price"
                   placeholder="The budget for the task.." required>
        </div>




        <div class="form-group">
            <i class="fa fa-cog" aria-hidden="true"></i>
            <label for="skills">Whats skills are required</label><br>
            <input class="form-control" id="skillTask" name="skills" type="text"
                   placeholder="Enter skills required"
                   value="{{old('skills')}}" data-role="tagsinput">
        </div>

        <div class="form-group">
            <i class="fa fa-comment-o" aria-hidden="true"></i>
            <label for="details">Describe what the task is about</label>
            <textarea class="form-control"  rows="10" name="details" value="{{old('details')}}" required></textarea>
        </div>


        {{--<div class="form-group">--}}
            {{--<i class="fa fa-picture-o" aria-hidden="true"></i>--}}

            {{--<label for="photo">Add pictures to support the task request</label>--}}
            {{--<input type="file" class="form-control" id="photo"  name="photo" >--}}
        {{--</div>--}}

        <div class="form-group">
            <input class="form-control" type="text" name="video_path" id="video_path"
                   placeholder="Embed the video path" class="form-control"
                   value="">
        </div>

    <hr>


    <div class="form-group">
        <a class="btn btn-success " id="btnVideoShow">
            <i class="fa fa-video-camera" aria-hidden="true"></i>
            Upload Video
        </a>

        <button type="submit" class="btn btn-lg btn-primary">
            <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

            Post Task</button>
    </div>

    </div>{{-- End of Coulmn--}}

    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <! Will be used to display an alert to the user>
        </div>
    </div>



</div>{{--End of Row--}}