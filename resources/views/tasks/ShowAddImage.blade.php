@if($user && !$user->owns($task))
    <button type="submit" class="btn btn-lg btn-success">Make A Bid</button>
@endif

@foreach($task->photos->chunk(3) as $set)
    <div class="row">
        @foreach($set as $photo)

            <div class="col-md-4 gallery_image">

                @if($user && $user->owns($task))

                    <form method="post" action="/taskphotos/{{$photo->id}}">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class=" btn btn-sm btn-danger"><i class="fa fa-trash-o"
                                                                                aria-hidden="true"></i>
                        </button>

                    </form>
                @endif


                <a href="/{{$photo->path}}" data-lity>

                    <img src="/{{$photo->thumbnail_path}}" alt="">

                </a>
            </div>

        @endforeach

    </div>
@endforeach

@if($task->video_path)
    <iframe width="420" height="315" src="{{$task->video_path}}"
            frameborder="0" allowfullscreen></iframe>
@endif


{{--@foreach($task->photos as $photo)--}}

{{--<img src="/{{$photo->thumbnail_path}}" alt="">--}}

{{--@endforeach--}}


@if($user && $user->owns($task))
    {{--If We have a user and the user owns the tasks then show--}}

    <hr>
    <form id="addTaskPhotoForm"
          action="{{route('store_task_photo',[$task->id])}}"
          method="post"
          class="dropzone">


        {{csrf_field()}}
    </form>

@endif