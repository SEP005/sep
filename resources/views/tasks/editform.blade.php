{{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}
<input type="hidden" name="_method" value="PUT">



        <div class="form-group">
            <label for="category_id" class=""> Category : </label>

            <select name="category_id" class="form-control">

                <option value="{{$task->category_id}}">{{$task->getCategory->name}}</option>

                {{--@foreach($category as $cat)--}}
                    {{--<option value="{{$cat->id}}">{{$cat->name}}</option>--}}
                    {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                {{--@endforeach--}}

                @foreach($category as $cat)
                    <option value="{{$cat->id}}"><strong>{{$cat->name}}</strong></option>
                    @foreach($cat->subCategory as $firstNestedSub)
                        <option value="{{$firstNestedSub->id}}"> - {{$firstNestedSub->name}}</option>
                    @endforeach
                    {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                @endforeach


            </select>

        </div>

        {{--     <div class="form-group">
                     <label for="name" class="col-sm-2 control-label"></label>
                     <div class="col-sm-10">


                         <select class="form-control">
                             @foreach($category as $subCategory)
                                 <option>{{$subCategory->subCategories[$i]->name}}</option>
                             @endforeach

                             <option>SubCategory</option>
                             <option>1</option>
                             <option>2</option>
                             <option>3</option>
                             <option>4</option>
                             <option>5</option>
                         </select>

                     </div>
                 </div>  --}}


        <hr>

        <div class="form-group">
            <label for="topic" >Topic</label>
            <input type="text" class="form-control" id="topic" name="topic" placeholder="Enter the topic for the request" value="{{$task->topic}}" required>


            <input type="hidden" class="form-control" id="user_id" name="user_id" placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

        </div>


        <hr>
        <div class="form-group">
            <i class="fa fa-money " aria-hidden="true"></i>
            <label for="price">What's the budget in mind</label>
            <input type="text" class="form-control" id="price" value="{{$task->getActualPrice()}}"  name="price" placeholder="The budget for the task.." required>
        </div>




        <div class="form-group">
            <i class="fa fa-cog" aria-hidden="true"></i>
            <label for="skills">Whats skills are required</label><br>
            <input class="form-control" id="skillTask" name="skills" type="text" placeholder="Enter skills required for the task" value="{{$task->skills}}" data-role="tagsinput">
        </div>

        <div class="form-group">
            <i class="fa fa-comment-o" aria-hidden="true"></i>
            <label for="details">Describe what the task is about</label>
            <textarea class="form-control"  rows="10" name="details" value="" required>{{$task->details}}</textarea>
        </div>

        <div class="form-group">
            <input class="form-control" type="text" name="video_path" id="video_path" placeholder="Embed the video path" class="form-control"
                   value="{{$task->video_path}}">
        </div>


        {{--<div class="form-group">--}}
        {{--<i class="fa fa-picture-o" aria-hidden="true"></i>--}}

        {{--<label for="photo">Add pictures to support the task request</label>--}}
        {{--<input type="file" class="form-control" id="photo"  name="photo" >--}}
        {{--</div>--}}


        <hr>


        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-primary">
                <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

                Edit Task</button>
        </div>


    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <! Will be used to display an alert to the user>
        </div>
    </div>



