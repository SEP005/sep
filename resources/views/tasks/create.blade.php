@extends('layouts.app')

@section('css.header')

    {{--<link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">--}}
    <link rel="stylesheet" href="/css/bootstrap-tagsinput.css">


@stop

@section('content')

    @include('includes.message_block')


    <div class="container">

        <h1>Want a task to be done ?</h1>



        <form method="post" action="/tasks" enctype="multipart/form-data" >

        @include('tasks.form')

        </form>




    </div>

@endsection


@section('scripts.footer')
    {{--<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>--}}

    <script src="/js/bootstrap-tagsinput.min.js"></script>


    <script>

        $("#video_path").hide();

        $("#btnVideoShow").click(function(){
            $("#video_path").show();
            $("#btnVideoShow").hide();
        });

    </script>
@stop