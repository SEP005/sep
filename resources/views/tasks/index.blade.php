@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')
    @include('includes.delete_confirm')

    <div class="container">
        <div class="row">
            <div class="page-header">
            <h2 class="">Tasks you have posted</h2>
            </div>
            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th>Task Id</th>

                    <th class="tableTaskTopic">Topic</th>
                    <th>Details</th>
                    {{--<th class="tableTaskSkills">Skills Required</th>--}}

                    {{-- <th>Category</th> --}}
                    <th class="tableTaskPrice">Price</th>
                    {{--Added by Shan--}}
                    {{--<th>Task Assigned to</th>--}}
                  {{--  <th></th>--}}
                    {{--End of added by Shan--}}
                    <th></th>
                    <th></th>


                </tr>
                </thead>


                <tbody>


                @foreach($tasks as $task)

                    <tr>
                        <td><a href="{{$task->path()}}">{{$task->id}} </a></td>

                        <td class="tableTaskTopic">
                            @if($task->status == 4)
                            <span class="help-block">Disapproved by Admin</span>
                            @endif
                            <a href="{{$task->path()}}">{{$task->topic}} </a>
                        </td>
                        <td><p class="tableTaskDetails"> {{$task->details}}<p></td>
                        {{--<td class="tableTaskSkills"> {{$task->skills}}</td>--}}

                        {{--<td>{{$task->category_id}}</td>--}}
                        <td class="tableTaskPrice">{{$task->price}}</td>
                        {{--<td>
                            @foreach($task->bids as $bid)
                                @if($bid->status == 2)
                                {{ $bid->bidOwner->name }}

                                    <a class="btn btn-info btn-sm" role="button" href="#">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i> Send Message</a>
                                @endif
                            @endforeach
                        </td>--}}

                        <td>
                            <a href="/tasks/{{$task->id}}/edit" class="btn btn-sm btn-warning">Edit
                            </a>
                        </td>

                        <td>

                            {{--TODO Check
                              <a href="/tasks/{{$task->id}}" class="btn btn-danger" data-method="delete"
                              data-token="{{csrf_token()}}" data-confirm="Are you sure?">Delete</a> --}}

                            <a class="btn btn-danger" role="button" onclick="confirmDelete({{ $task->id }})">
                                Delete
                                </a>
                            {{--<form id="deleteform" method="POST" action="/tasks/{{$task->id}}" >--}}
                                {{--{{csrf_field()}}--}}
                                {{--<input type="hidden" name="_method" value="DELETE">--}}

                                {{--<button type="submit" id="delete" class="btn btn-sm btn-danger">--}}
                                    {{--<i class="fa fa-trash-o" aria-hidden="true"></i> {{$task->id}}--}}
                                {{--</button>--}}

                            {{--</form>--}}



                        </td>


                    </tr>

                @endforeach

                </tbody>


            </table>


        </div>
    </div>
    </div>
@endsection

@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                   // {"searchable": false},
                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                   // {"sortable": false},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>

//        $('button#delete').on('click', function () {
//            swal({
//                        title: "Are you sure?",
//                        text: "You will not be able to recover this task!", type: "warning",
//                        showCancelButton: true,
//                        confirmButtonColor: "#DD6B55",
//                        confirmButtonText: "Yes, delete it!",
//                        closeOnConfirm: false
//                    },
//                    function () {
//
//                        $("#deleteform").submit();
//                    });
//        })
(function() {

    var laravel = {
        initialize: function() {
            this.methodLinks = $('a[data-method]');
            this.token = $('a[data-token]');
            this.registerEvents();
        },

        registerEvents: function() {
            this.methodLinks.on('click', this.handleMethod);
        },

        handleMethod: function(e) {
            var link = $(this);
            var httpMethod = link.data('method').toUpperCase();
            var form;

            // If the data-method attribute is not PUT or DELETE,
            // then we don't know what to do. Just ignore.
            if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                return;
            }

            // Allow user to optionally provide data-confirm="Are you sure?"
            if ( link.data('confirm') ) {
                if ( ! laravel.verifyConfirm(link) ) {
                    return false;
                }
            }

            form = laravel.createForm(link);
            form.submit();

            e.preventDefault();
        },

        verifyConfirm: function(link) {
            return confirm(link.data('confirm'));
        },


        createForm: function(link) {
            var form =
                    $('<form>', {
                        'method': 'POST',
                        'action': link.attr('href')
                    });

            var token =
                    $('<input>', {
                        'type': 'hidden',
                        'name': '_token',
                        'value': link.data('token')
                    });

            var hiddenInput =
                    $('<input>', {
                        'name': '_method',
                        'type': 'hidden',
                        'value': link.data('method')
                    });

            return form.append(token, hiddenInput)
                    .appendTo('body');
        }
    };

    laravel.initialize();

})();
    </script>

    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete the Task Post?",
                        text: "This post will be un-recoverable!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "/tasks/" + id + "/delete";
                    });
        }
    </script>

@stop


