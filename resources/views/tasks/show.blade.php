@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">


@stop
<?php
define("ONE", 1);
define("TWO", 2);
define("THREE", 3);
define("FOUR", 4);
define("FIVE", 5);
?>


@section('content')

    {{-- Code Written By  Shan --}}

    <div id="app">
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send Message</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/messages" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="ReceiverName">To:
                                </label>
                                @foreach($task->bids as $bid)
                                    @if($bid->status == TWO || $bid->status == FIVE)
                                        <input class="form-control" type="text" value="{{ $bid->bidOwner->name }}" placeholder="" readonly>
                                        <input type="hidden" name="receiver_id" value="{{ $bid->bidOwner->id }}">
                                    @endif
                                @endforeach
                            </div>
                            <input type="hidden" name="sender_id" value="{{ $task->owner->id }}">
                            {{--<div class="form-group">
                                <label for="subject">Subject: </label>
                                <input class="form-control" type="text" id="subject" name="subject" value="{{ $task->topic }}" readonly>
                            </div>--}}
                            <div class="form-group">
                                <label for="Message">Message: </label>
                                <textarea v-model="message" id="message" name="message" class="form-control" rows="6"></textarea>
                            </div>

                            <button v-show="message" type="submit" class="btn btn-primary">Send Message</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>

    {{--End of Code Written By  Shan--}}
    <div class="container">


        <div class="row">

            <div class="col-md-6 ">


                <h1>{{$task->topic}}</h1>
                <h2>{{ $task->price }}</h2>
                <h4><span class="help-block">Created by {{$task->owner->name}}</span></h4>


                {{--Code Written By  Shan--}}

                <div class="pull-right">
                    @foreach($task->bids as $bid)
                        {{--2 is asssigned bid , 5 is completed bid--}}
                        @if($bid->status == TWO || $bid->status == FIVE )
                            <h4><strong>Task Assigned to:</strong>
                            </h4>
                            <div class="dropdown">
                                <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ $bid->bidOwner->name }}
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li><a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope-o" aria-hidden="true"></i> Send Message</a></li>
                                </ul>
                            </div>

                        @endif
                    @endforeach

                </div>

                {{--End of Code Written By  Shan--}}

                @if($task->skills) {{-- If skill required show--}}
                <h4>Skills Required: {{$task->skills}}</h4>
                @endif



                <hr>

                <div class="description">
                    {!!  nl2br($task->details) !!}  {{-- nl2br:To preserve the spaces --}}
                </div>


            </div> {{--End Of Col--}}


            <div class="col-md-6 gallery">

                @if($user && !$user->owns($task))
                    <a href="/tasks/{{$task->id}}/bids/create" class="btn btn-lg btn-success">Make A Bid</a>
                    <hr>
                @endif

                @foreach($task->photos->chunk(3) as $set)
                    <div class="row">
                        @foreach($set as $photo)

                            <div class="col-md-4 gallery_image">

                                @if($user && $user->owns($task))

                                    <form method="post" action="/taskphotos/{{$photo->id}}">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class=" btn btn-sm btn-danger"><i class="fa fa-trash-o"
                                                                                                aria-hidden="true"></i>
                                        </button>

                                    </form>
                                @endif


                                <a href="/{{$photo->path}}" data-lity>

                                    <img src="/{{$photo->thumbnail_path}}" alt="">

                                </a>
                            </div>

                        @endforeach

                    </div>
                @endforeach
                    @if($task->video_path)
                        <iframe width="420" height="315" src="{{$task->video_path}}"
                                frameborder="0" allowfullscreen></iframe>
                    @endif


                {{--@foreach($task->photos as $photo)--}}

                {{--<img src="/{{$photo->thumbnail_path}}" alt="">--}}

                {{--@endforeach--}}


                @if($user && $user->owns($task))
                    {{--If We have a user and the user owns the tasks then add Image--}}

                    <hr>
                    <form id="addTaskPhotoForm"
                          action="{{route('store_task_photo',[$task->id])}}"
                          method="post"
                          class="dropzone">


                        {{csrf_field()}}
                    </form>
                @endif


            </div> {{--End Of Col Gallery--}}

        </div>{{--End Of Row--}}
        <br>
    </div>
@stop



@section('scripts.footer')

    <script src="/js/dropzone.js"></script>
    <script src="/js/lity.js"></script>

    <script>

        Dropzone.options.addTaskPhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the task',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'

        };


    </script>


<script>
    new Vue({
        el: '#app',
        data: {
            message: ''
        }
    })
</script>
@stop