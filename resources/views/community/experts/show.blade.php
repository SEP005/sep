@extends('layouts.app')
@section('content')
    <style>
        .ggTitle {
            margin-top: -20px;
            padding-top:40px;
            padding-bottom: 40px;
            color: #FFF;
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,2989d8+50,7db9e8+100 */
            background: #1e5799; /* Old browsers */
            background: -moz-linear-gradient(left, #1e5799 0%, #2989d8 50%, #7db9e8 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(left, #1e5799 0%,#2989d8 50%,#7db9e8 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to right, #1e5799 0%,#2989d8 50%,#7db9e8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=1 ); /* IE6-9 */
        }

        .color-red {
            color: #ff0000;
        }
    </style>

    <div class="ggTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center animated fadeInDown">
                    <h1><strong>Community Experts</strong></h1>
                    <h3>Ask questions and learn from community experts</h3>
                </div>
            </div>
        </div>
    </div>
    <div id="ansapp" class="container">
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Report</h4>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="">
                            <label for="reason">Please tell us the reason</label>
                            <textarea name="reason" id="reason" rows="4" class="form-control" v-model="flag.reason"></textarea>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button v-if="flag.reason" @click="storeReport()" type="button" class="btn btn-primary" data-dismiss="modal">Report</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 40px;"></div>
        <div class="row">
            <div class="col-md-8">
                <a href="/community/experts" class="btn btn-default btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to all Questions</a>
                    <h3><strong>{{ $question->question }}</strong></h3>
                    <p class="text-muted">asked by {{ $question->owner->name }} in {{ $question->created_at->diffForHumans() }}</p>
                    <p>{{ $question->information }}</p>
                    {{--<p class="text-muted"><i class="fa fa-heart-o" aria-hidden="true"></i> {{ $question->likes }} likes</p>--}}

                <hr>
                <p><strong>@{{ answerList.length }} Answers</strong> <button form="ans" class="btn btn-default pull-right">Answer Question</button> </p>
                    <form id="ans" method="post" action="/community/experts/question/answer">
                        {{ csrf_field() }}
                        <input type="hidden" name="question_id" value="{{ $question->id }}">
                        <input type="hidden" name="user_id" value="{{ $question->user_id }}">
                        <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                            <label for="answer">Answer this question</label>
                            <textarea name="answer" id="answer" rows="5" class="form-control" value="{{ old('answer') }}"></textarea>
                            @if ($errors->has('answer'))
                                <span class="help-block">
                            <strong>{{ $errors->first('answer') }}</strong>
                        </span>
                            @endif
                        </div>
                    </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div v-for="d in answerList">
                    <span @click="toggleLike(d.id,{{ Auth::user()->id }})" class="text-muted pull-right" v-bind:class="{ 'color-red': checkIfLiked(d.id,{{ Auth::user()->id }})}" style="cursor: pointer;">
                    <i class="fa fa-heart-o" aria-hidden="true"></i> @{{ getLikes(d.id) }} likes
                    </span>
                    <p><a href="/user/@{{ d.name }}"><strong>@{{ d.name }}</strong></a> in @{{ getDate(d.created_at) }}</p>

                    <p>@{{ d.answer }}</p>

                    <span v-if="checkIfReport(d.id,{{ Auth::user()->id }})" class="color-red">
                        @{{ getFlags(d.id) }} <i class="fa fa-flag" aria-hidden="true"></i>
                    </span>

                    <span
                        v-if="!checkIfReport(d.id,{{ Auth::user()->id }})"
                        @click="report(d.id,{{ Auth::user()->id }})"
                        class="text-muted"
                        v-bind:class="{ 'color-red': checkIfReport(d.id,{{ Auth::user()->id }})}"
                        style="cursor: pointer;"
                        data-toggle="modal" data-target=".bs-example-modal-sm"
                    >
                    @{{ getFlags(d.id) }} <i class="fa fa-flag" aria-hidden="true"></i>
                    </span>
                    <hr>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts.footer')
    <script>
        Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
        var app = new Vue({
            el: '#ansapp',
            data: {
                answerList: [],
                answerLikeList: [],
                answerReportList: [],
                flag: {
                    answer_id: '',
                    user_id: '',
                    reason: '',
                },
            },

            created: function() {
                this.fetchAnswers();
                this.fetchAnswerLikes();
                this.fetchAnswerReports();
            },

            methods: {
                fetchAnswers: function() {
                    var url= window.location.href;
                    var qid = url.charAt( url.length - 1 );
                    console.log(qid);
                    this.$http.get('/api/answers/question/'+qid).then(function (response) {
                        this.answerList = response.data;
                    });
                },

                fetchAnswerLikes: function() {
                    this.$http.get('/api/answer-likes').then(function (response) {
                        this.answerLikeList = response.data;
                    });
                },

                fetchAnswerReports: function() {
                    this.$http.get('/api/answer-reports').then(function (response) {
                        this.answerReportList = response.data;
                    });
                },

                getDate(date){
                    return moment(date).fromNow();
                },

                toggleLike(answerid, userid) {
                    var like = {};
                    like.answer_id = answerid;
                    like.user_id = userid;

                    this.$http.post('/community/experts/answer/toggle/like', like).then(function (response) {
                                this.fetchAnswerLikes();
                            },
                            function (response) {
                                // error callback
                            });

                },

                getLikes(id) {
                    var count = 0;
                    for(var i=0; i<this.answerLikeList.length; i++) {
                        if(this.answerLikeList[i].answer_id == id) {
                            count++;
                        }
                    }
                    return count;
                },

                checkIfLiked(id,uid) {
                    for(var i=0; i<this.answerLikeList.length; i++) {
                        if(this.answerLikeList[i].answer_id == id && this.answerLikeList[i].user_id == uid) {
                            return true;
                        }
                    }
                },

                report(answerid, userid) {
                    this.flag.answer_id = answerid;
                    this.flag.user_id = userid;
                },

                storeReport() {

                    this.$http.post('/community/experts/answer/report', this.flag).then(function (response) {
                                this.fetchAnswerReports();
                                this.flag.reason = '';
                            },
                            function (response) {
                                // error callback
                            });
                },

                getFlags(id) {
                    var count = 0;
                    for(var i=0; i<this.answerReportList.length; i++) {
                        if(this.answerReportList[i].answer_id == id) {
                            count++;
                        }
                    }
                    return count;
                },

                checkIfReport(id,uid) {
                    for(var i=0; i<this.answerReportList.length; i++) {
                        if(this.answerReportList[i].answer_id == id && this.answerReportList[i].user_id == uid) {
                            return true;
                        }
                    }
                },
            }
        })
    </script>
@stop