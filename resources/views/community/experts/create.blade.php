@extends('layouts.app')
@section('content')
    <style>
        .ggTitle {
            margin-top: -20px;
            padding-top:40px;
            padding-bottom: 40px;
            color: #FFF;
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,2989d8+50,7db9e8+100 */
            background: #1e5799; /* Old browsers */
            background: -moz-linear-gradient(left, #1e5799 0%, #2989d8 50%, #7db9e8 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(left, #1e5799 0%,#2989d8 50%,#7db9e8 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to right, #1e5799 0%,#2989d8 50%,#7db9e8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=1 ); /* IE6-9 */
        }
    </style>
    <div class="ggTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center animated fadeInDown">
                    <h1><strong>Community Experts</strong></h1>
                    <h3>Ask questions and learn from community experts</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" style="margin-top:20px;">
            <div class="col-md-8">
                <a href="/community/experts" class="btn btn-default btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to all Questions</a>
                <h1 style="margin-bottom: 20px;"><strong>Ask experts a question</strong></h1>
                <form method="post" action="/community/experts">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                        <label for="question"><strong>Your Question</strong></label>
                        <input type="text"
                               class="form-control"
                               id="question"
                               name="question"
                               placeholder="Keep it short and sweet"
                               value="{{ old('question') }}"
                        >
                        @if ($errors->has('question'))
                            <span class="help-block">
                            <strong>{{ $errors->first('question') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="information"><strong>More Information (if needed)</strong></label>
                        <textarea class="form-control" name="information" id="information" rows="5"></textarea>
                    </div>
                    <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                        <label for="questionCategory_id">Category</label>
                        <select name="questionCategory_id" id="questionCategory_id" class="form-control">
                            <option selected disabled>Choose a category</option>
                            @foreach($cats as $d)
                                <option value="{{ $d->id }}">{{ $d->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('questionCategory_id'))
                            <span class="help-block">
                            <strong>{{ $errors->first('questionCategory_id') }}</strong>
                        </span>
                        @endif
                    </div>
                    <button class="btn btn-primary pull-right">Ask Question</button>
                </form>
            </div>
        </div>
        <div class="row" style="margin-top: 40px;"></div>
    </div>


@stop