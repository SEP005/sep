@extends('layouts.app')
@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>My Community Questions</h2>
                <div class="box">
                    <table id="dataTable" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Question</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        @foreach($data as $d)
                            <tr style="@if($d->active==0) background-color: #ffaaaa; @endif">
                                <td>{{ $d->question }}</td>
                                <td>
                                    @if($d->active == 1)
                                        Active
                                    @else
                                        Disabled
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop
@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        } );
    </script>
@stop