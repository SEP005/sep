@extends('layouts.app')
@section('content')
    <style>
        .ggTitle {
            margin-top: -20px;
            padding-top:40px;
            padding-bottom: 40px;
            color: #FFF;
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1e5799+0,2989d8+50,7db9e8+100 */
            background: #1e5799; /* Old browsers */
            background: -moz-linear-gradient(left, #1e5799 0%, #2989d8 50%, #7db9e8 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(left, #1e5799 0%,#2989d8 50%,#7db9e8 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to right, #1e5799 0%,#2989d8 50%,#7db9e8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=1 ); /* IE6-9 */
        }
        
        .color-red {
            color: #ff0000;
        }
    </style>
    <div class="ggTitle">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center animated fadeInDown">
                    <h1><strong>Community Experts</strong></h1>
                    <h3>Ask questions and learn from community experts</h3>
                </div>
            </div>
        </div>
    </div>
    <div id="q" class="container">
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Report</h4>
                    </div>
                    <div class="modal-body">
                        <form @submit.prevent="">
                            <label for="reason">Please tell us the reason</label>
                            <textarea name="reason" id="reason" rows="4" class="form-control" v-model="flag.reason"></textarea>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button v-if="flag.reason" @click="storeReport()" type="button" class="btn btn-primary" data-dismiss="modal">Report</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:20px;">
            <div class="col-md-12">
                <h2 style="margin-bottom: 20px;">
                    <strong>Popular Questions
                        <span v-if="categoryName !== null">: @{{ categoryName }}</span>
                        <span v-if="questionBy !== null">asked by: @{{ questionBy }}</span>
                    </strong>
                    <a class="btn btn-default pull-right" href="/community/experts/ask">Ask a Question</a>
                </h2>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <ul class="list-group" style="cursor: pointer;">
                                <li class="list-group-item" @click="showAllQuestions()">All</li>
                                <li class="list-group-item" v-for="d in categoryList" @click="showCategory(d.id)">@{{ d.name }}</li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div v-for="d in questionList | orderBy 'created_at' -1">
                                <h3><strong>@{{ d.question }}</strong></h3>
                                <span @click="toggleLike(d.id,{{ Auth::user()->id }})" class="text-muted pull-right" v-bind:class="{ 'color-red': checkIfLiked(d.id,{{ Auth::user()->id }})}" style="cursor: pointer;">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i> @{{ getLikes(d.id) }} likes
                                    {{--<i class="fa fa-comment-o" aria-hidden="true"></i> answers--}}
                                </span>
                                <p class="text-muted">
                                    asked by
                                    <a href="#" @click="showQuestionsByUser(d.user_id)">
                                    <strong>@{{ getUserName(d.user_id) }}</strong>
                                    </a>
                                    @{{ getDate(d.created_at) }} in
                                    <a href="#" @click="showCategory(d.questionCategory_id)">
                                    <strong>@{{ getCategoryName(d.questionCategory_id) }}</strong>
                                    </a>

                                </p>

                                <p>@{{ d.information }}</p>
                                <p><strong><a href="/community/experts/question/@{{ d.id }}">Read more</a></strong></p>

                                <span v-if="checkIfReport(d.id,{{ Auth::user()->id }})" class="color-red">
                                    @{{ getFlags(d.id) }} <i class="fa fa-flag" aria-hidden="true"></i>
                                </span>

                                <span
                                v-if="!checkIfReport(d.id,{{ Auth::user()->id }})"
                                @click="report(d.id,{{ Auth::user()->id }})"
                                class="text-muted"
                                v-bind:class="{ 'color-red': checkIfReport(d.id,{{ Auth::user()->id }})}"
                                style="cursor: pointer;"
                                data-toggle="modal" data-target=".bs-example-modal-sm"
                                >
                                @{{ getFlags(d.id) }} <i class="fa fa-flag" aria-hidden="true"></i>
                                </span>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('scripts.footer')
    <script>
        Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
        var app = new Vue({
            el: '#q',


            data: {
                questionList: [],
                categoryList: [],
                userList: [],
                questionLikeList: [],
                categoryName: null,
                questionBy: null,
                likeState: null,

                questionReportList: [],
                flag: {
                    question_id: '',
                    user_id: '',
                    reason: '',
                },
            },

            created: function() {
                this.fetchQuestions();
                this.fetchCategories();
                this.fetchUsers();
                this.fetchQuestionLikes();
                this.fetchQuestionReports();
            },

            methods: {
                fetchQuestions: function() {
                    this.$http.get('/api/questions').then(function (response) {
                        this.questionList = response.data;
                    });
                },

                fetchCategories: function() {
                    this.$http.get('/api/question-categories').then(function (response) {
                        this.categoryList = response.data;
                    });
                },

                fetchUsers: function() {
                    this.$http.get('/api/users').then(function (response) {
                        this.userList = response.data;
                    });
                },

                fetchQuestionLikes: function() {
                    this.$http.get('/api/question-likes').then(function (response) {
                        this.questionLikeList = response.data;
                    });
                },

                fetchQuestionReports: function() {
                    this.$http.get('/api/question-reports').then(function (response) {
                        this.questionReportList = response.data;
                    });
                },

                getCategoryName(id) {
                    for(var i=0; i<this.categoryList.length; i++) {
                        if(this.categoryList[i].id == id) {
                            return this.categoryList[i].name;
                        }
                    }
                },

                getUserName(id) {
                    for(var i=0; i<this.userList.length; i++) {
                        if(this.userList[i].id == id) {
                            return this.userList[i].name;
                        }
                    }
                },

                formatDate(date) {
                    var dateObj = new Date(date);
                    var year = dateObj.getFullYear();
                    var month = dateObj.getMonth() + 1;
                    var day = dateObj.getDate();

                    return day + '-' + month + '-' + year;
                },

                getDate(date){
                    return moment(date).fromNow();
                },

                showCategory(id) {
                    this.$http.get('/api/questions/' +id).then(function (response) {
                        this.questionList = response.data;
                    });

                    this.categoryName = this.getCategoryName(id);
                    this.questionBy = null;
                },

                showAllQuestions() {
                    this.$http.get('/api/questions/').then(function (response) {
                        this.questionList = response.data;
                    });

                    this.categoryName = null;
                    this.questionBy = null;
                },

                showQuestionsByUser(id) {
                    this.$http.get('/api/questions/user/' +id).then(function (response) {
                        this.questionList = response.data;
                    });

                    this.questionBy = this.getUserName(id);
                    this.categoryName = null;
                },

                getLikes(id) {
                    var count = 0;
                    for(var i=0; i<this.questionLikeList.length; i++) {
                        if(this.questionLikeList[i].question_id == id) {
                            count++;
                        }
                    }
                    return count;
                },

                checkIfLiked(id,uid) {
                    for(var i=0; i<this.questionLikeList.length; i++) {
                        if(this.questionLikeList[i].question_id == id && this.questionLikeList[i].user_id == uid) {
                            return true;
                        }
                    }
                },

                toggleLike(questionid, userid) {
                    var like = {};
                    like.question_id = questionid;
                    like.user_id = userid;

                    this.$http.post('experts/question/toggle/like', like).then(function (response) {
                        this.fetchQuestionLikes();
                    },
                    function (response) {
                        // error callback
                    });

                },


                report(questionid, userid) {
                    this.flag.question_id = questionid;
                    this.flag.user_id = userid;
                },

                storeReport() {

                    this.$http.post('/community/experts/question/report', this.flag).then(function (response) {
                                this.fetchQuestionReports();
                                this.flag.reason = '';
                            },
                            function (response) {
                                // error callback
                            });
                },

                getFlags(id) {
                    var count = 0;
                    for(var i=0; i<this.questionReportList.length; i++) {
                        if(this.questionReportList[i].question_id == id) {
                            count++;
                        }
                    }
                    return count;
                },

                checkIfReport(id,uid) {
                    for(var i=0; i<this.questionReportList.length; i++) {
                        if(this.questionReportList[i].question_id == id && this.questionReportList[i].user_id == uid) {
                            return true;
                        }
                    }
                },

            }
        })
    </script>
@stop