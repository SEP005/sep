@if(session()->has('flash_message'))
    <div class="Alert Alert--{{ucwords(session()->get('flash_message_level'))}}">
        {{ session()->get('flash_message') }}

    </div>
@endif