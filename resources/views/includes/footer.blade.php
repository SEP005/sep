<div id="ggFt">
<div style="background-color: #1f2836; color: #FFF; margin-top: 60px; padding: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="col-md-6">
                    <p>@{{ stats.users }}</p>
                    <p style="font-size: 12px;">REGISTERED USERS</p>
                </div>
                <div class="col-md-6">
                    <p>@{{ stats.tasks }}</p>
                    <p style="font-size: 12px;">TOTAL TASKS POSTED</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="background-color: #161e2c; color: #FFF; padding-top: 20px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>Copyright 2016 - GoGetter</p>
            </div>
        </div>
    </div>
</div>
</div>
@section('scripts.footer')
<script>
    var app = new Vue({
        el: '#ggFt',
        data: {
            stats:{}
        },

        created: function() {
            this.fetchData();
        },

        methods: {
            fetchData: function() {
                this.$http.get('/api/footerstats').then(function (response) {
                    this.stats = response.data;
                });


            },
        }
    })
</script>
    @stop