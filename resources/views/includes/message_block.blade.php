@if(count($errors) > 0) {{--Checks if validation outputs errors and show the errors--}}
    <div class="row alert alert-danger">
        <div class="col-md-4 col-md-offset-4 error">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if(Session::has('message')) {{--Checks if message is available and show message--}}
    <div class="row">
        <div class="col-md-4 col-md-offset-4 success">
            {{Session::get('message')}}
        </div>
    </div>
@endif


{{--<div class="alert alert-success" role="alert">...</div>--}}
{{--<div class="alert alert-info" role="alert">...</div>--}}
{{--<div class="alert alert-warning" role="alert">...</div>--}}
{{--<div class="alert alert-danger" role="alert">...</div>--}}