
<div class="media">
    <a class="pull-left" href="{{ route('profile.index', ['name'=> $user -> name]) }}">
        @if (Storage::disk('local')->has($user->first_name . '-' . $user->id . '.jpg'))
            <img class="img-circle" src="{{ route('account.image', ['filename' => $user->first_name . '-' . $user->id . '.jpg']) }}"
                 alt="{{$user->getName()}}" width="50" height="50" >
        @else
            <img class="img-circle" alt="{{$user->getName()}}" src="{{$user->getAvatarUrl()}}" >
        @endif
    </a>
    <div class="media-body">
       <h4 class="media-heading"><a href="{{ route('profile.index', ['name'=> $user -> name]) }}">{{ $user->getName() }}</a></h4>
    </div>
</div>