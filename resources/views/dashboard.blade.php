@extends('layouts.app')

@section('css.header')
    @yield('dash.css')

@stop

@section('content')
    @yield('dash.content')

@stop

@section('scripts.footer')
    @yield('dash.scripts')

@stop