@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><strong>Browse Categories</strong></h2>
                <hr/>

                    @foreach($categories as $category)
                        <div class="col-md-12" style="margin-bottom:40px;">
                            <div class="box">
                                <h3><a style="color:#333;" href="category/{{ $category->id }}">{{ $category->name }}</a></h3>
                                <hr/>
                                @foreach($category->subCategory as $firstNestedSub)
                                   <span style="margin-right:15px;"><a href="category/{{ $firstNestedSub->id }}">{{ $firstNestedSub->name }}</a></span>
                                @endforeach
                            </div>
                        </div>
                    @endforeach

            </div>
        </div>
    </div>
@stop