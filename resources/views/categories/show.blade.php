@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><strong>Tasks for Category: </strong></h2>
                <hr/>
                <table id="dataTable" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Task Id</th>

                        <th class="tableTaskTopic">Topic</th>
                        <th>Details</th>
                        {{--<th class="tableTaskSkills">Skills Required</th>--}}

                        {{-- <th>Category</th> --}}
                        <th class="tableTaskPrice">Price</th>

                        <th></th>
                        <th></th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td><a href="{{$task->path()}}">{{$task->id}} </a></td>

                            <td class="tableTaskTopic"><a href="{{$task->path()}}">{{$task->topic}} </a></td>
                            <td><p class="tableTaskDetails"> {{$task->details}}<p></td>
                            <td class="tableTaskSkills"> {{$task->skills}}</td>

                            {{--<td>{{$task->category_id}}</td>--}}
                            <td class="tableTaskPrice">{{$task->price}}</td>


                            <td>
                                <a href="/tasks/{{$task->id}}/bids/create" class="btn btn-sm btn-success">Bid
                                </a>
                            </td>



                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": true},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},
                    {"sortable": true},
                    {"sortable": false},

                ],


            });

        });


    </script>
   @stop
