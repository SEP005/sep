<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>GO GETTER</title>
    <script src="/js/jquery.min.js" crossorigin="anonymous"></script>


<!-- Fonts -->
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"  crossorigin="anonymous">--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"--}}
    {{--crossorigin="anonymous">--}}
    <link rel="stylesheet" href="/css/font-awesome.min.css" crossorigin="anonymous">

    {{--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">--}}
    <link rel="stylesheet" href="/css/googlefont-lato.css" crossorigin="anonymous">

    <!-- Styles -->

    <link rel="stylesheet" href="/css/bootstrap.min.css" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" type="text/css" href="/css/sweetalert.css">
    {{-- to ammed last commit--}}

    {{-- To Change NavBar Color--}}
    <link rel="stylesheet" type="text/css" href="/css/navBarApp.css">

    <link rel="stylesheet" type="text/css" href="/css/animate.css">


    @yield('css.header')

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }

        /*Code Written by bala */

        .rating {
            float: left;
        }

        /* :not(:checked) is a filter, so that browsers that don’t support :checked don’t
           follow these rules. Every browser that supports :checked also supports :not(), so
           it doesn’t make the test unnecessarily selective */
        .rating:not(:checked) > input {
            position: absolute;
            top: -9999px;
            clip: rect(0, 0, 0, 0);
        }

        .rating:not(:checked) > label {
            float: right;
            width: 1em;
            padding: 0 .1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 200%;
            line-height: 1.2;
            color: #ddd;
            text-shadow: 1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0, 0, 0, .5);
        }

        .rating:not(:checked) > label:before {
            content: '★ ';
        }

        .rating > input:checked ~ label {
            color: #f70;
            text-shadow: 1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0, 0, 0, .5);
        }

        .rating:not(:checked) > label:hover,
        .rating:not(:checked) > label:hover ~ label {
            color: gold;
            text-shadow: 1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0, 0, 0, .5);
        }

        .rating > input:checked + label:hover,
        .rating > input:checked + label:hover ~ label,
        .rating > input:checked ~ label:hover,
        .rating > input:checked ~ label:hover ~ label,
        .rating > label:hover ~ input:checked ~ label {
            color: #ea0;
            text-shadow: 1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0, 0, 0, .5);
        }

        .rating > label:active {
            position: relative;
            top: 2px;
            left: 2px;
        }

        input[type="file"] {
            display: none;
        }

        .custom-file-upload {
            border: 1px solid #ccc;
            display: inline-block;
            padding: 6px 12px;
            cursor: pointer;
        }

        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Float the list items side by side */
        ul.tab li {
            float: left;
        }

        /* Style the links inside the list items */
        ul.tab li a {
            display: inline-block;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of links on hover */
        ul.tab li a:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        ul.tab li a:focus, .active {
            background-color: #ccc;
        }

        /* Style the tab content */

        .tabcontent {
            -webkit-animation: fadeEffect 1s;
            animation: fadeEffect 3s; /* Fading effect takes 1 second */
        }

        @-webkit-keyframes fadeEffect {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        @keyframes fadeEffect {
            from {
                opacity: 0;
            }
            to {
                opacity: 1;
            }
        }

        /* End of Code Written by bala */
        /**/
    </style>
</head>
<body id="app-layout">

<style>
    /*TODO:apply this id and change colors for nav bar
    TODO id="navAnchor"*/
    #navAnchor {
        color: #005483;
    }

    .navbar-brand {

    }
</style>

<nav class="navbar navbar-ggNavBAr navbar-static-top">
    {{--<nav class="navbar navbar-light navbar-static-top" style="background-color: #505050;">--}}
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            {{--If not logged In, clicking gogetter image would reload startpage--}}
            @if (Auth::guest())
                <a class="navbar-brand" href="{{ url('/') }}">
                    <strong style="background: #FFF; padding: 9px; border-radius: 4px; color: #005483;">
                        Go-Getter
                    </strong>
                </a>
            @else
                {{--If Logged in click gogetter image will go to home page--}}
                <a class="navbar-brand" href="{{ url('/home') }}">
                    <strong style="background: #FFF; padding: 9px; border-radius: 4px; color: #005483;">
                        Go-Getter
                    </strong>
                </a>
        @endif
        <!-- Branding End -->
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">

            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @if (Auth::guest())

                @else
                    <li><a href="{{ url('/home') }}">Home</a></li>

                    <li><a href="{{ url('/taskprogress') }}">Dashboard</a></li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Community
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/community/experts') }}">Ask an Expert</a></li>
                            <li><a href="{{ url('/community/experts/my') }}">My Questions</a></li>
                            <li><a href="{{ url('/community/experts/myAnswers') }}">My Answers</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Requester
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/tasks/create') }}">Post Task</a></li>{{--request--}}
                            <li><a href="{{ url('/tasks') }}">View posted tasks</a></li>
                            <li><a href="{{ url('/taskBids') }}">View Bids</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Worker
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/workerTasks') }}">Browse tasks</a></li>

                            {{--<li><a href="{{ url('/categories') }}">Browse Categories</a></li>--}}
                            {{--<li><a href="{{ url('/tasks/create') }}">Browse By Category</a></li>--}}{{--request--}}

                            <li><a href="{{ url('/workerBids') }}">Worker Bids</a></li>

                            <li><a href="{{ url('/showcase') }}">Explore Showcases</a></li>

                        </ul>
                    </li>


                    <form action="{{route('search.results')}}" role="search" class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" name="query" class="form-control"
                                   placeholder="Find people" size="25px"/>
                        </div>
                        <button type="submit" class="btn btn-Primary"
                                style="background-color: Transparent;
                                        background-repeat:no-repeat;
                                        border: none;
                                        cursor:pointer;
                                        overflow: hidden;
                                        outline:none;">
                            <i class="fa fa-search fa" aria-hidden="true" style="color:white;
                                                                                text-shadow: 1px 1px 1px #ccc;
                                                                                font-size: 1.5em;"></i>
                        </button>
                    </form>

                @endif
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">

                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                <!-- friend request notification
				 -->
                    <li class="dropdown">
                        <?php
                        $user = Auth::user();
                        $friends = Auth::user()->friends();
                        $requests = Auth::user()->friendRequests();
                        ?>

                        {{-- Code written by bala--}}

                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false" style="background-color: Transparent;
                                        background-repeat:no-repeat;
                                        border: none;
                                        cursor:pointer;
                                        overflow: hidden;
                                        outline:none;">
                            <i class="fa fa-users fa-2x" aria-hidden="true"
                               style="color:white;
                                       text-shadow: 1px 1px 1px #ccc;
                                       font-size: 2em;"></i><b><font color="#dc143c">{{$requests->count()}}</font></b>
                                       <span class="caret">
                                        </span>
                        </a>
                        <ul class="dropdown-menu "
                            style=" position: absolute;
                                    background-color: #f9f9f9;
                                    min-width: 200px;
                                    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);"
                            role="menu">
                            <li>
                                @if(!$requests->count())
                                    <p class="text-center"><b><font color="#dc143c">no new requests.</font></b></p>
                                @else
                                    @foreach($requests as $user)
                                        @if( Auth::user()->hasFriendRequestsReceived($user))
                                            <div class="media">
                                                <a class="pull-left"
                                                   href="{{ route('profile.index', ['name'=> $user -> name]) }}">
                                                    @if (Storage::disk('local')->has($user->first_name . '-' . $user->id . '.jpg'))
                                                        <img class="img-circle"
                                                             src="{{ route('account.image', ['filename' => $user->first_name . '-' . $user->id . '.jpg']) }}"
                                                             alt="{{$user->getName()}}" width="50" height="50">
                                                    @else
                                                        <img class="img-circle" alt="{{$user->getName()}}"
                                                             src="{{$user->getAvatarUrl()}}">
                                                    @endif
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><a
                                                                href="{{ route('profile.index', ['name'=> $user -> name]) }}">{{ $user->getName() }}</a>
                                                    </h4>
                                                </div>
                                                <a href="{{route('friend.accept',['name'=>$user->name])}}"
                                                   class="btn-btn-primary"
                                                   style="background-color: rgba(50, 105, 255, 0.62);border-radius: 25px;color: white;padding: 2px 5px;text-align: center;text-decoration: none;display: inline-block;"
                                                   role="button"><p> Accept</p>
                                                </a>
                                                <a href="{{route('friend.delete',['name'=>$user->name])}}"
                                                   class="btn-btn-primary"
                                                   style="background-color: rgba(238, 42, 9, 0.56);border-radius: 25px;color: white;padding: 2px 5px;text-align: center;text-decoration: none;display: inline-block;"
                                                   role="button"><p>Delete</p></a>

                                            </div>
                                        @endif
                                    @endforeach
                                @endif

                            </li>
                        </ul>
                    </li>
                    {{--<li class="dropdown">--}}
                        {{--<a href="/home" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                           {{--aria-expanded="false"--}}
                           {{--style="background-color: Transparent;--}}
                                        {{--background-repeat:no-repeat;--}}
                                        {{--border: none;--}}
                                        {{--cursor:pointer;--}}
                                        {{--overflow: hidden;--}}
                                        {{--outline:none;">--}}
                            {{--<i class="fa fa-globe fa-2x" aria-hidden="true"--}}
                               {{--style="color:white;--}}
                                       {{--text-shadow: 1px 1px 1px #ccc;--}}
                                       {{--font-size: 2em;"></i><b><font color="#dc143c">{{$requests->count()}}</font></b>--}}
                            {{--<span class="caret"></span>--}}
                        {{--</a>--}}

                        {{--<ul class="dropdown-menu" role="menu">--}}
                            {{--<li>test 1</li>--}}
                            {{--<li>test 2</li>--}}

                        {{--</ul>--}}


                    {{--</li>--}}


                    {{-- End of Code written by bala--}}

                <!-- user profile-->
                    <li class="dropdown">
                        <a href="/home" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{route('profile.index',['name'=> Auth::user()->name])}}"><i
                                            class="fa fa-user"></i>{{ Auth::user()->name }}</a></li>
                            <li><a href="{{route('profile.edit')}}"><i class="fa fa-bars"> </i>Update Profile</a></li>
                            <li><a href="{{ url('/messages') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    Messages</a></li>
                            <li><a href="{{route('friend.index')}}"><i class="fa fa-user-plus"></i>Connections</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                        </ul>


                    </li>
                @endif
            </ul>
        </div>
    </div>

</nav>

{{-- @include('includes.message_block') --}}


@yield('content')

<!-- JavaScripts -->

<script src="/js/jquery.min.js" crossorigin="anonymous"></script>

<script src="/js/bootstrap.min.js" crossorigin="anonymous"></script>

{{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
<script src="/js/sweetalert-dev.js"></script>
<script src="/js/vue.min.js"></script>
<script src="/js/vue-resource.js"></script>
<script src="/js/moment.js"></script>
{{--<script src="/js/turbolinks.js"></script>--}}

{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>--}}

@yield('scripts.footer')



@include('flash')

</body>
</html>
