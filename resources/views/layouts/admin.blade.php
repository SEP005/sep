<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>GG Admin</title>
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">

    <!--Fonts-->
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <!--Styles-->
    <link href="/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/css/sweetalert.css" rel="stylesheet">

    <!--User Styles-->
    <link href="/css/admin.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin">GG Admin</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="/admin" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-expanded="false">
                        Welcome,
                        @if(Auth::user())
                            {{ Auth::user()->name }} {{ Auth::user()->lastname }}<span class="caret"></span>
                        @endif
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navigation -->

<!-- Left Side -->

<div class="col-md-2 left">
    <a href="/admin"><i class="fa fa-tachometer" aria-hidden="true" ></i> Dashboard</a>
    <a href="/admin/categories"><i class="fa fa-sitemap" aria-hidden="true"></i> Categories</a>
    <a href="#communityExperts" data-toggle="collapse"><i class="fa fa-users" aria-hidden="true"></i> Community Experts <span class="caret"></span></a>
    <div id="communityExperts" class="collapse submenu">
        <a href="/admin/community/experts/answers">Answers</a>
        <a href="/admin/community/experts/categories">Categories</a>
        <a href="/admin/community/experts/questions">Questions</a>
    </div>
    <a href="#users" data-toggle="collapse"><i class="fa fa-users" aria-hidden="true"></i> Users <span class="caret"></span></a>
    <div id="users" class="collapse submenu">
        <a href="/admin/users">All Users</a>
        <a href="/admin/user-new">Add New User</a>
        <a href="/admin/roles">User Roles</a>
    </div>
    <a href="/admin/tasks"><i class="fa fa-tasks" aria-hidden="true"></i> Requester Tasks</a>
    <a href="/admin/bids"><i class="fa fa-money" aria-hidden="true"></i> Worker Bids</a>
</div>
<div class="col-md-10 content">
    @yield('content')
</div>

<!-- Left Side -->

<!-- Javascript files -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    } );

    $(document).ready(function() {
        $('#dataTableCat').DataTable( {
            "ordering": false
        } );
    } );
</script>
@include('flash')
</body>
</html>
