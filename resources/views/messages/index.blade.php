@extends('layouts.app')

@section('content')

    <div class="container" id="app">
        <div class="row">
            <div class="col-md-4">
                <h3>Recent Conversations</h3>
                <hr>
                <input type="hidden" v-model="userid" value="{{ Auth::user()->id }}">
                <ul class="list-group">
                    <li class="list-group-item" style="cursor: pointer" v-for="d in receivers" @click="viewChat(d)">@{{ getUserName(d) }}</li>
                </ul>
            </div>
            <div class="col-md-8 animated" v-show="showChatArea" transition="fade">
                <h3>@{{ receiverName }}</h3>
                <hr>
                <ul class="list-group" style="height: 200px;  overflow-y: scroll;">
                    <li
                            class="list-group-item list-group-item-info"
                            v-show="messages.message"
                    ><strong>@{{ getUserName(userid) }}:</strong> @{{ messages.message }}</li>
                    <li class="list-group-item" v-for="d in activeMessages" v-bind:class="{'list-group-item-success':isReceiver({{ Auth::user()->id }},d.sender_id)}">
                        <strong>@{{ getUserName(d.sender_id) }}:</strong> @{{ d.message }} <span style="font-size:10px;" class="text-muted pull-right">@{{ getDate(d.created_at) }}</span>
                    </li>

                </ul>
                <form @submit.prevent="storeMessage()">
                    <div class="form-group">
                        <label><strong>Send Message</strong></label>
                        <textarea
                                class="form-control"
                                rows="5"
                                placeholder="Write a reply..."
                                v-model="messages.message"
                        ></textarea>
                    </div>
                    <button v-if="messages.message" class="btn btn-primary btn-sm">Send</button>
                </form>
            </div>
        </div>
    </div>
@stop
@section('scripts.footer')
    <script>
        Vue.http.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');
        Vue.transition('fade', {
            enterClass: 'fadeIn',
            leaveClass: 'fadeOut'
        });

        var app = new Vue({
            el: '#app',
            data: {

                showChatArea: false,

                userid: '',
                receiverName: '',
                receiverId: '',

                userList: [],
                messageList: [],
                receivers: [],
                activeMessages: [],

                messages: {
                    message: '',
                }
            },

            ready: function() {

            },

            created: function() {
                this.fetchUserList();
                this.fetchMessageList();

            },

            methods: {

                fetchUserList: function() {
                    this.$http.get('api/users').then(function (response) {
                        this.userList = response.data;
                    });


                },

                fetchMessageList: function() {
                    this.$http.get('api/messages').then(function (response) {
                        this.messageList = response.data;
                        this.getReceivers();
                    });


                },

                getDate(date){
                    return moment(date).fromNow();
                },

                getReceivers: function () {

                    var r = [];

                    for(var i = 0; i < this.messageList.length; i++){
                        if(this.messageList[i].sender_id == this.userid){
                            r.push(this.messageList[i].receiver_id);
                        }
                        if(this.messageList[i].receiver_id == this.userid){
                            r.push(this.messageList[i].sender_id);
                        }
                    }

                    this.receivers = r.filter(function(elem, index, self) {
                        return index == self.indexOf(elem);
                    })
                    console.log(this.receivers);
                },

                getUserName(id) {
                    for(var i = 0; i < this.userList.length; i++){
                        if(this.userList[i].id == id){
                            return this.userList[i].name;
                        }
                    }
                },

                viewChat(id) {
                    this.fetchMessageList();
                    this.showChatArea = false;
                    this.messages.message = '';
                    this.receiverName = this.getUserName(id);
                    this.receiverId = id;
                    /*console.log(this.receiverId);*/
                    this.showChatArea = true;

                    this.getChatMessages(this.userid, id);

                },

                getChatMessages(sid, rid) {
                    this.activeMessages = [];
                    for(var i = 0; i < this.messageList.length; i++){
                        if(
                                (this.messageList[i].sender_id == sid && this.messageList[i].receiver_id == rid) ||
                                (this.messageList[i].sender_id == rid && this.messageList[i].receiver_id == sid)
                        ){
                            this.activeMessages.push(this.messageList[i]);
                        }
                    }
                    /*console.log(this.activeMessages);*/
                },

                getLatestMessage(sid, rid) {
                    for(var i = 0; i < this.messageList.length; i++){
                        if(this.messageList[i].id == id){
                            return this.messageList[i].message;
                        }
                    }
                },

                isReceiver(uid, sid) {
                      if(uid !== sid) {
                          return true;
                      }
                    else {
                          return false;
                      }
                },

                /*getUserMessages(sid,rid){
                    this.$http.get('api/messages/user/'+sid+rid).then(function (response) {
                        this.messageList = response.data;
                    });
                },*/

                storeMessage(){
                    var m = {};
                    m.sender_id = this.userid;
                    m.receiver_id = this.receiverId;
                    m.message = this.messages.message;

                    this.$http.post('message/store', m).then(function (response) {
                                this.viewChat(this.receiverId);
                                this.viewChat(this.receiverId);
                    },
                    function (response) {
                        // error callback
                    });
                }
            }
        })
    </script>

@stop