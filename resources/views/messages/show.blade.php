@extends('layouts.app')

@section('content')
    <div id="app" class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Conversation between me and {{ $rname }}</h3>
                <hr>
                @foreach($messages as $message)

                    @if($message->sender_id == Auth::user()->id)
                        <li class="list-group-item">
                            <strong>{{ Auth::user()->name }}: </strong>
                            {{ $message->message }}
                            <p style="font-size: 12px; color:#CCC;">{{ $message->created_at }}</p>
                        </li>
                    @else
                        <li class="list-group-item list-group-item-info">
                            <strong>{{ $rname }}: </strong></span>
                            {{ $message->message }}</span>
                            <p style="font-size: 12px; color:#CCC;">{{ $message->created_at }}</p>
                        </li>

                    @endif



                @endforeach
            </div>
            <div class="col-md-12" style="margin-top: 20px;"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="/messages" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <textarea v-model="message" name="message" class="form-control"></textarea>
                    </div>
                    <input type="hidden" value="{{ Auth::user()->id }}" name="sender_id">
                    <input type="hidden" value="{{ $rid }}" name="receiver_id">
                    <button v-show="message" class="btn btn-primary">Send</button>
                </form>
            </div>
        </div>
    </div>
@stop
@section('scripts.footer')
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: ''
            }
        })
    </script>
@stop