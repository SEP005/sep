@inject('countries', 'App\Http\Utilities\Country')
@extends('layouts.app')

@section('content')
    @include('includes.message_block')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel  panel-primary">
                <div class="panel-heading"><h3 class="text-center">Edit Profile</h3></div>
                    <div class="panel-body">
                        <form class="form-vertical" role="form" method="post" action="
                        {{route('profile.edit')}}">

                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('firstname')? 'has-error':'' }}">
                                    <label for="firstname" class="control-label">First name</label>
                                    <input type="text" name="firstname" class="form-control" id="first_name" value="{{ Request::old('firstname')?:Auth::user()->name  }}">
                                    @if($errors->has('firstname'))
                                        <span class="help-block">{{$errors->first('firstname')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('lastname')? 'has-error':'' }}">
                                    <label for="lastname" class="control-label">Last name</label>
                                    <input type="text" name="lastname" class="form-control" id="lastname"
                                           value="{{ Request::old('lastname')?:Auth::user()->lastname }}">
                                    @if($errors->has('lastname'))
                                        <span class="help-block">{{$errors->first('lastname')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group{{ $errors->has('address')? 'has-error':'' }}">
                                        <label for="address" class="control-label">Address</label>
                                        <input type="text" name="address" class="form-control" id="address"
                                               value="{{ Request::old('address')?:Auth::user()->address }}">
                                        @if($errors->has('address'))
                                            <span class="help-block">{{$errors->first('address')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group{{ $errors->has('street')? 'has-error':'' }}">
                                        <label for="street" class="control-label">Street name</label>
                                        <input type="text" name="street" class="form-control" id="street"
                                               value="{{ Request::old('street')?:Auth::user()->street  }}">
                                        @if($errors->has('street'))
                                            <span class="help-block">{{$errors->first('street')}}</span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group{{ $errors->has('city')? 'has-error':'' }}">
                                        <label for="city" class="control-label">City</label>
                                        <input type="text" name="city" class="form-control" id="city"
                                               value="{{ Request::old('city')?:Auth::user()->city }}">
                                        @if($errors->has('city'))
                                            <span class="help-block">{{$errors->first('city')}}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select class="form-control" name="country">
                                            <option value="{{ $user->country }}">{{ $user->country }}</option>
                                            @foreach($countries::all() as $country)
                                                <option value="{{ $country }}">{{ $country }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group{{ $errors->has('website')? 'has-error':'' }}">
                                        <label for="website" class="control-label">Website</label>
                                        <input type="text" name="website" class="form-control" id="website" value="{{ Request::old('website')?:Auth::user()->website}}">
                                        @if($errors->has('website'))
                                            <span class="help-block">{{$errors->first('website')}}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group{{ $errors->has('bio')? 'has-error':'' }}">
                                    <h3>About yourself</h3>
                                    <label for="bio" class="control-label">Bio</label>
                                    <textarea name="bio" rows="6" class="form-control" id="bio" value="{{ Request::old('bio')?:Auth::user()->bio}}"></textarea>
                                    @if($errors->has('bio'))
                                        <span class="help-block">{{$errors->first('bio')}}</span>
                                    @endif
                                </div>
                            </div>
                                </div>

                        <div class="form-group">
                            <button tupe="submit" class="btn btn-info btn-block"><b>Update</b></button>
                        </div>
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                        </form>
                </div>
            </div>
        </div>
    </div>
@endsection
