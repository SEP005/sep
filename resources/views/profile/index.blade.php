@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="/css/frontEnd-UI.css">

    <h2 class="text-center">{{ $user->name." ".$user->lastname }}</h2>
    <br />
    <div class="col-md-10 col-md-offset-1">
    <div class="row jumbotron" >
        <div class="col-sm-3">
            @if (Storage::disk('local')->has($user->first_name . '-' . $user->id . '.jpg'))
                <img src="{{ route('account.image', ['filename' => $user->first_name . '-' . $user->id . '.jpg']) }}"
                     alt="" class="img-circle" width="250" height="250">
            @else
                <img class="img-circle" src="{{$user->getAvatarUrl()}}" width="250" height="250">
            @endif
            @if(Auth::user()->id == $user->id)
                <hr>
            <form action="{{ route('account.save', $user->first_name) }}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="image" class="custom-file-upload">
                        <input type="file" name="image" class="form-control" id="image">
                        Browse</label>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i>
                    </button>

                </div>

                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
            @endif
        </div>

        <div class="col-sm-5">
            <div class="form-group row">
                <h3 class="text-center row col-sm-12">Basic Details</h3>
            </div>

            <div class="form-group row">
                <label class="text-right col-sm-3">Fullname:</label>
                <input class="col-sm-8" type="text" value="{{ $user->name." ".$user->lastname }}" readonly>
            </div>
            <div class="form-group row">
                <label class="text-right col-sm-3">Location:</label>
                <input class="col-sm-8" type="text" value="{{ $user->country }}" readonly>
            </div>

            <h3 class="text-center row col-sm-12">Contact Details</h3>
            <div class="form-group row">
                <label class="text-right col-sm-3">Website</label>
                <input class="col-sm-8" type="text" value="{{ $user->website }}" readonly>
            </div>

            <div class="form-group row">
                <label class="text-right col-sm-3">Email:</label>
                <input class="col-sm-8" type="text" value="{{ $user->email }}" readonly>
            </div>

        </div>


        <div class="col-sm-4">
            <div class="form-group row">
                <h3 class="text-center row col-sm-12"></h3>
            </div>
            <div class="form-group row">
                <h3 class="text-center row col-sm-12"></h3>
            </div>

            <div class="form-group row">
                <label class="text-right col-sm-5">Connections:</label>
                <input class="col-sm-3 col-sm-offset-1" style="text-align: center;border: none;opacity:0.4; color: red;font-weight: bold;; text-decoration: solid;" type="text" value="{{ $user->friends()->count() }}" readonly>
            </div>
            <div class="form-group row col-sm-offset-2">
                <div class="ratings">
                    {{--<p><b>{{$user->rating_count}} {{ str_plural('review', $user->rating_count)}}</b></p>--}}
                    <p>
                        @for ($i=1; $i <= 5 ; $i++)
                            <span class="glyphicon glyphicon-star{{ ($i <= $user->rating_cache) ? '' : '-empty'}}"></span>
                        @endfor
                        <b>{{ number_format($user->rating_cache, 1)}} stars</b>
                    </p>
                </div>

            </div>
            <div class="form-group row col-sm-offset-2">
                @if( Auth::user()->hasFriendRequestsPending($user))
                    <p><b><font color="#a9a9a9">Waiting for {{$user->getName()}}</font></b></p>
                    <p><b><font color="#a9a9a9">to accept your request.</font></b></p>
                @elseif( Auth::user()->hasFriendRequestsReceived($user))
                    <a href="{{route('friend.accept',['name'=>$user->name])}}"
                       class="btn-btn-primary"
                       style="background-color: rgba(50, 105, 255, 0.62);border-radius: 25px;color: white;padding: 2px 5px;text-align: center;text-decoration: none;display: inline-block;"
                       role="button"><p><b><font color="#f5f5f5"> Accept</font></b></p>
                    </a>
                    <a href="{{route('friend.delete',['name'=>$user->name])}}"
                       class="btn-btn-primary"
                       style="background-color: rgba(238, 42, 9, 0.56);border-radius: 25px;color: white;padding: 2px 5px;text-align: center;text-decoration: none;display: inline-block;"
                       role="button"><p><b><font color="#f5f5f5">Delete</font></b></p></a>
                @elseif(Auth::user()->isFriendsWith($user))
                    <p><b><font color="#a9a9a9">You and {{$user->getName()}} are connected</font></b></p>
                @elseif(Auth::user()->id !== $user->id)
                    <a href="{{route('friend.add',['name'=>$user->name])}}"
                       class="btn-btn-primary"
                       style="background-color: rgba(238, 42, 9, 0.56);border-radius: 25px;color: white;padding: 2px 5px;text-align: center;text-decoration: none;display: inline-block;"
                       role="button">
                        <h4>
                            <B>Send request</B>
                        </h4>
                    </a>
                @endif
            </div>

        </div>
    </div>
    </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="col-md-4">
                <div class="col-md-3" style="color: #FFF;background-color: #00AA88; padding-top: 10px; padding-bottom: 10px;">
                    <span style="font-size: 21px;"><strong>{{ $friendsCount }}</strong></span>
                </div>
                <div class="col-md-9" style="background-color: #EEE; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                    <h4>Friends</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="color: #FFF;background-color: #5d548f; padding-top: 10px; padding-bottom: 10px;">
                    <span style="font-size: 21px;"><strong>{{ $tasksCount }}</strong></span>
                </div>
                <div class="col-md-9" style="background-color: #EEE; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                    <h4>Tasks</h4>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-3" style="color: #FFF;background-color: #c7254e; padding-top: 10px; padding-bottom: 10px;">
                    <span style="font-size: 21px;"><strong>{{ $bidsCount }}</strong></span>
                </div>
                <div class="col-md-9" style="background-color: #EEE; border-bottom: 1px solid #DDD; padding-top: 5px; padding-bottom: 5px;">
                    <h4>Bids</h4>
                </div>
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1" style="height: 40px;"></div>

    <div class="col-md-10 col-md-offset-1">
    <div class="row jumbotron">

        <ul class="tab">
            <li><a href="javascript:void(0)" class="tablinks active" id="defaultOpen" onclick="openCity(event, 'Timeline')">Timeline</a></li>
            <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Connections')">Connections</a></li>
            <li><a href="javascript:void(0)" class="tablinks" onclick="openCity(event, 'Review')">Review</a></li>
        </ul>

        <div id="Timeline" class="tabcontent ">
            <h3 class="col-md-offset-1" style="visibility: hidden" >Timeline</h3>
            <div class="col-md-5 col-md-offset-1">
                @if(!$statuses->count())
                    <p ><b><font color="#dc143c">{{$user->getName()}}  hasn't post anything yet.</font></b></p>
                @else
                    @foreach($statuses as $status)
                        <div class="media">
                            <a class="pull-left" href="{{ route('profile.index',['name'=> $status->user->name]) }}">
                                @if (Storage::disk('local')->has($status->user->first_name . '-' . $status->user->id . '.jpg'))
                                    <img class="img-circle" src="{{ route('account.image', ['filename' => $status->user->first_name . '-' . $status->user->id . '.jpg']) }}"
                                         alt="{{$status->user->getName()}}" width="50" height="50" >
                                @else
                                    <img class="img-circle" alt="{{$status->user->getName()}}" src="{{$status->user->getAvatarUrl()}}" >
                                @endif
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="{{ route('profile.index',['name'=> $status->user->name]) }}">{{$status->user->getName()}}</a></h4>
                                <p><b>{{$status->body}}</b></p>
                                <ul class="list-inline">
                                    <li>{{ $status->created_at->diffForHumans() }}</li>

                                    @if($status->user->id !== Auth::user()->id)
                                        <li><a href="{{route('status.like', ['statusId'=> $status->id])}}">Like </a></li>
                                    @endif
                                    <li>{{ $status->likes->count() }} {{str_plural('like',$status->likes->count())}}</li>
                                </ul>

                                {{--@foreach($status->replies as $reply)--}}
                                {{--<div class="media">--}}
                                {{--<a class="pull-left" href="{{route('profile.index',['name'=> $reply->user->name])}}">--}}
                                {{--<img class="media-object" alt="{{$reply->user->getName()}}" src="{{$reply->user->getAvatarUrl()}}">--}}
                                {{--</a>--}}
                                {{--<div class="media-body">--}}
                                {{--<h5 class="media-heading"><a href="{{route('profile.index',['name'=> $reply->user->name])}}">{{$reply->user->getName()}}</a></h5>--}}
                                {{--<p>{{$reply->body}}</p>--}}
                                {{--<ul class="list-inline">--}}
                                {{--<li>{{ $reply->created_at->diffForHumans() }}</li>--}}
                                {{--@if($reply->user->id !== Auth::user()->id)--}}
                                {{--<li><a href="{{route('status.like', ['statusId'=> $reply->id])}}">Like </a></li>--}}
                                {{--@endif--}}
                                {{--<li>{{ $reply->likes->count() }} {{str_plural('like',$status->likes->count())}}</li>--}}

                                {{--</ul>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}

                                {{--@if($authUserIsFriend || Auth::user()->id === $status->user->id)--}}
                                {{--<form role="form" action="{{ route('status.reply',['statusId'=>$status->id]) }}" method="post">--}}
                                {{--<div class="form-group--}}
                                {{--{{ $errors->has("reply-{$status->id}")?' has-error':""}}">--}}
                                {{--<textarea name="reply-{{ $status->id }}" class="form-control" rows="2" placeholder="Reply to this status"></textarea>--}}
                                {{--@if($errors->has("reply-{$status->id}"))--}}
                                {{--<span class="help-block">--}}
                                {{--{{ $errors->first("reply-{$status->id}")}}--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--</div>--}}
                                {{--<input type="submit" value="Reply" class="btn btn-default btn-sm">--}}
                                {{--<input type="hidden" name="_token" value="{{Session::token()}}">--}}
                                {{--</form>--}}
                                {{--@endif--}}
                            </div>
                        </div>
                    @endforeach

                @endif
            </div>
        </div>

        <div id="Connections" class="tabcontent col-md-offset-1">
            <h3 class="col-md-offset-1" style="visibility: hidden">Connections</h3>
            <div class="form-group row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- Friend , Friend request -->


                    <h4>{{ $user->getName() }}'s Connections.</h4>

                    @if(!$user->friends()->count())
                        <p><b><font color="#dc143c"> {{$user->getName()}} has no Connections.</font></b></p>
                    @else
                        @foreach($user->friends()as $user)
                            @include('user/partials/userblock')
                        @endforeach
                    @endif

                </div>
            </div>
        </div>

        <div id="Review" class="tabcontent col-md-offset-1">
            <h3 class="col-md-offset-1" style="visibility: hidden">Review</h3>
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-10">
                    @if($user->id !== Auth::user()->id)
                        <div class="row">
                        <div class="col-md-15">
                            <form role="form" action="{{route('review.save',['userId'=> $user->id])}}" method="post">
                                <div class="col-lg-9">
                                    <div class="form-group {{ $errors->has('comment')?' has-error':""}}">
                                        <input type="text" class="form-control animated col-md-4" id="newReview"
                                                  name="comment" placeholder="Enter your review here..." rows="2">
                                        @if($errors->has('comment'))
                                            <span class="help-block">
                                                {{ $errors->first('comment')}}
                                             </span>
                                        @endif
                                    </div>
                                    <div class="rating pull-right" id="rating">
                                        <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Rocks!">5 stars</label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Pretty good">4 stars</label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Meh">3 stars</label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Kinda bad">2 stars</label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Very bad">1 star</label>
                                    </div>
                                </div>
                                <button class="btn btn-success btn-lg" type="submit">Save</button>
                                <input type="hidden" name="_token" value="{{Session::token()}}">

                            </form>
                        </div>
                    </div>
                    @endif



                </div>

                    <div class="row col-md-8">
                            @if(!$user->rating_count)
                                <p align="auto"><b><font color="#dc143c">No Reviews Yet!!!</font></b></p>
                            @else

                                {{--@if(isset($reviews))--}}
                                    {{--<h1>fff</h1>--}}
                        {{--@endif--}}

                            @foreach($reviews as $review)
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        @for ($i=1; $i <= 5 ; $i++)
                                            <span class="glyphicon glyphicon-star{{ ($i <= $review->rating) ? '' : '-empty'}}"></span>
                                        @endfor

                                        {{ $review->user->name }} <span class="pull-right">{{$review->created_at->diffForHumans()}}</span>

                                        <p>{{$review->comment}}</p>
                                    </div>
                                </div>
                            @endforeach
{{--                            {{ $user->review()->links() }}--}}
                        @endif
                    </div>
            </div>
        </div>

    <script>
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
        function openCity(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    </script>
@endsection
