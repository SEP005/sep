@extends('layouts.app')

@section('css.header')
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">
    <link rel="stylesheet" href="/css/animate.css">

@stop

@section('content')

    @include('includes.message_block')

    @if($user && $user->owns($showcase))

        <div class="container showcase_edit">

            <h1>Edit the Showcase</h1>

            <div class="row">

                <div class="col-md-6">

                    <form method="post" action="/showcase/{{$showcase->id}}" enctype="multipart/form-data">

                        {{--@include('$showcase.editform')--}}

                        {{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}
                        <input type="hidden" name="_method" value="PUT">


                        <div class="form-group">
                            <label for="category_id" class=""> Category : </label>

                            <select name="category_id" class="form-control">

                                <option value="{{$showcase->category_id}}">{{$showcase->getCategory->name}}</option>

                                {{--@foreach($category as $cat)--}}
                                {{--<option value="{{$cat->id}}">{{$cat->name}}</option>--}}
                                {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                                {{--@endforeach--}}

                                @foreach($category as $cat)
                                    <option value="{{$cat->id}}"><strong>{{$cat->name}}</strong></option>
                                    @foreach($cat->subCategory as $firstNestedSub)
                                        <option value="{{$firstNestedSub->id}}"> - {{$firstNestedSub->name}}</option>
                                    @endforeach
                                    {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                                @endforeach


                            </select>

                        </div>


                        <hr>
                        {{----}}
                        <div class="form-group">
                            <label for="title">Showcase title</label>
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="Enter the topic for the request" value="{{$showcase->title}}" required>


                            <input type="hidden" class="form-control" id="user_id" name="user_id"
                                   placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

                        </div>


                        <div class="form-group">
                            <input type="hidden" v-model="currencyType" class="form-control" id="currency_type"
                                   value="{{$showcase->currency_type}}"
                                   name="currency_type">
                        </div>

                        <hr>

                        <div class="form-group" v-show="currencyType">
                            {{--<i class="fa fa-money " aria-hidden="true"></i>--}}
                            <label for="price">What is the rate you charge</label>
                            <input type="text" class="form-control" id="budget" value="{{$showcase->budget}}" name="budget"
                                   placeholder="The rate for the showcase" required>
                        </div>

                        <div class="form-group">
                            <i class="fa fa-comment-o" aria-hidden="true"></i>
                            <label for="description">Describe the showcase design</label>
                            <textarea class="form-control" rows="10" name="description" required>
                                {{$showcase->description}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="video_path">Enter the video path to be embedded</label>

                            <input class="form-control" type="text" name="video_path" id="video_path" placeholder="Embed the video path"
                                   class="form-control"
                                   value="{{$showcase->video_path}}">
                        </div>
                        {{----}}

                        @if(!$showcase->pdf_path)
                            <div class="form-group">
                                <label for="pdfFile" class="btn btn-primary">
                                    <input type="file" name="pdfFile" class="form-control" id="pdfFile">
                                    Add a Pdf File
                                    <i class="fa fa-file" aria-hidden="true"></i>
                                </label>
                            </div>
                        @endif
                        {{--<div class="form-group" v-show="message">--}}
                            {{--<label for="pdfFile" class="btn btn-primary">--}}
                                {{--<input type="file" name="pdfFile" class="form-control" id="pdfFile">--}}
                                {{--Add a Pdf File--}}
                                {{--<i class="fa fa-file" aria-hidden="true"></i>--}}
                            {{--</label>--}}
                        {{--</div>--}}

                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary">
                                <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

                                Edit Showcase
                            </button>
                        </div>

                    </form>
                </div>
                {{--Info End of Coulmn-md-6--}}


                <div class="col-md-6 gallery showcase_edit">

                    @if($showcase->pdf_path)
                        {{--Info Edit PDF--}}

                        <form  method="POST" action="/deletePdf/{{ $showcase->id }}" v-ajax v-show="!message" complete="Pdf Deleted">
                            {{method_field('PUT')}}
                            {{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}



                            <a class="label label-success animated"
                               transition="fade"
                               href="{{ asset($showcase->pdf_path) }}"
                               v-show="!message"
                              >
                                View this showcase's pdf document
                            </a>
                            <button type="submit" class="btn label label-danger" @click="message='show'" >x</button>



                        </form>
                        {{--<a href="#"--}}
                           {{--class="label label-danger">x</a>--}}
                        {{--v-ajax--}}

                    @endif
                    <br>

                    @foreach($showcase->photos->chunk(3) as $set)
                        <div class="row">
                            @foreach($set as $photo)

                                <div class="col-md-4 gallery_image">

                                    @if($user && $user->owns($showcase))

                                        <form method="post" action="/showcasephotos/{{$photo->id}}">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button type="submit" class=" btn btn-sm btn-danger"><i class="fa fa-trash-o"
                                                                                                    aria-hidden="true"></i>
                                            </button>

                                        </form>
                                    @endif


                                    <a href="/{{$photo->path}}" data-lity>

                                        <img src="/{{$photo->thumbnail_path}}" alt="">

                                    </a>
                                </div>

                            @endforeach

                        </div>


                    @endforeach

                        @if($showcase->video_path)
                            <iframe width="420" height="315" src="{{$showcase->video_path}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @endif

                    @if($user && $user->owns($showcase))
                        {{--If We have a user and the user owns the tasks then add Image--}}

                        <hr>
                        <form id="addShowcasePhotoForm"
                              action="{{route('store_showcase_photo',[$showcase->id])}}"
                              method="post"
                              class="dropzone">


                            {{csrf_field()}}
                        </form>
                    @endif

                    {{--Info Showcase Photo End--}}

                </div> {{--End Of Col Gallery--}}


            </div>{{--End of Row--}}

        </div>

    @else

        {{flash()->error('You are not allowed to view this page','Only showcase owner can delete a task')}}
        <script type="text/javascript">
            window.location = "{{ url('/home') }}";//here double curly bracket
        </script>
    @endif

@endsection



@section('scripts.footer')

    <script src="/js/dropzone.js"></script>
    <script src="/js/lity.js"></script>
    <script src="/js/vue-resource.js"></script>

    <script src="/js/VueScripts/showcase-index.js"></script>

    <script>

        Dropzone.options.addShowcasePhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the task',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'


        };
    </script>

@stop