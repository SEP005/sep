@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/lity.min.css">


@stop



@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-6 ">


                <h1>Show case: {{$showcase->title}}</h1>
                <h4><span class="help-block">Showcase designed by {{$showcase->owner->name}}</span></h4>
                <h4><span class="help-block">Rate:  $ {{$showcase->budget}}</span>
                </h4>
                @if($showcase->pdf_path)
                    <a href="{{ asset($showcase->pdf_path) }}" class="label label-success">View this showcase's pdf document</a>
                @endif

                <hr>

                <div class="description">
                    {!!  nl2br($showcase->description) !!}  {{-- nl2br:To preserve the spaces --}}
                </div>

            </div> {{--End Of Col--}}

        <br>

            {{--Info Showcase photo--}}
    <div class="col-md-6 gallery">
        <hr>



        @foreach($showcase->photos->chunk(3) as $set)
            <div class="row">
                @foreach($set as $photo)

                    <div class="col-md-4 gallery_image">

                        @if($user && $user->owns($showcase))

                            <form method="post" action="/showcasephotos/{{$photo->id}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class=" btn btn-sm btn-danger"><i class="fa fa-trash-o"
                                                                                        aria-hidden="true"></i>
                                </button>

                            </form>
                        @endif


                        <a href="/{{$photo->path}}" data-lity>

                            <img src="/{{$photo->thumbnail_path}}" alt="">

                        </a>
                    </div>

                @endforeach

            </div>


        @endforeach

        @if($showcase->video_path)
            <iframe width="420" height="315" src="{{$showcase->video_path}}"
                    frameborder="0" allowfullscreen></iframe>
        @endif

    @if($user && $user->owns($showcase))
        {{--If We have a user and the user owns the tasks then add Image--}}

        <hr>
        <form id="addShowcasePhotoForm"
              action="{{route('store_showcase_photo',[$showcase->id])}}"
              method="post"
              class="dropzone">


            {{csrf_field()}}
        </form>
    @endif

            {{--Info Showcase Photo End--}}

    </div> {{--End Of Col Gallery--}}

    </div>{{--End Of Row--}}

@stop



@section('scripts.footer')

    <script src="/js/dropzone.js"></script>
    <script src="/js/lity.js"></script>

    <script>

        Dropzone.options.addShowcasePhotoForm = {

            dictDefaultMessage: 'Drag and drop any images that might be helpful in explaining the showcase',

            paramName: 'photo',

            maxFilesize: 3,

            acceptedFiles: '.jpg, .jpeg, .png, .bmp'

        };


    </script>


@stop