@extends('layouts.app')


@section('css.header')
    <link rel="stylesheet" type="text/css" href="../css/CssFileScripts/showcase-index.css">

    <style>
        .bg {
            background: url('../img/sayagata.png') ;
            /*background: image('../img/sayagata.jpg')  ;*/
            background-color: #2ad2c9;
            position: fixed;
            width: 100%;
            height: 350px; /*same height as jumbotron */
            top:0;
            left:0;
            z-index: -1;
        }

        .jumbotron {
            height: 300px;
            color: #6a737b;
            text-shadow: #444 0 1px 1px;
            background:transparent;
        }



    </style>
@stop

@section('content')



    <link href='http://fonts.googleapis.com/css?family=Esteban' rel='stylesheet' type='text/css'>

    <div class="container">

        {{--<div class="bg"></div>--}}
        {{--<div class="jumbotron">--}}
            {{--<h1>Showcases</h1>--}}
            {{--<p class="lead">View designs and works done by the community's workers</p>--}}
        {{--</div>--}}




        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="/showcase">All Showcases</a></li>
            <li role="presentation"><a href="/myshowcases">Your Showcases</a></li>

            <li role="presentation" class=""><a href="/showcase/create">
                    <i class="fa fa-plus  fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

                    Add your showcase
                </a></li>

            {{--<li role="presentation"><a href="#">Messages</a></li>--}}
        </ul>


        <div class="row">
            <div class="col-md-12">


        <!--Pattern HTML-->
        <div id="pattern" class="pattern">

            <ul class="list img-list">
                @foreach($showcases as $showcase)
                <li>
                    <a href="{{$showcase->path()}}" class="inner">
                        <div class="li-img">
                            {{--Info: Change pic Here--}}

                            {{--Check if showcase contains photos--}}
                            @if(count($showcase->photos))

                            @foreach($showcase->photos->slice (0, 1) as $set)
                                <img src="/{{$set->thumbnail_path}}" alt="Image Alt Text">
                            @endforeach

                                {{--Check if showcase doesnt have photos show a default pic--}}
                            @else
                            <img src="http://bradfrost.github.com/this-is-responsive/patterns/images/fpo_square.png" alt="Image Alt Text" />
                            @endif


                        </div>
                        <div class="li-text">
                            <h4 class="li-head">{{$showcase->title}}</h4>
                            <p style="color: #0b0b0b" class="li-sub tableTaskDetails">Showcase by:{{$showcase->owner->name}}</p>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <!--End Pattern HTML-->



            </div>
        </div>
    </div>
@stop

@section('scripts.footer')

    {{--<script src="../js/JsFileScripts/showcase-menu.js"></script>--}}



@stop