{{csrf_field()}} {{--This token is used to verify that the authenticated user is the one actually making the requests to the application.--}}

<div class="row showcase_edit">

    <div class="col-md-8">

        <div class="form-group">
            <label for="category_id" class=""> Category : </label>

            <select name="category_id" required class="form-control">

                <option>Category</option>


                @foreach($category as $cat)
                    <option value="{{$cat->id}}"><strong>{{$cat->name}}</strong></option>
                        @foreach($cat->subCategory as $firstNestedSub)
                        <option value="{{$firstNestedSub->id}}"> - {{$firstNestedSub->name}}</option>
                    @endforeach
                    {{--   <option>{{$cat->subCategories[0]->name}}</option> --}}

                @endforeach


            </select>

        </div>

        <hr>

        <div class="form-group">
            <label for="title" >Showcase title</label>
            <input type="text" class="form-control" id="title" name="title"
                   placeholder="Enter the topic for the request" value="{{old('title')}}" required>


            <input type="hidden" class="form-control" id="user_id" name="user_id"
                   placeholder="" value="{{Auth::user()->id}}">{{--To pass in the user's Id--}}

        </div>


        {{--<div class="form-group" >--}}
            {{--<i class="fa fa-money " aria-hidden="true"></i>--}}
            {{--<label for="currency_type">Currency</label>--}}
            {{--<select id="currency_type" name="currency_type" class="currency_type"></select>--}}

        {{--</div>--}}

        {{--TODO <div class="bfh-selectbox bfh-currencies" data-currency="EUR" data-flags="true">--}}
            {{--<input type="hidden" value="">--}}
            {{--<a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox" href="#">--}}
                {{--<span class="bfh-selectbox-option input-medium" data-option=""></span>--}}
                {{--<b class="caret"></b>--}}
            {{--</a>--}}
            {{--<div class="bfh-selectbox-options">--}}
                {{--<input type="text" class="bfh-selectbox-filter">--}}
                {{--<div role="listbox">--}}
                    {{--<ul role="option">--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--currencyType--}}
        {{--currency_type--}}


        <div class="form-group" >
            <input type="hidden" v-model="currencyType" class="form-control" id="currency_type" value="RS"  name="currency_type">
        </div>

        <hr>

        <div class="form-group" v-show="currencyType">
        {{--<i class="fa fa-money " aria-hidden="true"></i>--}}
            <label for="price">What is the rate you charge</label>
            <input type="text" class="form-control" id="budget" value="{{old('budget')}}"  name="budget"
                   placeholder="The rate for the showcase" required>
        </div>

        <div class="form-group">
            <i class="fa fa-comment-o" aria-hidden="true"></i>
            <label for="description">Describe the showcase design</label>
            <textarea class="form-control"  rows="10" name="description" value="{{old('description')}}" required></textarea>
        </div>

        <div class="form-group animated" transition="fade" v-show="video">
            <input class="form-control  " type="text" name="video_path" id="video_path"
                   placeholder="Embed the video path" class="form-control"
                   value="">
        </div>

        <div class="form-group">
            <label for="pdfFile"><span class="help-block">Add a pdf to upload a CV or additional showcase information</span></label>
<br>
            <label for="pdfFile" class="btn btn-primary">
                <input type="file" name="pdfFile" class="form-control" id="pdfFile">
                Add a Pdf File
                <i class="fa fa-file" aria-hidden="true"></i>
            </label>
        </div>

        <hr>

    <div class="form-group">

        <a class="btn btn-success animated " transition="fade" id="btnVideoShow" v-show="!video"  @click="video='show'">
            <i class="fa fa-video-camera" aria-hidden="true"></i>
            Embed a Video
        </a>



        <button type="submit" class="btn btn-lg btn-primary">
            <i class="fa fa-plus fa-spin fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

            Add Showcase</button>
    </div>

    </div>{{-- End of Coulmn--}}




</div>{{--End of Row--}}