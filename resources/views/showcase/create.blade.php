@extends('layouts.app')

@section('css.header')

    <link rel="stylesheet" href="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="/css/bootstrap-formhelpers.min.css">

    <link rel="stylesheet" href="/css/animate.css">


@stop

@section('content')
    @include('includes.message_block')

    <div class="container">

        <ul class="nav nav-tabs">
            <li role="presentation" class=""><a href="/showcase">All Showcases</a></li>
            <li role="presentation"><a href="/myshowcases">Your Showcases</a></li>

            <li role="presentation" class="active"><a href="/showcase/create">
                    <i class="fa fa-plus  fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

                    Add your showcase
                </a></li>

            {{--<li role="presentation"><a href="#">Messages</a></li>--}}
        </ul>

        <h1>ShowCase your work!</h1>



        <form class= "formSub" method="post" action="/showcase" enctype="multipart/form-data"  >

            @include('showcase.form')

        </form>




    </div>

@endsection


@section('scripts.footer')

    <script src="/js/bootstrap-formhelpers.min.js"></script>

    <script src="/js/vue-resource.js"></script>

    <script src="/js/VueScripts/showcase-index.js"></script>

    <script>


        //Vue model
        new Vue({

            el: '.formSub',
            data: {
                currencyType: ''
            }
        })


    </script>


@stop