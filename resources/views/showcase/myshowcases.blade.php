@extends('layouts.app')


@section('css.header')
    <link rel="stylesheet" type="text/css" href="../css/CssFileScripts/showcase-index.css">
    <link rel="stylesheet" href="/css/jquery.dataTables.min.css">

@stop

@section('content')





    <link href='http://fonts.googleapis.com/css?family=Esteban' rel='stylesheet' type='text/css'>

    <div class="container">

        <ul class="nav nav-tabs">
            <li role="presentation" ><a href="/showcase">All Showcases</a></li>
            <li role="presentation" class="active"><a href="/myshowcases">Your Showcases</a></li>
            <li role="presentation" class=""><a href="/showcase/create">
                    <i class="fa fa-plus  fa-1x fa-fw margin-bottom" aria-hidden="true"></i>

                    Add your showcase</a></li>

            {{--<li role="presentation"><a href="#">Messages</a></li>--}}
        </ul>

        <div class="row">
            <div class="page-header">
                <h2 class="">Showcases you have designed</h2>
            </div>
            <table id="myTable" class="table">
                <thead>
                <tr>
                    <th>Showcase Id</th>

                    <th class="tableTaskTopic">Title</th>
                    <th>Description</th>
                    {{--<th class="tableTaskSkills">Skills Required</th>--}}


                    <th></th>
                    <th></th>


                </tr>
                </thead>


                <tbody>


                @foreach($showcases as $showcase)

                    <tr>
                        <td><a href="{{$showcase->path()}}">{{$showcase->id}} </a></td>

                        <td class="tableTaskTopic">

                            <a href="{{$showcase->path()}}">{{$showcase->title}} </a>
                        </td>

                        <td><p class="tableTaskDetails"> {{$showcase->description}}<p></td>
                        {{--<td class="tableTaskSkills"> {{$task->skills}}</td>--}}



                        <td>
                            <a href="/showcase/{{$showcase->id}}/edit" class="btn btn-sm btn-warning">Edit
                            </a>
                        </td>

                        <td>

                            {{--TODO Check
                              <a href="/tasks/{{$task->id}}" class="btn btn-danger" data-method="delete"
                              data-token="{{csrf_token()}}" data-confirm="Are you sure?">Delete</a> --}}

                            <a class="btn btn-danger" role="button" onclick="confirmDelete({{ $showcase->id }})">
                                Delete
                            </a>
                            {{--<form id="deleteform" method="POST" action="/tasks/{{$task->id}}" >--}}
                            {{--{{csrf_field()}}--}}
                            {{--<input type="hidden" name="_method" value="DELETE">--}}

                            {{--<button type="submit" id="delete" class="btn btn-sm btn-danger">--}}
                            {{--<i class="fa fa-trash-o" aria-hidden="true"></i> {{$task->id}}--}}
                            {{--</button>--}}

                            {{--</form>--}}



                        </td>


                    </tr>

                @endforeach

                </tbody>


            </table>


        </div>



    </div>
@stop

@section('scripts.footer')
    <script src="/js/jquery.dataTables.min.js"></script>

    <script>

        $(document).ready(function () {
            $('#myTable').DataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true,
                "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                "ZeroRecords": "No matching records found",
                "searchDelay": null,
                "sPaginationType": "full_numbers",
                "scrollX": true,
                "columns": [
                    {"searchable": true},
                    {"searchable": true},
                    {"searchable": true},
                    // {"searchable": false},
//                    {"searchable": true},
                    {"searchable": false},
                    {"searchable": false},

                ],
                "columns": [
                    {"sortable": true},
                    {"sortable": true},
                    {"sortable": false},
                    // {"sortable": false},
//                    {"sortable": true},
                    {"sortable": false},
                    {"sortable": false},

                ],


            });

        });

    </script>

    <script>

        (function() {

            var laravel = {
                initialize: function() {
                    this.methodLinks = $('a[data-method]');
                    this.token = $('a[data-token]');
                    this.registerEvents();
                },

                registerEvents: function() {
                    this.methodLinks.on('click', this.handleMethod);
                },

                handleMethod: function(e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;

                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }

                    // Allow user to optionally provide data-confirm="Are you sure?"
                    if ( link.data('confirm') ) {
                        if ( ! laravel.verifyConfirm(link) ) {
                            return false;
                        }
                    }

                    form = laravel.createForm(link);
                    form.submit();

                    e.preventDefault();
                },

                verifyConfirm: function(link) {
                    return confirm(link.data('confirm'));
                },


                createForm: function(link) {
                    var form =
                            $('<form>', {
                                'method': 'POST',
                                'action': link.attr('href')
                            });

                    var token =
                            $('<input>', {
                                'type': 'hidden',
                                'name': '_token',
                                'value': link.data('token')
                            });

                    var hiddenInput =
                            $('<input>', {
                                'name': '_method',
                                'type': 'hidden',
                                'value': link.data('method')
                            });

                    return form.append(token, hiddenInput)
                            .appendTo('body');
                }
            };

            laravel.initialize();

        })();
    </script>

    <script>
        function confirmDelete(id) {
            swal({
                        title: "Are you sure you want to delete this Showcase?",
                        text: "This post will be un-recoverable!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                        window.location.href = "/showcase/" + id + "/delete";
                    });
        }
    </script>

@stop


