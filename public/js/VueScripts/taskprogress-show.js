
//        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('input[name="_token"]').value;
//v-ajax
try {
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('input[name="_token"]').getAttribute('value');
}
catch (err) {
    console.log('No Laravel CSRF',err);
}

Vue.transition('fade',{
    enterClass: 'flipInX',
    leaveClass:'lightSpeedOut'
})

Vue.component('milestone',{
    
    template: '#milestone-template',

    data:function () {
        return {
            editMessage: 'hello',
            editId: 0,
            deleteMilestoneId:0,

        }
    },

    props:['list'],



    created:function () {

        this.list = JSON.parse(this.list);
        // this.fetchTaskList();

    } ,// When component is created

    methods:{

        deleteMilestone:function (milestone) {
            this.list.$remove(milestone);
        },
        
        updateMilestone:function (milestone) {

            if(milestone.status == 1 )
            {
                milestone.status = 2;
            }
 
            if(milestone.status == 2 )
            {
                milestone.status =1;
            }
            
        },

    },



    directives: {
        ajax: {
            params:['complete'],

            //When Submitted Call on onSubmit
            bind: function () {
                this.el.addEventListener('submit',this.onSubmit.bind(this));// form v-ajax
            },

            update:function () {
                //alert('update');
            },

            unbind:function () {

            },

            onSubmit: function (e) {
                // this refers to form submitted
                e.preventDefault(); //Do not submit form


                //http.post or http['post']
                this.vm // use vue resource
                    .$http[this.getRequestType()](this.el.action) // eg .$http[delete](/task/1)
                    .then(this.onComplete.bind(this))
                    .catch(this.onError.bind(this)); // Catch any exceptions
            },

            onError:function (response) {
                console.log(response);
                alert(response.data.message);
            },

            onComplete:function () {

                if(this.params.complete){
                    alert(this.params.complete);

                }

            },

            getRequestType:function () {
                // To get delete/put/push method type
                var method = this.el.querySelector('input[name="_method"]');

                // if(method.value == "DELETE")
                // {
                //     confirm('are you sure?');
                //     // this.list.$remove(milestone);
                // }
                // TO get default post or delete/put/push method type
                return (method ? method.value : this.el.method).toLowerCase();
            },

            http:{

            }
        }
    }


});

new Vue({
    el: '.vueMilestone',
    data:{
        message:'',
        editId: 0,
        deleteMilestoneId:0,
    }

});
