
try {
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('input[name="_token"]').getAttribute('value');
}
catch (err) {
    console.log('No Laravel CSRF',err);
}

Vue.transition('fade',{
    enterClass: 'flipInX',
    leaveClass:'zoomOutDown'
});

Vue.component('todolist',{

    template: '#todolist-template',


    data:function () {
        return {
            list:[],
            editMessage: 0,
            editId: 1,

        }
    },
    created:function () {

        this.fetchTaskList();

    } ,// When component is created

    methods:{

        deleteToDoItem:function (todoitem) {
            this.list.$remove(todoitem);

        },
        updateToDoItem:function (todoitem) {

            if(todoitem.completed == 0 )
            {
                todoitem.completed =1
            }
                else {
                todoitem.completed =0
            }
        },

        addItem:function () {
            this.list.push({body:this.message,completed:0});
            this.message='';
        },



        fetchTaskList: function () {

            var resource = this.$resource('api/todolist'); //:id is depreciated
            resource.get().then((response) => {
                // success callback
                console.log(response);

            var vm= this;

            vm.list = response.data;
        });

        }

    },

    directives: {
        ajax: {
            params:['complete'],
            
            //When Submitted Call on onSubmit
            bind: function ()
            {
                this.el.addEventListener('submit',this.onSubmit.bind(this));// form v-ajax
            },

            onSubmit: function (e)
            {
                // this refers to form submitted
                e.preventDefault(); //Do not submit form

                //http.post or http['post']
                this.vm // use vue resource
                    .$http[this.getRequestType()](this.el.action) // eg .$http[delete](/task/1)
                    .then(this.onComplete.bind(this))
                    .catch(this.onError.bind(this)); // Catch any exceptions
            },

            onError:function (response)
            {
                console.log(response);
                alert(response.data.message);
            },

            onComplete:function ()
            {
                if(this.params.complete)
                {
                    alert(this.params.complete);
                }
            },

            getRequestType:function ()
            {
                // To get delete/put/push method type
                var method = this.el.querySelector('input[name="_method"]');

                // TO get default post or delete/put/push method type
                return (method ? method.value : this.el.method).toLowerCase();
            },
        }
    }

});

new Vue({
    el: '.vueToDo',
    data:{
        message:'',
        addToDoItem: ''
    }

});
