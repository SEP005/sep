/**
 * Created by Nauf on 20/09/16.
 */
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('input[name="_token"]').value;
//v-ajax
Vue.directive('ajax',{

    params:['complete'],
    //When Submitted CAll on onSubmit
    bind: function () {
        this.el.addEventListener('submit',this.onSubmit.bind(this));// form v-ajax 
    },

    update:function () {
        //alert('update');
    },

    unbind:function () {

    },
    
    onSubmit: function (e) {
      // this refers to form submitted
        e.preventDefault(); //Do not submit form


        //http.post or http['post']
        this.vm // use vue resource
            .$http[this.getRequestType()](this.el.action) // eg .$http[delete](/task/1)
            .then(this.onComplete.bind(this))
            .catch(this.onError.bind(this)); // Catch any exceptions


    },
    
    onError:function (response) {
        console.log(response);
        alert(response.data.message);
    },

    onComplete:function () {

        if(this.params.complete){
            alert(this.params.complete);

        }

    },

    getRequestType:function () {
        var method = this.el.querySelector('input[name="_method"]');

        return (method ? method.value : this.el.method).toLowerCase(); // TO get post or delete
    },

});

new Vue({
    el: '.vueToDo',

    http:{
        headers:{
            //Alternative to Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('input[name="_token"]').value;
            'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value,
        }
    }

})