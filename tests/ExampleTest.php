<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
//    /**
//     * A basic testing todo-list creation
//     *
//     * @return void
//     */
//    public function testCreationOfTodoItem()
//    {
//
//        $todoItem = new \App\ToDoList();
//        // The user who creates the to do list item
//        $todoItem->user_id = 1;
//        //To-do list items body
//        $todoItem->body = "Loremn Ipsum test";
//        // Completed status of to-do list item is 0
//        $todoItem->completed = 0;
//        $todoItem->save();
//
//    }

    /**
     * To test creation of Tasks, Showcases and Todo Lists
     * which is used for testing
     *
     * @return void
     */
    public function testCreationOfDummyData()
    {
        // Create 10 Tasks
        factory('App\Task',10)->create();
        // Create 10 Showcases
        factory('App\Showcase',5)->create();
        // Create 10 To do lists
        factory('App\ToDoList',10)->create();
    }

}






//        //Visit the home page
//        $this->visit('/home')
//             ->see('Dashboard');
////             ->click('Login');

//Press go to dashboard

// See Ongoing tasks
// Assert that the current url is /taskprogress
