<?php
//$factory('App\User',[
//        'name' => $faker->name,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(10)),
//        'remember_token' => str_random(10),
//]);

$factory('App\Task',[

    'topic' => $faker->jobTitle,

    'user_id' => $faker->numberBetween(1,2),//factory('App\User')->create()->id,

    'category_id' => $faker->numberBetween(1,2),

    'subcategory_id' => $faker->numberBetween(1,2),

    'details' => $faker->paragraph(4),

    'status' => 0,

    'skills' => 'webdevelopment,Desktop application',

    'price' => $faker->numberBetween(1,90000)

]);